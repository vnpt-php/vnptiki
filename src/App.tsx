import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React, { Suspense } from "react";
import { Spin } from "antd";

const lazyImport = (path: string) => React.lazy(() => import(`${path}`));

function App() {
  const Home = lazyImport("./pages/home");
  const Store = lazyImport("./pages/store");
  const Shop = lazyImport("./pages/shop");
  const StoreProfile = lazyImport("./pages/store-profile");
  const StoreRegistration = lazyImport("./pages/store-registration");
  const StoreProductList = lazyImport("./pages/store-product-list");
  const StoreCreateProduct = lazyImport("./pages/store-create-product");
  const StoreSettingProduct = lazyImport("./pages/store-setting-product");
  const StoreAddress = lazyImport("./pages/store-address");
  const StoreOrderList = lazyImport("./pages/store-order-list");
  const AccountInfo = lazyImport("./pages/account-info");
  const AccountAddressBook = lazyImport("./pages/account-address-book");
  const AccountAddAddress = lazyImport("./pages/account-add-address");
  const AccountEditAddress = lazyImport("./pages/account-edit-address");
  const OrderManagement = lazyImport("./pages/order-management");
  const Cart = lazyImport("./pages/cart");
  const Product = lazyImport("./pages/product");
  const Payment = lazyImport("./pages/payment");
  const ProductType = lazyImport("./pages/product-type");
  const ProductBrand = lazyImport("./pages/product-brand");

  return (
    <Router>
      <Suspense fallback={<Spin size="large" />}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/store" element={<Store />} />
          <Route path="/store/setting/profile" element={<StoreProfile />} />
          <Route path="/store/address" element={<StoreAddress />} />
          <Route
            path="/store/registration/form"
            element={<StoreRegistration />}
          />
          <Route
            path="/store/product/list/all"
            element={<StoreProductList />}
          />
          <Route path="/store/product/new" element={<StoreCreateProduct />} />
          <Route
            path="/store/product/setting"
            element={<StoreSettingProduct />}
          />
          <Route path="/store/sale/order" element={<StoreOrderList />} />
          <Route path="/customer/account-info" element={<AccountInfo />} />
          <Route path="/customer/address" element={<AccountAddressBook />} />
          <Route
            path="/customer/address/create"
            element={<AccountAddAddress />}
          />
          <Route
            path="/customer/address/edit/:id"
            element={<AccountEditAddress />}
          />
          <Route path="/sales/order/history" element={<OrderManagement />} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/cart/payment" element={<Payment />} />
          <Route path="/product/:name/:id" element={<Product />} />
          <Route path="/products/:name/:id" element={<ProductType />} />
          <Route path="/products/brand/:id" element={<ProductBrand />} />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
