import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreProductList from "../containers/store/Store_Product_List";
import { Product } from "../models/types/Product";
import ProductContext from "../context/ProductContext";
import { useState } from "react";
import { Store } from "../models/types/Store";
import StoreContext from "../context/StoreContext";

function StoreProductListParent() {
  const [dataStore, setDataStore] = useState<Store>();
  const [dataProduct, setDataProduct] = useState<Product[]>([]);

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <ProductContext.Provider value={{ dataProduct, setDataProduct }}>
          <HeaderStore />
          <StoreProductList />
        </ProductContext.Provider>
      </StoreContext.Provider>
    </>
  );
}

export default StoreProductListParent;
