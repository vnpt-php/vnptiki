import { StyledUniversalFreeShipPlus } from "../components/styled/styled-components";
import HeaderHome from "../components/HeaderHome";
import FooterHome from "../components/FooterHome";
import OrderManagement from "../containers/customer/Order_Management";
import { Helmet } from "react-helmet";

function order_management() {
  return (
    <>
      <Helmet>
        <title>Đơn hàng của tôi | VNPTiki</title>
      </Helmet>
      <HeaderHome />
      <StyledUniversalFreeShipPlus />
      <OrderManagement />
      <FooterHome />
    </>
  );
}

export default order_management;
