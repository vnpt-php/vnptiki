import { Helmet } from "react-helmet";
import ProductBrand from "../containers/ProductBrand";

function product_type() {
  return (
    <>
      <Helmet>
        <title>Cửa hàng online | VNPTiki</title>
      </Helmet>
      <ProductBrand />
    </>
  );
}

export default product_type;
