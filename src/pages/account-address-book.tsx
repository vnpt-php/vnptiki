import { StyledUniversalFreeShipPlus } from "../components/styled/styled-components";
import HeaderHome from "../components/HeaderHome";
import FooterHome from "../components/FooterHome";
import AddressBook from "../containers/customer/Address_Book";
import TitleAddress from "../components/customer/TitleAddress";
import { Cart } from "../models/types/Cart";
import CartContext from "../context/CartContext";
import { useState } from "react";

function AccountAddressBookParent() {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <>
      <TitleAddress />
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>
      <StyledUniversalFreeShipPlus />
      <AddressBook />
      <FooterHome />
    </>
  );
}

export default AccountAddressBookParent;
