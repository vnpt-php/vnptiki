import { Helmet } from "react-helmet";
import Payment from "../containers/Payment";

function payment() {
  return (
    <>
      <Helmet>
        <title>Thanh toán | VNPTiki</title>
      </Helmet>
      <Payment />
    </>
  );
}

export default payment;
