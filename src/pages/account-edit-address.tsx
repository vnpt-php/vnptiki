import { StyledUniversalFreeShipPlus } from "../components/styled/styled-components";
import HeaderHome from "../components/HeaderHome";
import FooterHome from "../components/FooterHome";
import EditAddress from "../containers/customer/Edit_Address";
import TitleAddress from "../components/customer/TitleAddress";
import { Cart } from "../models/types/Cart";
import CartContext from "../context/CartContext";
import { useState } from "react";

function AccountEditAddress() {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <>
      <TitleAddress />
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>
      <StyledUniversalFreeShipPlus />
      <EditAddress />
      <FooterHome />
    </>
  );
}

export default AccountEditAddress;
