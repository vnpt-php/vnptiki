import { Helmet } from "react-helmet";
import Product from "../containers/Product";

function product() {
  return (
    <>
      <Helmet>
        <title>Cửa hàng online | VNPTiki</title>
      </Helmet>
      <Product />
    </>
  );
}

export default product;
