import { useState } from "react";
import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreContainer from "../containers/store/Store";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";
import { Product } from "../models/types/Product";
import ProductContext from "../context/ProductContext";

const StoreParent: React.FC = () => {
  const [dataStore, setDataStore] = useState<Store>();
  const [dataProduct, setDataProduct] = useState<Product[]>([]);

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <ProductContext.Provider value={{ dataProduct, setDataProduct }}>
          <HeaderStore />
          <StoreContainer />
        </ProductContext.Provider>
      </StoreContext.Provider>
    </>
  );
};

export default StoreParent;
