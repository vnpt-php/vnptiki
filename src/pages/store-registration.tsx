import { Helmet } from "react-helmet";
import styled from "styled-components";
import RegistrationForm from "../components/store/RegistrationForm";
import RegistrationHeader from "../components/store/RegistrationHeader";
import { useCallback, useEffect, useState } from "react";
import { User } from "../models/types/User";
import { REQUEST_TYPE } from "../enums/RequestType";
import { ApiResponse } from "../models/ApiResponse";

import { useNavigate } from "react-router-dom";
import { useApi } from "../hooks/useApi";

const StyledPage = styled.div`
  padding-top: 56px;
  min-width: 1264px;
  margin: 0 auto;
`;

const StyledHeaderPage = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 56px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.07);
  background-color: #fff;
  z-index: 999;
`;

const StyledContentPage = styled.div`
  min-height: calc(100vh - 56px - 40px);
  padding-bottom: 40px;
  background-color: #f6f6f6;
`;

const StyledContentHeader = styled.div`
  margin-bottom: 36px;
  background-color: #fff;
`;

function StoreRegistration() {
  const { callApi } = useApi();
  const navigate = useNavigate();
  const [dataUser, setDataUser] = useState<User | undefined>();

  const token = sessionStorage.getItem("ACCESS_TOKEN");

  const getDataUser = useCallback(() => {
    callApi<ApiResponse<User>>(REQUEST_TYPE.GET, "api/user/current")
      .then((response) => {
        setDataUser(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const redirectToStore = useCallback(
    (userData: User | undefined) => {
      if (userData && userData.store) {
        navigate("/store");
      }
    },
    [navigate]
  );

  useEffect(() => {
    if (token) {
      redirectToStore(dataUser);
      getDataUser();
    } else {
      navigate("/");
    }
  }, [token, navigate]);

  return (
    <>
      <Helmet>
        <title>VNPTiki - Trở thành thành viên mới</title>
      </Helmet>

      <StyledPage>
        <StyledHeaderPage>
          <RegistrationHeader dataUser={dataUser} />
        </StyledHeaderPage>

        <StyledContentPage>
          <StyledContentHeader />
          <RegistrationForm dataUser={dataUser} />
        </StyledContentPage>
      </StyledPage>
    </>
  );
}

export default StoreRegistration;
