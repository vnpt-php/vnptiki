import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreOrderList from "../containers/store/Store_Order_List";
import { useState } from "react";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";

const StoreOrderListParent: React.FC = () => {
  const [dataStore, setDataStore] = useState<Store>();

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <HeaderStore />
        <StoreOrderList />
      </StoreContext.Provider>
    </>
  );
};

export default StoreOrderListParent;
