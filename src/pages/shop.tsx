import { Helmet } from "react-helmet";
import Shop from "../containers/Shop";

function shop() {
  return (
    <>
      <Helmet>
        <title>Cửa hàng online | VNPTiki</title>
      </Helmet>
      <Shop />
    </>
  );
}

export default shop;
