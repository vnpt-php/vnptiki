import { StyledUniversalFreeShipPlus } from "../components/styled/styled-components";
import HeaderHome from "../components/HeaderHome";
import FooterHome from "../components/FooterHome";
import AccountInfo from "../containers/customer/Account_Info";
import { Helmet } from "react-helmet";
import CartContext from "../context/CartContext";
import { Cart } from "../models/types/Cart";
import { useState } from "react";

function AccountInfoParent() {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <>
      <Helmet>
        <title>Tài khoản của tôi | VNPTiki</title>
      </Helmet>
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>
      <StyledUniversalFreeShipPlus />
      <AccountInfo />
      <FooterHome />
    </>
  );
}

export default AccountInfoParent;
