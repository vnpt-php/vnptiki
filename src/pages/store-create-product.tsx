import { useState } from "react";
import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreCreateProduct from "../containers/store/Store_Create_Product";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";
import CategoryContext from "../context/Category";
import { Category } from "../models/types/Category";

function StoreCreateProductParent() {
  const [dataStore, setDataStore] = useState<Store>();
  const [dataCategory, setDataCategory] = useState<Category[]>([]);

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <CategoryContext.Provider value={{ dataCategory, setDataCategory }}>
          <HeaderStore />
          <StoreCreateProduct />
        </CategoryContext.Provider>
      </StoreContext.Provider>
    </>
  );
}

export default StoreCreateProductParent;
