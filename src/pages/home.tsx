import { Helmet } from "react-helmet";
import Home from "../containers/Home";

function home() {
  return (
    <>
      <Helmet>
        <title>VNPTiki - Mua hàng online giá tốt, hàng chuẩn</title>
      </Helmet>
      <Home />
    </>
  );
}

export default home;
