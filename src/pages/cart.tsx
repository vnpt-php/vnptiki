import { Helmet } from "react-helmet";
import Cart from "../containers/Cart";

function cart() {
  return (
    <>
      <Helmet>
        <title>Giỏ hàng | VNPTiki</title>
      </Helmet>
      <Cart />
    </>
  );
}

export default cart;
