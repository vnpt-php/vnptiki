import { useState } from "react";
import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreProfile from "../containers/store/Store_Profile";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";

function StoreProfileParent() {
  const [dataStore, setDataStore] = useState<Store>();

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <HeaderStore />
        <StoreProfile />
      </StoreContext.Provider>
    </>
  );
}

export default StoreProfileParent;
