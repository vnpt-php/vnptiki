import { Helmet } from "react-helmet";
import ProductType from "../containers/ProductType";

function product_type() {
  return (
    <>
      <Helmet>
        <title>Cửa hàng online | VNPTiki</title>
      </Helmet>
      <ProductType />
    </>
  );
}

export default product_type;
