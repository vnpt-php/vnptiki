import { useState } from "react";
import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreAddress from "../containers/store/Store_Address";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";

function StoreAddressParent() {
  const [dataStore, setDataStore] = useState<Store>();

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <HeaderStore />
        <StoreAddress />
      </StoreContext.Provider>
    </>
  );
}

export default StoreAddressParent;
