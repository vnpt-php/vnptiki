import { useState } from "react";
import HeaderStore from "../components/store/HeaderStore";
import TitleStore from "../components/store/TitleStore";
import StoreContext from "../context/StoreContext";
import { Store } from "../models/types/Store";
import StoreSettingProduct from "../containers/store/Store_Setting_Product";
import CategoryContext from "../context/Category";
import { Category } from "../models/types/Category";
import BrandContext from "../context/BrandContext";
import { Brand } from "../models/types/Brand";

const StoreSettingProductParent: React.FC = () => {
  const [dataStore, setDataStore] = useState<Store>();
  const [dataCategory, setDataCategory] = useState<Category[]>([]);
  const [dataBrand, setDataBrand] = useState<Brand[]>([]);

  return (
    <>
      <TitleStore />
      <StoreContext.Provider value={{ dataStore, setDataStore }}>
        <CategoryContext.Provider value={{ dataCategory, setDataCategory }}>
          <BrandContext.Provider value={{ dataBrand, setDataBrand }}>
            <HeaderStore />
            <StoreSettingProduct />
          </BrandContext.Provider>
        </CategoryContext.Provider>
      </StoreContext.Provider>
    </>
  );
};

export default StoreSettingProductParent;
