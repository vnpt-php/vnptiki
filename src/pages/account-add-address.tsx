import { StyledUniversalFreeShipPlus } from "../components/styled/styled-components";
import HeaderHome from "../components/HeaderHome";
import FooterHome from "../components/FooterHome";
import AddAddress from "../containers/customer/Add_Address";
import TitleAddress from "../components/customer/TitleAddress";
import { Cart } from "../models/types/Cart";
import CartContext from "../context/CartContext";
import { useState } from "react";

function AccountAddAddress() {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <>
      <TitleAddress />
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>
      <StyledUniversalFreeShipPlus />
      <AddAddress />
      <FooterHome />
    </>
  );
}

export default AccountAddAddress;
