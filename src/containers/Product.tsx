import { useEffect, useState } from "react";
import ProductOrder from "../components/Product/ProductOrder";
import ProductSimilar from "../components/Product/ProductSimilar";
import ProductInfo from "../components/Product/ProductInfo";
import ProductDescribe from "../components/Product/ProductDescribe";
import ProductComment from "../components/Product/ProductComment";
import { Layout, Space } from "antd";
import HeaderHome from "../components/HeaderHome";
import { useParams } from "react-router-dom";
import { Product } from "../models/types/Product";
import { ApiResponse } from "../models/ApiResponse";
import { baseUrl } from "../constants/url";
import axios from "axios";
import CartContext from "../context/CartContext";
import { Cart } from "../models/types/Cart";

function ProductDetail() {
  const { id } = useParams();

  const [dataProduct, setDataProduct] = useState<Product | undefined>();
  const [dataProducts, setDataProducts] = useState<Product[] | undefined>([]);
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  useEffect(() => {
    const getDataProduct = () => {
      const url = `${baseUrl}/api/product/${id}`;
      axios
        .get<ApiResponse<Product>>(url)
        .then((res) => {
          setDataProduct(res.data.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    const getDataProducts = () => {
      const url = `${baseUrl}/api/products`;
      axios
        .get<ApiResponse<Product[]>>(url)
        .then((res) => {
          setDataProducts(res.data.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    getDataProducts();
    getDataProduct();
  }, [id]);

  return (
    <Space
      direction="vertical"
      style={{
        width: "100%",
        backgroundColor: "#f5f5fa",
      }}
      size={[0, 0]}
    >
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>

      <Layout
        style={{
          width: "1270px",
          paddingLeft: "15px",
          paddingRight: "15px",
          marginRight: "auto",
          marginLeft: "auto",
        }}
      >
        <ProductOrder dataProduct={dataProduct} />
        <ProductSimilar dataProduct={dataProduct} dataProducts={dataProducts} />
        <ProductInfo dataProduct={dataProduct} />
        <ProductDescribe dataProduct={dataProduct} />
        <ProductComment />
      </Layout>
    </Space>
  );
}

export default ProductDetail;
