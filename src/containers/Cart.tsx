import { Layout, Space } from "antd";
import HeaderHome from "../components/HeaderHome";
import ContentCart from "../components/ContentCart";
import FooterHome from "../components/FooterHome";
import { Cart } from "../models/types/Cart";
import React, { useState } from "react";
import CartContext from "../context/CartContext";

const CartContainer: React.FC = () => {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <Space
      direction="vertical"
      style={{
        width: "100%",
        backgroundColor: "#f5f5fa",
      }}
      size={[0, 0]}
    >
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
        <ContentCart />
      </CartContext.Provider>
      <Layout
        style={{
          width: "100%",
          backgroundColor: "white",
          paddingRight: "145px",
          paddingLeft: "145px",
        }}
      >
        <FooterHome></FooterHome>
      </Layout>
    </Space>
  );
};

export default CartContainer;
