import { Layout, Space } from "antd";
import React from "react";
import FooterHome from "../components/FooterHome";
import HeaderHome from "../components/HeaderHome";
import BannerShop from "../components/shop/BannerShop";
import ContentShop from "../components/shop/ContentShop";
import HeaderShop from "../components/shop/HeaderShop";

function Shop() {
  return (
    <Space
      direction="vertical"
      style={{
        width: "100%",
        backgroundColor: "#f5f5fa",
      }}
      size={[0, 0]}
    >
      <HeaderHome></HeaderHome>
      <Layout
        style={{
          width: "100%",
          backgroundColor: "transparent",
          paddingRight: "145px",
          paddingLeft: "145px",
        }}
      >
        <HeaderShop></HeaderShop>
        <BannerShop></BannerShop>
        <ContentShop></ContentShop>
      </Layout>
      <Layout
        style={{
          width: "100%",
          backgroundColor: "white",
          paddingRight: "145px",
          paddingLeft: "145px",
        }}
      >
        <FooterHome></FooterHome>
      </Layout>
    </Space>
  );
}

export default Shop;
