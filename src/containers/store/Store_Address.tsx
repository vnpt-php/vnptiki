import { useState } from "react";
import Address from "../../components/store/Address";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store_Address() {
  const [defaultSelectedKey] = useState("30");

  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <Address />
    </StyledStoreContainer>
  );
}
export default Store_Address;
