import { useState } from "react";
import CreateProduct from "../../components/store/CreateProduct";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store_Create_Product() {
  const [defaultSelectedKey] = useState("8");

  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <CreateProduct />
    </StyledStoreContainer>
  );
}

export default Store_Create_Product;
