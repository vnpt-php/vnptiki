import { useState } from "react";
import Profile from "../../components/store/Profile";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store_Profile() {
  const [defaultSelectedKey] = useState("25");

  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <Profile />
    </StyledStoreContainer>
  );
}
export default Store_Profile;
