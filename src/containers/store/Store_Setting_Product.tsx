import { useState } from "react";
import { StyledStoreContainer } from "../../components/styled/styled-components";
import SidebarStore from "../../components/store/SidebarStore";
import SettingProduct from "../../components/store/SettingProduct";

function Store_Setting_Product() {
  const [defaultSelectedKey] = useState("10");

  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <SettingProduct />
    </StyledStoreContainer>
  );
}

export default Store_Setting_Product;
