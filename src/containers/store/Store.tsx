import { useState } from "react";
import Dashboard from "../../components/store/Dashboard";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store() {
  const [defaultSelectedKey] = useState("");
  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <Dashboard />
    </StyledStoreContainer>
  );
}

export default Store;
