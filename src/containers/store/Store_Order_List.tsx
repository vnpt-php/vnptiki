import { useState } from "react";
import OrderList from "../../components/store/OrderList";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store_Order_List() {
  const [defaultSelectedKey] = useState("4");

  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <OrderList />
    </StyledStoreContainer>
  );
}

export default Store_Order_List;
