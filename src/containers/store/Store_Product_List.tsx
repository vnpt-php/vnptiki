import { useState } from "react";
import ProductList from "../../components/store/ProductList";
import SidebarStore from "../../components/store/SidebarStore";
import { StyledStoreContainer } from "../../components/styled/styled-components";

function Store_Product_List() {
  const [defaultSelectedKey] = useState("7");
  return (
    <StyledStoreContainer>
      <SidebarStore defaultSelectedKey={defaultSelectedKey} />
      <ProductList />
    </StyledStoreContainer>
  );
}

export default Store_Product_List;
