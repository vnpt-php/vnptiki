import OrderManagement from "../../components/customer/OrderManagement";
import AccountSidebar from "../../components/customer/AccountSidebar";
import { Breadcrumb, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  StyledAccountContainer,
  StyledAccountLayout,
  StyledAccountBreadcrumb,
} from "../../components/styled/styled-components";
import { useState } from "react";
function Order_Management() {
  const [defaultSelectedKey] = useState("3");

  return (
    <main>
      <StyledAccountContainer>
        <StyledAccountLayout>
          <StyledAccountBreadcrumb>
            <Breadcrumb
              items={[
                {
                  title: <Link to="/">Trang chủ</Link>,
                },
                {
                  title: <Typography.Text>Đơn hàng của tôi</Typography.Text>,
                },
              ]}
            />
          </StyledAccountBreadcrumb>

          <AccountSidebar defaultSelectedKey={defaultSelectedKey} />

          <div style={{ flex: "1 1 0%" }}>
            <OrderManagement />
          </div>
        </StyledAccountLayout>
      </StyledAccountContainer>
    </main>
  );
}

export default Order_Management;
