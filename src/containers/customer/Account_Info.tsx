import AccountInfo from "../../components/customer/AccountInfo";
import AccountSidebar from "../../components/customer/AccountSidebar";
import { Breadcrumb, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  StyledAccountContainer,
  StyledAccountLayout,
  StyledAccountBreadcrumb,
} from "../../components/styled/styled-components";
import { useState } from "react";

function Account_Info() {
  const [defaultSelectedKey] = useState("1");

  return (
    <main>
      <StyledAccountContainer>
        <StyledAccountLayout>
          <StyledAccountBreadcrumb>
            <Breadcrumb
              items={[
                {
                  title: <Link to="/">Trang chủ</Link>,
                },
                {
                  title: <Typography.Text>Thông tin tài khoản</Typography.Text>,
                },
              ]}
            />
          </StyledAccountBreadcrumb>

          <AccountSidebar defaultSelectedKey={defaultSelectedKey} />

          <div style={{ flex: "1 1 0%" }}>
            <AccountInfo />
          </div>
        </StyledAccountLayout>
      </StyledAccountContainer>
    </main>
  );
}

export default Account_Info;
