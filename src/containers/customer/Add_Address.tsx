import AddAddress from "../../components/customer/AddAddress";
import AccountSidebar from "../../components/customer/AccountSidebar";
import { Breadcrumb, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  StyledAccountContainer,
  StyledAccountLayout,
  StyledAccountBreadcrumb,
} from "../../components/styled/styled-components";
import { useState } from "react";

function Add_Address() {
  const [defaultSelectedKey] = useState("");

  return (
    <main>
      <StyledAccountContainer>
        <StyledAccountLayout>
          <StyledAccountBreadcrumb>
            <Breadcrumb
              items={[
                {
                  title: <Link to="/">Trang chủ</Link>,
                },
                {
                  title: <Typography.Text>Tạo sổ địa chỉ</Typography.Text>,
                },
              ]}
            />
          </StyledAccountBreadcrumb>

          <AccountSidebar defaultSelectedKey={defaultSelectedKey} />

          <div style={{ flex: "1 1 0%" }}>
            <AddAddress />
          </div>
        </StyledAccountLayout>
      </StyledAccountContainer>
    </main>
  );
}

export default Add_Address;
