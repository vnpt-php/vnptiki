import EditAddress from "../../components/customer/EditAddress";
import AccountSidebar from "../../components/customer/AccountSidebar";
import { Breadcrumb, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  StyledAccountContainer,
  StyledAccountLayout,
  StyledAccountBreadcrumb,
} from "../../components/styled/styled-components";
import { useState } from "react";

function Edit_Address() {
  const [defaultSelectedKey] = useState("");

  return (
    <main>
      <StyledAccountContainer>
        <StyledAccountLayout>
          <StyledAccountBreadcrumb>
            <Breadcrumb
              items={[
                {
                  title: <Link to="/">Trang chủ</Link>,
                },
                {
                  title: (
                    <Typography.Text>Chỉnh sửa sổ địa chỉ</Typography.Text>
                  ),
                },
              ]}
            />
          </StyledAccountBreadcrumb>

          <AccountSidebar defaultSelectedKey={defaultSelectedKey} />

          <div style={{ flex: "1 1 0%" }}>
            <EditAddress />
          </div>
        </StyledAccountLayout>
      </StyledAccountContainer>
    </main>
  );
}

export default Edit_Address;
