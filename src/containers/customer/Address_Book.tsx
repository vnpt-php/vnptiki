import AddressBook from "../../components/customer/AddressBook";
import AccountSidebar from "../../components/customer/AccountSidebar";
import { Breadcrumb, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  StyledAccountContainer,
  StyledAccountLayout,
  StyledAccountBreadcrumb,
} from "../../components/styled/styled-components";
import { useState } from "react";

function Address_Book() {
  const [defaultSelectedKey] = useState("5");

  return (
    <main>
      <StyledAccountContainer>
        <StyledAccountLayout>
          <StyledAccountBreadcrumb>
            <Breadcrumb
              items={[
                {
                  title: <Link to="/">Trang chủ</Link>,
                },
                {
                  title: <Typography.Text>Sổ địa chỉ</Typography.Text>,
                },
              ]}
            />
          </StyledAccountBreadcrumb>

          <AccountSidebar defaultSelectedKey={defaultSelectedKey} />

          <div style={{ flex: "1 1 0%" }}>
            <AddressBook />
          </div>
        </StyledAccountLayout>
      </StyledAccountContainer>
    </main>
  );
}

export default Address_Book;
