import { Layout, Space } from "antd";
import FooterHome from "../components/FooterHome";
import HeaderHome from "../components/HeaderHome";
import ContentProductType from "../components/producttype/ContentProductType";
import SiderHome from "../components/SiderHome";
import CartContext from "../context/CartContext";
import { Cart } from "../models/types/Cart";
import { useState } from "react";
import BannerShop from "../components/shop/BannerShop";

function ProductType() {
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  return (
    <Space
      direction="vertical"
      style={{
        width: "100%",
        backgroundColor: "#f5f5fa",
      }}
      size={[0, 0]}
    >
      <CartContext.Provider value={{ dataCart, setDataCart }}>
        <HeaderHome />
      </CartContext.Provider>

      <Layout
        style={{
          display: "flex",
          flexDirection: "unset",
          paddingTop: "16px",
          justifyContent: "space-between",
          backgroundColor: "transparent",
          width: "1440px",
          paddingLeft: "24px",
          paddingRight: "24px",
          marginRight: "40px",
          marginLeft: "40px",
          boxSizing: "border-box",
        }}
      >
        <SiderHome></SiderHome>
        <Layout
          style={{
            maxWidth: "calc(100% - 254px)",
          }}
        >
          <BannerShop />
          <ContentProductType />
          <FooterHome></FooterHome>
        </Layout>
      </Layout>
    </Space>
  );
}

export default ProductType;
