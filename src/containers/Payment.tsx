import { Layout, Space } from 'antd'
import React from 'react'
import PaymentHeader from '../components/payment/PaymentHeader'
import PaymentContent from '../components/payment/PaymentContent'

function Payment() {
    return (
        <Space
            direction="vertical"
            style={{
                width: '100%',
                backgroundColor: '#f5f5fa'
            }}
            size={[0, 0]}>
            <PaymentHeader></PaymentHeader>
            <Layout
                style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    minHeight: 'calc(100vh - 260px)',
                    paddingTop: '20px',
                    paddingBottom: '80px',
                    width: '1110px',
                    paddingLeft: '15px',
                    paddingRight: '15px',
                    marginRight: 'auto',
                    marginLeft: 'auto',
                }}>
                <PaymentContent></PaymentContent>
            </Layout>
        </Space >
    )
}

export default Payment