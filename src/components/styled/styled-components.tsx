import styled from "styled-components";

// alert dialog
export const StyledAlertWrapper = styled.div`
  position: fixed;
  top: 20px;
  right: 20px;
  z-index: 9999;
`;
///
export const StyledUniversalFreeShipPlus = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: row;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  padding: 8px 16px;
  gap: 4px;
  background: white;
  border-width: 1px 0px;
  border-style: solid;
  border-color: rgb(235, 235, 240);
  background-color: rgb(255, 232, 128);
  height: 21px;
`;

export const StyledAccountContainer = styled.div`
  width: 1270px;
  padding-left: 15px;
  padding-right: 15px;
  margin-right: auto;
  margin-left: auto;
`;

export const StyledAccountLayout = styled.div`
  display: flex;
  width: 100%;
  margin: 0px auto 20px;
  flex-wrap: wrap;
`;

export const StyledAccountBreadcrumb = styled.div`
  width: 1270px;
  padding-left: 15px;
  padding-right: 15px;
  margin-right: auto;
  margin-left: auto;
  height: 40px;
  display: flex;
  align-items: center;
`;

// Orders & Products
export const StyledPageContainer = styled.div`
  position: relative;
  -webkit-box-flex: 1;
  flex: 1;
  margin: 0 auto;
  min-width: 0;
  padding-bottom: 72px;
  padding-left: 230px;
`;

export const StyledPageContentWrapper = styled.div`
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  margin: 0 auto;
  width: 1104px;
  min-height: calc(100vh - 75px - 72px);
`;

export const StyledProductList = styled.div`
  margin: 0 auto;
  width: 1104px;
  background: #f6f6f6;
`;

export const StyledProductFilterCard = styled.div`
  margin-bottom: 16px;
  padding: 24px;
  background: #fff;
  box-shadow: 0 1px 4px 0 #0002;
  border-radius: 4px;
  position: relative;
  overflow: hidden;
`;

export const StyledProductSection = styled.div`
  border-radius: 4px;
  border: 1px solid #e5e5e5;
  margin: 0 24px 24px 24px;
`;

// Store
export const StyledStoreContainer = styled.div`
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
  flex-direction: row;
  padding-top: 56px;
`;

export const StyledProductBasicInfo = styled.div`
  overflow: hidden;
  width: 1104px;
  padding: 24px 24px 0;
  background: #fff;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.12);
  border-radius: 2px;
`;

export const StyledProductRow = styled.div`
  margin-bottom: 24px;
  display: flex;
  flex-wrap: wrap;
  -webkit-box-align: start;
  align-items: flex-start;
  -webkit-box-pack: start;
  justify-content: flex-start;
`;

export const StyledProductTitle = styled.div`
  display: flex;
  -webkit-box-pack: end;
  justify-content: flex-start;
  -webkit-box-align: center;
  align-items: center;
  position: relative;
  min-height: 40px;
  text-align: right;
  color: #333;
  flex-basis: 144px;
  width: 144px;
  max-width: 144px;
  margin-right: 16px;
`;
