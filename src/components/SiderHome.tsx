import { Typography, Button } from "antd";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { baseUrl } from "../constants/url";
import { ApiResponse } from "../models/ApiResponse";
import { Category } from "../models/types/Category";
import slugify from "slugify";

function SiderHome() {
  const [dataList, setDataList] = useState<Category[]>([]);

  const getCategories = () => {
    axios
      .get<ApiResponse<Category[]>>(`${baseUrl}/api/categories`, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        setDataList(res.data.data);
      })
      .catch((err) => {
        console.error(err);
      });
  };
  useEffect(() => {
    getCategories();
  }, []);

  return (
    <div
      className="slider-bar"
      style={{
        textAlign: "center",
        color: "#fff",
        width: "230px",
        overflow: "auto",
        position: "sticky",
        maxHeight: "500px",
        top: "16px",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "8px",
      }}
    >
      <div
        style={{
          padding: "12px 8px",
        }}
      >
        <div
          style={{
            paddingLeft: "16px",
            marginBottom: "8px",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
            }}
          >
            Danh mục
          </Typography.Title>
        </div>
        {dataList.map((item, key) => (
          <div
            key={key}
            style={{
              lineHeight: 0,
              justifyContent: "left",
              alignItems: "center",
            }}
          >
            <Link
              rel="noopener noreferrer"
              to={`/products/${slugify(item.name, {
                lower: true,
                locale: "vi",
                remove: /[*+~.()'"!:@]/g,
              }).replace(/\s+/g, "-")}/${item.id}`}
            >
              <Button
                style={{
                  height: "46px",
                  display: "flex",
                  width: "100%",
                  alignItems: "center",
                }}
                type="text"
              >
                <Typography.Text>{item.name}</Typography.Text>
              </Button>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}

export default SiderHome;
