import { Typography } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import {
  StyledPageContainer,
  StyledPageContentWrapper,
  StyledProductList,
} from "../styled/styled-components";
import { CSSProperties, useContext } from "react";
import ProductContext from "../../context/ProductContext";

const StyledTodoBox = styled.div`
  max-height: 154px;
  overflow: hidden;
  transition: max-height 0.3s ease 0s;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: -24px;
`;

const StyledTodoBoxItem = styled.div`
  width: calc(1104px / 3);
  padding: 8px 0;
  margin-bottom: 15px;
  position: relative;
  text-align: center;
  cursor: pointer;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  user-select: none;

  ::after {
    content: "";
    width: 1px;
    height: 35px;
    background-color: #e5e5e5;
    position: absolute;
    right: 0;
    top: 50%;
    margin-top: -18px;
  }

  :nth-child(3n)::after {
    width: 0;
  }

  :hover {
    background-color: #f2f1f1c3;
    border-radius: 2px;
  }
`;

const styles = {
  card: {
    padding: "24px 24px 8px",
    background: "#fff",
    marginTop: "16px",
    borderRadius: "2px",
    boxShadow: "0 1px 4px 0 rgba(0, 0, 0, .1)",
  },

  itemTitle: {
    fontSize: "16px",
    minHeight: "18px",
    color: "#2673dd",
    fontWeight: 600,
  },

  itemDesc: {
    fontSize: "12px",
    color: "#333",
    maxHeight: "42px",
    overflow: "hidden",
    padding: "0 12px",
    display: "flex",
    justifyContent: "center",
    position: "relative",
  } as CSSProperties,
};

const Dashboard: React.FC = () => {
  const { dataProduct } = useContext(ProductContext);

  return (
    <StyledPageContainer>
      <StyledPageContentWrapper>
        <StyledProductList style={styles.card}>
          <Typography.Title level={4} style={{ marginTop: 0 }}>
            Danh sách cần làm
            <Typography.Paragraph
              style={{
                fontSize: 12,
                fontWeight: 400,
                color: "#999",
                margin: "6px 0 0 0",
              }}
            >
              Những việc bạn sẽ phải làm
            </Typography.Paragraph>
          </Typography.Title>

          <div style={{ margin: "24px 0" }}>
            <StyledTodoBox>
              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Chờ xác nhận
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Chờ lấy hàng
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Đã xử lý
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Đơn huỷ
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Sản phẩm bị tạm khoá
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Link to="" style={{ textDecoration: "none" }}>
                  <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                  <Typography.Text style={styles.itemDesc}>
                    Sản phẩm hết hàng
                  </Typography.Text>
                </Link>
              </StyledTodoBoxItem>
            </StyledTodoBox>
          </div>
        </StyledProductList>

        <StyledProductList style={styles.card}>
          <Typography.Title level={4} style={{ marginTop: 0 }}>
            Tổng quan
            <Typography.Paragraph
              style={{
                fontSize: 12,
                fontWeight: 400,
                color: "#999",
                margin: "6px 0 0 0",
              }}
            >
              Tổng quan dữ liệu của cửa hàng
            </Typography.Paragraph>
          </Typography.Title>

          <div style={{ margin: "24px 0" }}>
            <StyledTodoBox>
              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Lượt truy cập
                </Typography.Text>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Lượt xem
                </Typography.Text>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Đơn hàng
                </Typography.Text>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Doanh thu
                </Typography.Text>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>
                  {dataProduct.length}
                </Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Sản phẩm
                </Typography.Text>
              </StyledTodoBoxItem>

              <StyledTodoBoxItem>
                <Typography.Text style={styles.itemTitle}>0</Typography.Text>
                <Typography.Text style={styles.itemDesc}>
                  Đăng ký
                </Typography.Text>
              </StyledTodoBoxItem>
            </StyledTodoBox>
          </div>
        </StyledProductList>
      </StyledPageContentWrapper>
    </StyledPageContainer>
  );
};

export default Dashboard;
