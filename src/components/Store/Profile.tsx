import { Button, Form, Input, Tabs, Typography } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import { useState, CSSProperties, useContext, useEffect } from "react";
import styled from "styled-components";
import {
  StyledPageContainer,
  StyledPageContentWrapper,
} from "../styled/styled-components";
import StoreContext from "../../context/StoreContext";

const StyledStoreProfile = styled.div`
  margin-top: 16px;
  background-color: rgb(255, 255, 255);
  box-shadow: rgba(0, 0, 0, 0.09) 0px 0.2rem 0.4rem;
`;

const StyledStoreProfileHeader = styled.div`
  position: relative;
  display: flex;
  padding: 24px;
  padding-bottom: 0;
`;

const StyledFormItem = styled.div`
  display: flex;
  font-size: 14px;
  transition: margin-bottom 0s 0.2s;
`;

const StyledFormItemContent = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-flex: 1;
  line-height: 32px;

  ::before {
    display: table;
    height: 32px;
    content: "";
  }
`;

const StyledAvatarEditor = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
`;

const StyledEditForm = styled.div`
  position: relative;
  width: 96px;
  height: 96px;
  overflow: hidden;
  background-repeat: no-repeat;
  background-position: 50% center;
  background-size: cover;
  border-radius: 50%;
`;

const styles = {
  editBtn: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: "24px",
    lineHeight: "24px",
    color: "rgb(255, 255, 255)",
    textAlign: "center",
    background: "rgba(0, 0, 0, .5)",
    cursor: "pointer",
  } as CSSProperties,
};

function Profile() {
  const { dataStore } = useContext(StoreContext);

  return (
    <StyledPageContainer>
      <StyledPageContentWrapper>
        <StyledStoreProfile>
          <StyledStoreProfileHeader>
            <div style={{ WebkitBoxFlex: 1, flex: "1 1 0%" }}>
              <Typography.Title level={4} style={{ margin: 0 }}>
                Hồ sơ cửa hàng
              </Typography.Title>

              <Typography.Text style={{ color: "rgb(153, 153, 153)" }}>
                Xem tình trạng Shop và cập nhật hồ sơ Shop của bạn
              </Typography.Text>
            </div>
          </StyledStoreProfileHeader>

          <Tabs defaultActiveKey="1" style={{ padding: "8px 24px 24px" }}>
            <TabPane tab="Thông tin cơ bản" key="1">
              <div>
                <Form autoComplete="off">
                  <StyledFormItem>
                    <Form.Item
                      label="Tên cửa hàng"
                      name=""
                      style={{ width: "68%", marginLeft: 150 }}
                    >
                      <div style={{ marginLeft: 12 }}>
                        <Input
                          disabled
                          value={dataStore && dataStore.name}
                          style={{ userSelect: "none" }}
                        />
                        <Typography.Paragraph
                          style={{
                            paddingTop: 4,
                            fontSize: 12,
                            color: "rgb(153, 153, 153)",
                          }}
                        >
                          Các lần cập nhật tên Shop phải cách nhau tối thiểu 30
                          ngày. Bạn có thể cập nhật tên Shop sau ngày ????
                        </Typography.Paragraph>
                      </div>
                    </Form.Item>
                  </StyledFormItem>

                  <StyledFormItem>
                    <Form.Item label="" name="">
                      <StyledFormItemContent>
                        <StyledAvatarEditor>
                          <Typography.Text
                            style={{
                              fontFamily:
                                "-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,'Noto Sans',sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji'",
                              margin: "0 0 0 143px",
                              width: "120px",
                            }}
                          >
                            Logo cửa hàng:
                          </Typography.Text>

                          <StyledEditForm
                            style={{
                              backgroundImage:
                                "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAMAAAAKE/YAAAABJlBMVEXP2NxgfYtjf412j5uMoavO19uis7vG0dVxi5fBzNJyjJhjgI56kp5hfoxxi5h5kZ2ouL9ohJGInaipucCHnah3kJyxv8bL1NnM1dlphJGXqrN4kZzM1tqUp7Gnt76InqlkgY6wvsWGnKdrhpOnuL+xwMZ8lJ+3xMvAzNGCmaTFz9Sdr7fN1trN19tlgY5qhZJkgI6/y9COoqzL1dlphJJ0jZp7k566x8y7x81ng5CywMegsrp4kZ14kJyhs7uJn6nH0dZlgY+Fm6bG0daWqrO2xMqzwcfF0NXBzdK3xcu0wsh7k5+qusG0wsmzwchuiJVviZaYq7S7yM2Kn6m8yM6/y9GInqiWqbKNoqxzjJmjtLyqucGfsbmdr7iwv8WKoKpuiZVif42PQ9RwAAABzUlEQVR4Xu3YVZLjShBA0UyBmZmamXuYmZmZ3tv/Jua3Y8JWTdsKKR1zzwruR1aVlDIdAAAAAAAAAAAAAAAAAADyN1t+3fPqfiOTl7kQ1qp6QqkZinmdnP6hcFVs6wY6RtATwx75OlY7K2ZlczpBzmx194ZONOyKTbc0QiAmrWikFTHoIKeRKgdiz7o6LIo919XhophzRp3yYs01dcqINavqtCTWXFAnX6y5ok7nxZqyOhWJNjAeHERzVx6Py7Y6PRZzqnP4wSQ1dajN4U9AIRSDOhqpIyYFGuGs2HRuqBMNR/O3rKlYXjG1dazhbTGsF4yd557YtlFwrHpNCpslPeHZ81DmQj7T8OvFYt9vfM3LPwkAgOzDzMLdUqV/qHqnXymtLmRe3hPD9h/s7RzpGEc7e1v7Yk+4dX/zWCMsbw6eiClPgzV107XPL8SI3cEr/Wuv3+xK+t4ueXoq3rv3kq4PrbKeWvnjtqRn9MnTqXiDkaTkUkGn9uWypKK5rDM4/CYp+K4z+iGJW9SZrSc+z57OzEt6rtsag6ok6qfGYkOS1NJY/JIk/aexyEmS/tdYHEuSNCbzGU000UQTTTQAAAAAAAAAAAAAAAAAAL8BwZgl987F+p8AAAAASUVORK5CYII=')",
                            }}
                          >
                            <label htmlFor="inputFile">
                              <Typography.Text style={styles.editBtn}>
                                Sửa
                              </Typography.Text>
                            </label>
                            <Input
                              id="inputFile"
                              type="file"
                              accept="image/jpg,image/png,image/jpeg"
                              style={{ display: "none" }}
                            />
                          </StyledEditForm>

                          <ul style={{ marginLeft: 16 }}>
                            <li
                              style={{
                                fontSize: 12,
                                color: "rgb(153, 153, 153)",
                                lineHeight: "14px",
                              }}
                            >
                              Recommended image dimensions: width 300px, height
                              300px
                            </li>
                            <li
                              style={{
                                fontSize: 12,
                                color: "rgb(153, 153, 153)",
                                lineHeight: "14px",
                              }}
                            >
                              Maximum file size: 2.0MB
                            </li>
                            <li
                              style={{
                                fontSize: 12,
                                color: "rgb(153, 153, 153)",
                                lineHeight: "14px",
                              }}
                            >
                              Image format accepted: JPG,JPEG,PNG
                            </li>
                          </ul>
                        </StyledAvatarEditor>
                      </StyledFormItemContent>
                    </Form.Item>
                  </StyledFormItem>

                  <StyledFormItem>
                    <Form.Item
                      label=""
                      name=""
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        width: "82%",
                      }}
                    >
                      <Button type="primary" style={{ width: 80 }}>
                        Lưu
                      </Button>
                    </Form.Item>
                  </StyledFormItem>
                </Form>
              </div>
            </TabPane>
          </Tabs>
        </StyledStoreProfile>
      </StyledPageContentWrapper>
    </StyledPageContainer>
  );
}

export default Profile;
