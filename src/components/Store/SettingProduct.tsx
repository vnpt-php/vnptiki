import { Alert, Button, Form, Input, Select, Typography } from "antd";
import {
  StyledAlertWrapper,
  StyledPageContainer,
  StyledPageContentWrapper,
  StyledProductBasicInfo,
  StyledProductRow,
} from "../styled/styled-components";
import CategoryContext from "../../context/Category";
import { useContext, useState, useCallback } from "react";
import { ApiResponse } from "../../models/ApiResponse";
import { Category } from "../../models/types/Category";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { useApi } from "../../hooks/useApi";
import BrandContext from "../../context/BrandContext";

const { Option } = Select;

const title = {
  fontSize: "20px",
  fontWeight: "600",
  lineHeight: "22px",
  color: "#333",
  margin: "0",
};

function SettingProduct() {
  const { dataCategory } = useContext(CategoryContext);
  const { dataBrand } = useContext(BrandContext);
  const { callApi } = useApi();

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  // const [selectedIdCate, setSelectedIdCate] = useState("");
  const [nameCategory, setNameCategory] = useState("");
  const [nameBrand, setNameBrand] = useState("");

  // const onChangeCate = (value: any) => {
  //   setSelectedIdCate(value);
  // };

  const onChangeNameCate = (value: any) => {
    setNameCategory(value);
  };

  const onChangeNameBrand = (value: any) => {
    setNameBrand(value);
  };

  const handleCreateCategory = useCallback(() => {
    const data = {
      name: nameCategory,
    };

    callApi<ApiResponse<Category>>(
      REQUEST_TYPE.POST,
      "api/category/create",
      data
    )
      .then((response) => {
        setSuccess("Thêm thể loại sản phẩm thành công");
      })
      .catch((error) => {
        setError("Thêm thể loại sản phẩm thất bại");
        console.log(error);
      });
  }, [callApi, nameCategory]);

  const handleCreateBrand = useCallback(() => {
    const data = {
      name: nameBrand,
    };

    callApi<ApiResponse<Category>>(REQUEST_TYPE.POST, "api/brand/create", data)
      .then((response) => {
        setSuccess("Thêm thương hiệu thành công");
      })
      .catch((error) => {
        setError("Thêm thương hiệu thất bại");
        console.log(error);
      });
  }, [callApi, nameBrand]);

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledPageContainer>
        <StyledPageContentWrapper style={{ minHeight: "auto" }}>
          <div style={{ margin: "16px auto 0" }}>
            <StyledProductBasicInfo>
              <div style={{ marginBottom: 24 }}>
                <Typography.Title level={5} style={title}>
                  Thêm thể loại sản phẩm
                </Typography.Title>
              </div>

              <Form>
                <StyledProductRow>
                  <Form.Item
                    label="Tên thể loại"
                    name="name-category"
                    rules={[
                      {
                        required: true,
                        message: "Chưa nhập tên thể loại",
                      },
                    ]}
                    style={{ width: "50%", marginLeft: 32 }}
                  >
                    <Input
                      placeholder="Nhập vào"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      onChange={(e) => onChangeNameCate(e.target.value)}
                    />
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      width: "53%",
                    }}
                  >
                    <Button
                      type="primary"
                      htmlType="submit"
                      onClick={handleCreateCategory}
                    >
                      Lưu
                    </Button>
                  </div>
                </StyledProductRow>
              </Form>
            </StyledProductBasicInfo>
          </div>
        </StyledPageContentWrapper>

        <StyledPageContentWrapper style={{ minHeight: "auto" }}>
          <div style={{ margin: "16px auto 0" }}>
            <StyledProductBasicInfo>
              <div style={{ marginBottom: 24 }}>
                <Typography.Title level={5} style={title}>
                  Thêm thương hiệu
                </Typography.Title>
              </div>

              <Form>
                <StyledProductRow>
                  <Form.Item
                    label="Tên thương hiệu"
                    name="name-brand"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "50%", marginLeft: 32 }}
                  >
                    <Input
                      placeholder="Nhập vào"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      onChange={(e) => onChangeNameBrand(e.target.value)}
                    />
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      width: "53%",
                    }}
                  >
                    <Button
                      type="primary"
                      htmlType="submit"
                      onClick={handleCreateBrand}
                    >
                      Lưu
                    </Button>
                  </div>
                </StyledProductRow>
              </Form>
            </StyledProductBasicInfo>
          </div>
        </StyledPageContentWrapper>

        <StyledPageContentWrapper style={{ minHeight: "auto" }}>
          <div style={{ margin: "16px auto 0" }}>
            <StyledProductBasicInfo>
              <div style={{ marginBottom: 24 }}>
                <Typography.Title level={5} style={title}>
                  Thêm thương hiệu vào thể loại sản phẩm
                </Typography.Title>
              </div>

              <Form>
                {/* <StyledProductRow>
                <Form.Item
                  label="Thể loại"
                  name="category"
                  rules={[
                    {
                      required: true,
                      message: "",
                    },
                  ]}
                  style={{ width: "50%", marginLeft: 32 }}
                >
                  <Input
                    placeholder="Nhập vào"
                    style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                  />
                </Form.Item>
              </StyledProductRow> */}

                <StyledProductRow>
                  <Form.Item
                    label="Thể loại"
                    name="category"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "50%", marginLeft: 60 }}
                  >
                    <Select
                      placeholder="Chọn"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      // onChange={onChangeCate}
                    >
                      {dataCategory &&
                        dataCategory.map((item) => (
                          <Option key={item.id} value={item.id}>
                            {item.name}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Thương hiệu"
                    name="brand"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "50%", marginLeft: 32 }}
                  >
                    <Select
                      placeholder="Chọn"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      // onChange={}
                    >
                      {dataBrand &&
                        dataBrand.map((item) => (
                          <Option key={item.id} value={item.id}>
                            {item.name}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "flex-end",
                      width: "53%",
                    }}
                  >
                    <Button type="primary">Lưu</Button>
                  </div>
                </StyledProductRow>
              </Form>
            </StyledProductBasicInfo>
          </div>
        </StyledPageContentWrapper>
      </StyledPageContainer>
    </>
  );
}

export default SettingProduct;
