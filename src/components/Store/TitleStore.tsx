import { Helmet } from "react-helmet";

function TitleStore() {
  return (
    <Helmet>
      <title>VNPTiki - Kênh người bán</title>
    </Helmet>
  );
}

export default TitleStore;
