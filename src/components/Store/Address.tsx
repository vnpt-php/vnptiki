import { useContext } from "react";
import styled from "styled-components";
import {
  StyledPageContainer,
  StyledPageContentWrapper,
} from "../styled/styled-components";
import StoreContext from "../../context/StoreContext";
import { SlLocationPin } from "react-icons/sl";
import { Typography } from "antd";

const StyledStoreProfile = styled.div`
  margin-top: 16px;
  background-color: rgb(255, 255, 255);
  box-shadow: rgba(0, 0, 0, 0.09) 0px 0.2rem 0.4rem;
`;

const StyledStoreProfileHeader = styled.div`
  position: relative;
  display: flex;
  padding: 24px;
  padding-bottom: 0;
`;

const StyledView = styled.div`
  clear: both;
  transition: color 0.3s ease;
  border-bottom: 1px solid #fafafa;
  display: flex;
  margin: 0 0 0 40px;
  padding: 20px 32px 20px 0;
`;

const StyledViewContent = styled.div`
  min-width: 0;
  -webkit-box-flex: 1;
  flex: 1;
  margin-left: 34px;
`;

const StyledContentGrid = styled.div`
  display: -webkit-box;
  display: flex;
  line-height: 28px;
  margin-bottom: 4px;
  flex-wrap: wrap;
  align-content: normal;
  -webkit-box-align: start;
  align-items: flex-start;
  -webkit-box-pack: start;
  justify-content: flex-start;
`;

const StyledContentLabel = styled.div`
  width: 110px;
  font-size: 14px;
  margin-right: 20px;
  color: rgba(85, 85, 85, 0.8);
  font-weight: 600;
`;

const StyledViewName = styled.div`
  display: inline-block;
  font-weight: 500;
  margin-right: 16px;
  font-size: 16px;
  line-height: 24px;
`;

const StyledDetailAddress = styled.div`
  white-space: pre-wrap;
  margin-bottom: 0;
  width: 700px;
  line-height: 24px;
`;

const styles = {
  viewIcon: {
    width: "32px",
    height: "32px",
    marginTop: "10px",
    borderRadius: "50%",
    color: "green",
  },
};

function Address() {
  const { dataStore } = useContext(StoreContext);

  return (
    <StyledPageContainer>
      <StyledPageContentWrapper>
        <StyledStoreProfile>
          <StyledStoreProfileHeader>
            <div style={{ WebkitBoxFlex: 1, flex: "1 1 0%" }}>
              <Typography.Title level={4} style={{ margin: 0 }}>
                Địa chỉ
              </Typography.Title>

              <Typography.Text style={{ color: "rgb(153, 153, 153)" }}>
                Quản lý việc vận chuyển và địa chỉ giao hàng của bạn
              </Typography.Text>
            </div>
          </StyledStoreProfileHeader>

          <StyledView>
            <SlLocationPin style={styles.viewIcon} />
            <StyledViewContent>
              <StyledContentGrid>
                <StyledContentLabel>Tên cửa hàng</StyledContentLabel>
                <StyledViewName> {dataStore?.name}</StyledViewName>
              </StyledContentGrid>

              <StyledContentGrid>
                <StyledContentLabel>Địa chỉ</StyledContentLabel>
                <StyledDetailAddress>{dataStore?.address}</StyledDetailAddress>
              </StyledContentGrid>
            </StyledViewContent>
          </StyledView>
        </StyledStoreProfile>
      </StyledPageContentWrapper>
    </StyledPageContainer>
  );
}

export default Address;
