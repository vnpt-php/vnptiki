import styled from "styled-components";
import { Button, Popover, Typography } from "antd";
import { IoIosArrowDown, IoIosLogOut } from "react-icons/io";
import { Link, useNavigate } from "react-router-dom";
import { useCallback, useEffect } from "react";
import { User } from "../../models/types/User";

const StyledHeaderWrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledHeaderLogo = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  margin-left: 16px;
`;

const StyledHeaderUser = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 4px 16px 4px 4px;

  :hover {
    opacity: 0.7;
  }
`;

const styles = {
  logo: {
    width: "42.75px",
    height: "30px",
    marginTop: "-8px",
  },

  title: {
    fontSize: "18px",
    color: "#1d1d1d",
    margin: "2px 0 0 5px",
    fontWeight: "500",
  },

  avatar: {
    display: "inline-block",
    width: "32px",
    height: "32px",
    borderRadius: "50%",
    marginRight: "6px",
  },

  username: {
    fontSize: "14px",
    color: "#333",
    fontWeight: "400",
  },

  iconArrow: {
    width: "16px",
    height: "16px",
    marginLeft: "4px",
    lineHeight: "0",
  },

  btn: {
    height: "100%",
    display: "flex",
    alignItems: "center",
  },

  iconLogout: {
    width: "18px",
    height: "18px",
    marginRight: "8px",
  },
};

interface ProductUserPops {
  dataUser: User | undefined;
}

function RegistrationHeader({ dataUser }: ProductUserPops) {
  const navigate = useNavigate();

  const token = sessionStorage.getItem("ACCESS_TOKEN");

  const handleLogout = useCallback(() => {
    sessionStorage.removeItem("ACCESS_TOKEN");
    navigate("/");
  }, [navigate]);

  const content = (
    <Button type="ghost" style={styles.btn} onClick={handleLogout}>
      <IoIosLogOut style={styles.iconLogout} />
      <Typography.Text>Đăng xuất</Typography.Text>
    </Button>
  );

  useEffect(() => {
    if (token) {
      if (dataUser && dataUser.store) {
        navigate("/store");
      }
    } else if (!token) {
      navigate("/");
    }
  }, [navigate, token]);

  return (
    <StyledHeaderWrapper>
      <StyledHeaderLogo>
        <div className="col-5"></div>
        <Link to="/">
          <img
            src="https://salt.tikicdn.com/ts/upload/e4/49/6c/270be9859abd5f5ec5071da65fab0a94.png"
            alt="Logo"
            style={styles.logo}
          />
        </Link>

        <Typography.Title level={4} style={styles.title}>
          Đăng ký cửa hàng trở thành Người bán trên Tiki
        </Typography.Title>
      </StyledHeaderLogo>

      <Popover
        content={content}
        placement="bottom"
        trigger="hover"
        overlayStyle={{
          position: "fixed",
        }}
      >
        <StyledHeaderUser>
          <img
            src={
              dataUser && dataUser.avatar
                ? `${dataUser.avatar}`
                : `https://salt.tikicdn.com/desktop/img/avatar.png`
            }
            alt=""
            style={styles.avatar}
          />

          <Typography.Text style={styles.username}>
            {dataUser?.name}
          </Typography.Text>

          <IoIosArrowDown style={styles.iconArrow} />
        </StyledHeaderUser>
      </Popover>
    </StyledHeaderWrapper>
  );
}

export default RegistrationHeader;
