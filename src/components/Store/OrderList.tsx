import {
  Button,
  Input,
  Tabs,
  Form,
  Typography,
  Tag,
  Select,
  Alert,
} from "antd";
import Table, { ColumnsType } from "antd/es/table";
import TabPane from "antd/es/tabs/TabPane";
import { useState, useEffect, useCallback } from "react";
import CurrencyFormat from "react-currency-format";
import {
  StyledAlertWrapper,
  StyledPageContainer,
  StyledPageContentWrapper,
  StyledProductList,
  StyledProductFilterCard,
  StyledProductSection,
} from "../styled/styled-components";
import { tinhsContrastColor } from "../../utils/tinhsContrastColor";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { useApi } from "../../hooks/useApi";
import { ApiResponse } from "../../models/ApiResponse";
import { Order } from "../../models/types/Order";
import { OrderItem } from "../../models/types/OrderItem";

const { Option } = Select;

const styles = {
  productListMain: {
    boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.1)",
    borderRadius: "4px",
    overflow: "hidden",
    background: "#fff",
    padding: "0 24px",
  },

  product: {
    fontSize: "14px",
    padding: "4px 8px",
    textOverflow: "ellipsis",
    overflow: "hidden",
    maxWidth: "147px",
  },
};

interface DataType {
  key: string;
  name: string[];
  quantity: number[];
  amount: number[];
  status: string;
}

function OrderList() {
  const { callApi } = useApi();

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [searchOrder, setSearchOrder] = useState("");
  const [filteredData, setFilteredData] = useState<DataType[]>([]);
  const [searchError, setSearchError] = useState("");
  const [dataOrders, setDataOrders] = useState<Order[]>([]);

  const randomColor = () => {
    const colors = ["#f50", "#2db7f5", "#87d068", "#108ee9", "#eb2f96"];
    return colors[Math.floor(Math.random() * colors.length)];
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "Hóa đơn",
      dataIndex: "key",
      key: "key",
    },
    {
      title: "Sản phẩm",
      dataIndex: "name",
      key: "name",
      render: (name: string, record: DataType) => (
        <Tag
          key={name}
          style={{
            ...styles.product,
            background: randomColor(),
            color: tinhsContrastColor(randomColor()),
          }}
        >
          {`(${record.quantity}) ${record.name}`}
        </Tag>
      ),
    },
    {
      title: "Tổng tiền",
      dataIndex: "amount",
      key: "amount",
      render: (text: number) => (
        <CurrencyFormat
          value={text.toString()}
          displayType={"text"}
          thousandSeparator={true}
          suffix={"₫"}
        />
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      render: (status: string) => {
        let color;
        switch (status) {
          case "ordered":
            color = "processing";
            break;
          case "delivering":
            color = "blue";
            break;
          case "delivered":
            color = "success";
            break;
          case "canceled":
            color = "error";
            break;
          default:
            color = "default";
        }
        return <Tag color={color}>{status}</Tag>;
      },
    },
    {
      title: "Thao tác",
      key: "action",
      render: (text: string, record: DataType) => (
        <Select
          placeholder="Chỉnh trạng thái"
          style={{ width: 145 }}
          onChange={(value: string) =>
            handleOrderStatusChange(record.key, value)
          }
        >
          <Option value="ordered">Đã đặt</Option>
          <Option value="delivering">Đang giao</Option>
          <Option value="delivered">Đã giao</Option>
          <Option value="canceled">Đã huỷ</Option>
        </Select>
      ),
    },
  ];

  const data: DataType[] = dataOrders.map((data: Order) => ({
    key: data.id,
    name: data.order_items.map((items: OrderItem) => items.product.name),
    quantity: data.order_items.map((items: OrderItem) => items.quantity),
    amount: data.order_items.map(
      (items: OrderItem) => items.quantity * items.product.price
    ),
    status: data.status,
  }));

  const handleOrderStatusChange = (key: string, status: string) => {
    const myData = {
      id: key,
      status: status,
    };
    callApi<ApiResponse<Order>>(
      REQUEST_TYPE.PUT,
      "api/order/change-status",
      myData
    )
      .then((response) => {
        setSuccess("Thay đổi trạng thái thành công");
        window.location.reload();
      })
      .catch((error) => {
        setError("Thay đổi trạng thái thất bại");
        console.log(error);
      });
  };

  const handleSearchOrderChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchOrder(e.target.value);
  };

  const handleSearchButtonClick = (e: any) => {
    if (searchOrder.length === 0) {
      setSearchError("Chưa nhập mã đơn hàng");
    } else if (searchOrder.length < 3) {
      setSearchError("Tối thiểu nhập 3 ký tự trở lên");
    } else {
      handleFilterOrder();
    }
  };

  const handleSearchOrderRefresh = () => {
    // setSearchOrder("");
  };

  const handleFilterOrder = () => {
    const filtered = data.filter((order) =>
      order.key.toLowerCase().includes(searchOrder.toLowerCase())
    );
    setFilteredData(filtered);
  };

  const getDataOrders = useCallback(() => {
    callApi<ApiResponse<Order[]>>(REQUEST_TYPE.GET, "api/order/store-orders")
      .then((response) => {
        setDataOrders(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi, setDataOrders]);

  useEffect(() => {
    getDataOrders();

    if (searchError !== "") {
      const timeoutId = setTimeout(() => {
        setSearchError("");
      }, 3000);

      return () => clearTimeout(timeoutId);
    }
  }, [searchError]);

  return (
    <>
      <StyledAlertWrapper>
        {searchError && (
          <Alert
            message={searchError}
            type="warning"
            showIcon
            onClose={() => setSearchError("")}
            closable
          />
        )}
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledPageContainer>
        <StyledPageContentWrapper>
          <div style={{ margin: "16px auto 0" }}>
            <StyledProductList>
              <StyledProductFilterCard>
                <Form autoComplete="off">
                  <Form.Item label="" name="">
                    <Typography.Title level={5} style={{ margin: 0 }}>
                      Tìm kiếm đơn hàng
                    </Typography.Title>
                  </Form.Item>

                  <Form.Item label="Tên sản phẩm" name="product-name">
                    <Input
                      minLength={2}
                      placeholder="Vui lòng nhập tối thiểu 3 ký tự"
                      value={searchOrder}
                      onChange={handleSearchOrderChange}
                    />
                  </Form.Item>

                  <Form.Item label="" name="" style={{ margin: 0 }}>
                    <div style={{ display: "flex" }}>
                      <Button
                        type="primary"
                        style={{ minWidth: 72, height: 32 }}
                        onClick={handleSearchButtonClick}
                      >
                        Tìm
                      </Button>
                      <Button
                        type="default"
                        style={{ minWidth: 72, height: 32, marginLeft: 16 }}
                        onClick={handleSearchOrderRefresh}
                      >
                        Đặt lại
                      </Button>
                    </div>
                  </Form.Item>
                </Form>
              </StyledProductFilterCard>

              <Tabs defaultActiveKey="1" style={styles.productListMain}>
                <TabPane tab="Tất cả" key="1">
                  <Typography.Title
                    level={4}
                    style={{ marginLeft: 24, marginBottom: 24 }}
                  >
                    {data.length} Đơn hàng
                  </Typography.Title>
                  <StyledProductSection>
                    <Table
                      columns={columns}
                      dataSource={filteredData.length ? filteredData : data}
                    />
                  </StyledProductSection>
                </TabPane>

                <TabPane tab="Đã đặt" key="2"></TabPane>

                <TabPane tab="Đang giao" key="3"></TabPane>

                <TabPane tab="Đã giao" key="4"></TabPane>

                <TabPane tab="Đơn huỷ" key="5"></TabPane>
              </Tabs>
            </StyledProductList>
          </div>
        </StyledPageContentWrapper>
      </StyledPageContainer>
    </>
  );
}

export default OrderList;
