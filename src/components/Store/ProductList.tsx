import { Button, Form, Input, Table, Tabs, Typography } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import styled from "styled-components";
import { ColumnsType } from "antd/es/table";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import CurrencyFormat from "react-currency-format";
import {
  StyledPageContainer,
  StyledPageContentWrapper,
  StyledProductList,
  StyledProductFilterCard,
  StyledProductSection,
} from "../styled/styled-components";
import ProductContext from "../../context/ProductContext";
import { Product } from "../../models/types/Product";

const StyledProductSelectFix = styled.div`
  position: relative;
  height: 64px;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 4px 0px;
`;

const StyledSelectedPanel = styled.div`
  position: absolute;
  bottom: 0;
  left: 50%;
  right: 0;
  width: 100%;
  height: 100%;
  background: #fff;
  transform: translateX(-50%);
  border-radius: 4px;
  overflow: hidden;
`;

const StyledSelected = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  flex-wrap: nowrap;
  -webkit-box-pack: justify;
  justify-content: space-between;
  height: inherit;
  font-size: 16px;
  padding: 0 50px;
`;

const styles = {
  productListMain: {
    boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.1)",
    borderRadius: "4px",
    overflow: "hidden",
    background: "#fff",
    padding: "0 24px",
  },
};

interface DataType {
  key: string;
  image: string;
  name: string;
  price: number;
  category: string;
  brand: string;
}

const ProductList: React.FC = () => {
  const { dataProduct } = useContext(ProductContext);

  const [searchNameProduct, setSearchNameProduct] = useState("");
  const [filteredData, setFilteredData] = useState<DataType[]>([]);
  const [selectedRows, setSelectedRows] = useState<DataType[]>([]);

  const columns: ColumnsType<DataType> = [
    {
      title: "Hình ảnh",
      dataIndex: "image",
      key: "image",
      render: (image: string) => (
        <img src={image} alt="" style={{ width: 100 }} />
      ),
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Giá",
      dataIndex: "price",
      key: "price",
      render: (text: number) => (
        <CurrencyFormat
          value={text}
          displayType={"text"}
          thousandSeparator={true}
          suffix={"₫"}
        />
      ),
    },
    {
      title: "Loại sản phẩm",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Thương hiệu",
      dataIndex: "brand",
      key: "brand",
    },
    {
      title: "Thao tác",
      key: "action",
      render: (_, record) => (
        // /store/product/:productId/edit
        <Button type="link">
          <Link to={`/store/product/${record.key}/edit`}>Cập nhật</Link>
        </Button>
      ),
    },
  ];

  const data: DataType[] = dataProduct.map((product: Product) => ({
    key: product.id,
    image: product.product_images[0]?.url,
    name: product.name,
    price: product.price,
    category: product.category.name,
    brand: product.brand.name,
  }));

  const handleSelectChange = (
    selectedRowKeys: React.Key[],
    selectedRows: DataType[]
  ) => {
    setSelectedRows(selectedRows);
  };

  const handleSelectAll = (
    selected: boolean,
    selectedRows: DataType[],
    changeRows: DataType[]
  ) => {
    setSelectedRows(selectedRows);
  };

  const handleDeleteAll = () => {
    setSelectedRows([]);
  };

  const handleSearchNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchNameProduct(e.target.value);
  };

  const handleSearchButtonClick = () => {
    if (searchNameProduct.length < 2) {
      return;
    } else {
      handleFilterProduct();
    }
  };

  const handleSearchNameRefresh = () => {
    setSearchNameProduct("");
  };

  const handleFilterProduct = () => {
    const filtered = data.filter((product) =>
      product.name.toLowerCase().includes(searchNameProduct.toLowerCase())
    );
    setFilteredData(filtered);
  };

  useEffect(() => {}, [searchNameProduct]);

  return (
    <StyledPageContainer>
      <StyledPageContentWrapper>
        <div style={{ margin: "16px auto 0" }}>
          <StyledProductList>
            <StyledProductFilterCard>
              <Form autoComplete="off">
                <Form.Item label="" name="">
                  <Typography.Title level={5} style={{ margin: 0 }}>
                    Tìm kiếm sản phẩm
                  </Typography.Title>
                </Form.Item>

                <Form.Item
                  label="Tên sản phẩm"
                  name="product-name"
                  rules={[
                    {
                      min: 2,
                      message: "Vui lòng nhập tối thiểu 2 ký tự",
                    },
                  ]}
                >
                  <Input
                    placeholder="Vui lòng nhập tối thiểu 2 ký tự"
                    value={searchNameProduct}
                    onChange={handleSearchNameChange}
                  />
                </Form.Item>

                <Form.Item label="" name="" style={{ margin: 0 }}>
                  <div style={{ display: "flex" }}>
                    <Button
                      type="primary"
                      style={{ minWidth: 72, height: 32 }}
                      onClick={handleSearchButtonClick}
                    >
                      Tìm
                    </Button>
                    <Button
                      type="default"
                      style={{ minWidth: 72, height: 32, marginLeft: 16 }}
                      onClick={handleSearchNameRefresh}
                    >
                      Nhập lại
                    </Button>
                  </div>
                </Form.Item>
              </Form>
            </StyledProductFilterCard>

            <Tabs defaultActiveKey="1" style={styles.productListMain}>
              <TabPane tab="Tất cả" key="1">
                <StyledProductSection>
                  <Table
                    rowSelection={{
                      type: "checkbox",
                      onChange: handleSelectChange,
                      onSelectAll: handleSelectAll,
                    }}
                    columns={columns}
                    dataSource={filteredData.length ? filteredData : data}
                  />

                  {selectedRows.length > 0 && (
                    <StyledProductSelectFix>
                      <StyledSelectedPanel>
                        <StyledSelected>
                          <Typography.Text>{`${selectedRows.length} sản phẩm đã được chọn`}</Typography.Text>
                          <div>
                            <Button
                              type="default"
                              onClick={handleDeleteAll}
                              disabled={selectedRows.length === 0}
                              style={{ minWidth: 72, height: 32 }}
                            >
                              Xoá
                            </Button>

                            <Button
                              type="default"
                              disabled={selectedRows.length === 0}
                              style={{
                                minWidth: 72,
                                height: 32,
                                marginLeft: 16,
                              }}
                            >
                              Ẩn
                            </Button>
                          </div>
                        </StyledSelected>
                      </StyledSelectedPanel>
                    </StyledProductSelectFix>
                  )}
                </StyledProductSection>
              </TabPane>

              <TabPane tab="Đang hoạt động" key="2"></TabPane>

              <TabPane tab="Hết hàng" key="3"></TabPane>

              <TabPane tab="Đã ẩn" key="4"></TabPane>
            </Tabs>
          </StyledProductList>
        </div>
      </StyledPageContentWrapper>
    </StyledPageContainer>
  );
};

export default ProductList;
