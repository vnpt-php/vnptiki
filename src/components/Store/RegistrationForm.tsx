import { Button, Form, Input, Steps, Typography, Select, Switch } from "antd";
import styled from "styled-components";
import { useState, CSSProperties, useEffect, useCallback } from "react";
import { VscClose } from "react-icons/vsc";
import subVN from "sub-vn";
import { BsFillCheckCircleFill } from "react-icons/bs";
import { User } from "../../models/types/User";
import { Link, useNavigate } from "react-router-dom";
import { Store } from "../../models/types/Store";
import { ApiResponse } from "../../models/ApiResponse";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { useApi } from "../../hooks/useApi";

const { Option } = Select;
const { Step } = Steps;

const StyledLayoutMain = styled.div`
  width: 1264px;
  margin: 0 auto;
  display: flex;
`;

const StyledLayoutContent = styled.div`
  background-color: #fff;
  box-shadow: 0 1px 0 rgba(0, 0, 0, 0.12);
  flex: 1;
`;

const StyledStepsHeader = styled.div`
  margin: 0 24px;
  border-bottom: 1px solid #e5e5e5;
`;

const StyledSteps = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin: 48px 0 28px;
`;

const StyledStepsWrap = styled.div`
  display: flex;
  justify-content: center;
  max-width: 1216px;
  min-width: 944px;
`;

const StyledStepContent = styled.div`
  position: relative;
  max-width: 944px;
  margin: 48px auto 40px;
  min-height: 198px;
`;

const StyledStepFooter = styled.div`
  display: flex;
  border-top: 1px solid #e5e5e5;
  justify-content: space-between;
  padding: 24px 0;
  margin: 0 24px;
  text-align: right;
`;

const StyledModal = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  overflow: hidden;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 999;
`;

const StyledModalContainer = styled.div`
  position: absolute;
  top: 40px;
  right: 0;
  bottom: 40px;
  left: 0;
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
`;

const StyledModalBox = styled.div`
  position: relative;
  display: inline-flex;
  max-height: 100%;
  padding: 0 24px;
  transform-origin: top;
`;

const StyledModalContent = styled.div`
  width: 600px;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  max-height: 100%;
  overflow-y: auto;
  background-color: #fff;
  border-radius: 3px;
  box-shadow: 0 1px 8px 0 rgba(0, 0, 0, 0.12);
`;

const StyledModalBody = styled.div`
  position: relative;
  -webkit-box-flex: 1;
  flex-grow: 1;
  min-height: 32px;
  padding: 0 24px;
  overflow: auto;
  font-size: 14px;
`;

const StyledModalFooter = styled.div`
  position: relative;
  flex-shrink: 0;
  padding: 24px;
  overflow: hidden;
`;

const StyledPanelHeader = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 24px 0 24px 0px;
`;

const StyledShippingContainer = styled.div`
  position: relative;
  padding-top: 24px;
  border-top: 1px solid rgb(229, 229, 229); ;
`;

const StyledShippingItemBox = styled.div`
  border: 1px solid #e8e8e8;
  padding: 0 16px;
  margin: 16px 0;
  border-radius: 4px;
`;

const StyledShippingItem = styled.div`
  display: flex;
  flex-wrap: wrap;
  -webkit-box-align: start;
  align-items: flex-start;
  align-content: normal;
  padding-left: 10px;
  -webkit-box-pack: justify;
  justify-content: space-between;
  padding: 20px 0;
`;

const StyledContentResultStep = styled.div`
  position: relative;
  max-width: 944px;
  min-height: 198px;
  margin: 48px auto 68px;
`;

const StyledResult = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
`;

const StyledResultContent = styled.div`
  display: flex;
  margin: 32px 0;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const styles = {
  textError: {
    paddingTop: "4px",
    fontSize: "12px",
    lineHeight: "1",
    color: "#ff4742",
    transformOrigin: "center top",
  },

  closeModal: {
    position: "absolute",
    top: "24px",
    right: "48px",
    width: "24px",
    height: "24px",
    padding: "4px",
    cursor: "pointer",
    boxSizing: "border-box",
    color: "#999",
    transition: "color .2s ease-in-out",
    lineHeight: "0",
    display: "inline-block",
  } as CSSProperties,

  btnModal: {
    minWidth: "72px",
    height: "32px",
    padding: "0 16px",
    position: "relative",
  } as CSSProperties,

  selectInput: {
    position: "fixed",
  } as CSSProperties,

  iconResult: {
    height: "80px",
    width: "80px",
    color: "rgba(85, 204, 119, 1)",
  },

  textResultDesc: {
    color: "#666",
    fontSize: "14px",
    fontWeight: "400",
    lineHeight: "20px",
    marginBottom: "24px",
    marginTop: 0,
    textAlign: "center",
  } as CSSProperties,

  btnResult: {
    height: "40px",
    minWidth: "80px",
    padding: "0 16px",
  },
};

interface ProductUserPops {
  dataUser: User | undefined;
}

function RegistrationForm({ dataUser }: ProductUserPops) {
  const { callApi } = useApi();
  const navigate = useNavigate();

  const [currentStep, setCurrentStep] = useState(0);
  const [selectedProvince, setSelectedProvince] = useState<string | null>(null);
  const [selectedDistrict, setSelectedDistrict] = useState<string | null>(null);
  const [selectedWard, setSelectedWard] = useState<string | null>(null);
  const [openModalAdd, setOpenModalAdd] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openButtonAdd, setOpenButtonAdd] = useState(true);
  const [openButtonEdit, setOpenButtonEdit] = useState(false);
  const [openAddress, setOpenAddress] = useState(false);
  const [buttonClicked, setButtonClicked] = useState(false);

  const [storeName, setStoreName] = useState("");
  const [detailAddressTemp, setDetailAddressTemp] = useState("");
  const [provinceNameTemp, setProvinceNameTemp] = useState("");
  const [districtNameTemp, setDistrictNameTemp] = useState("");
  const [wardNameTemp, setWardNameTemp] = useState("");
  const [detailAddress, setDetailAddress] = useState("");
  const [provinceName, setProvinceName] = useState("");
  const [districtName, setDistrictName] = useState("");
  const [wardName, setWardName] = useState("");
  const [address, setAddress] = useState("");

  const handleStoreNameChange = (value: any) => {
    setStoreName(value);
  };

  const handleDetailAddressChange = (value: any) => {
    setDetailAddressTemp(value);
  };

  const handleNext = () => {
    setCurrentStep(currentStep + 1);
  };

  const handlePrev = () => {
    setCurrentStep(currentStep - 1);
  };

  const handleSubmit = () => {
    handleNext();
  };

  const handleSaveButtonClick = useCallback(() => {
    setDetailAddress(detailAddressTemp);
    setWardName(wardNameTemp);
    setDistrictName(districtNameTemp);
    setProvinceName(provinceNameTemp);
    setAddress(
      `${detailAddress}, ${wardName}, ${districtName}, ${provinceName}`
    );
  }, [
    detailAddressTemp,
    wardNameTemp,
    districtNameTemp,
    provinceNameTemp,
    detailAddress,
    wardName,
    districtName,
    provinceName,
  ]);

  const handleProvinceChange = (value: any) => {
    const province = provinces.find((p: any) => p.code === value);
    setSelectedProvince(value);
    setProvinceNameTemp(province ? province.name : "");
    setSelectedDistrict(null);
    setSelectedWard(null);
  };

  const handleDistrictChange = (value: any) => {
    const district = districts.find((p: any) => p.code === value);
    setSelectedDistrict(value);
    setDistrictNameTemp(district ? district.name : "");
    setSelectedWard(null);
  };

  const handleWardChange = (value: any) => {
    const ward = wards.find((p: any) => p.code === value);
    setSelectedWard(value);
    setWardNameTemp(ward ? ward.name : "");
  };

  const provinces = subVN.getProvinces();

  const districts =
    selectedProvince !== null
      ? subVN.getDistrictsByProvinceCode(selectedProvince)
      : [];

  const wards =
    selectedDistrict !== null
      ? subVN.getWardsByDistrictCode(selectedDistrict)
      : [];

  const validateAddress = () => {
    if (!selectedProvince || !selectedDistrict || !selectedWard) {
      return Promise.reject("Vui lòng chọn đầy đủ địa chỉ");
    }
    return Promise.resolve();
  };

  const handleCreateStore = useCallback(() => {
    const data = {
      name: storeName,
      address: address,
    };

    callApi<ApiResponse<Store>>(REQUEST_TYPE.POST, "api/store/create", data)
      .then((response) => {
        navigate("/store");
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi, address, navigate, storeName]);

  useEffect(() => {
    if (buttonClicked) {
      handleSaveButtonClick();
    }
  }, [
    selectedProvince,
    selectedDistrict,
    selectedWard,
    handleSaveButtonClick,
    buttonClicked,
  ]);

  return (
    <>
      <StyledLayoutMain>
        <StyledLayoutContent>
          <StyledStepsHeader>
            <StyledSteps>
              <StyledStepsWrap>
                <Steps current={currentStep}>
                  <Step title="Cài đặt thông tin cửa hàng" />
                  <Step title="Cài đặt vận chuyển" />
                  <Step title="Đăng bán sản phẩm" />
                </Steps>
              </StyledStepsWrap>
            </StyledSteps>
          </StyledStepsHeader>

          {currentStep === 0 && (
            <Form onFinish={handleSubmit}>
              <StyledStepContent>
                <Form.Item
                  label="Tên cửa hàng"
                  name="name-store"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập tên cửa hàng",
                    },
                  ]}
                  style={{ marginLeft: 15 }}
                >
                  <Input
                    style={{ width: 400 }}
                    placeholder="Nhập tên cửa hàng"
                    value={storeName}
                    onChange={(e) => handleStoreNameChange(e.target.value)}
                  />
                </Form.Item>

                <Form.Item
                  label="Địa chỉ lấy hàng"
                  name="address-store"
                  rules={[
                    {
                      required: true,
                      validator: (_, value) => {
                        if (value === "") {
                          return Promise.resolve();
                        }
                        if (address === "") {
                          return Promise.reject("Chưa thêm địa chỉ chi tiết");
                        }
                        return Promise.resolve();
                      },
                    },
                  ]}
                >
                  <Button
                    className={`${openButtonAdd ? "block" : "hidden"}`}
                    type="default"
                    onClick={() => setOpenModalAdd(true)}
                  >
                    + Thêm
                  </Button>

                  <Typography.Paragraph
                    className={`${openAddress ? "block" : "hidden"}`}
                    style={{
                      paddingTop: 8,
                      lineHeight: "18px",
                      margin: 0,
                      maxWidth: 400,
                    }}
                  >
                    {address}
                  </Typography.Paragraph>

                  <Button
                    className={`${openButtonEdit ? "block" : "hidden"}`}
                    type="link"
                    onClick={() => {
                      setOpenModalEdit(true);
                      setButtonClicked(false);
                    }}
                    style={{ padding: 0 }}
                  >
                    Chỉnh sửa
                  </Button>
                </Form.Item>

                <Form.Item
                  label="Số điện thoại"
                  name="phone-number"
                  style={{ marginLeft: 28 }}
                >
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    <Input
                      style={{ width: 400 }}
                      defaultValue={dataUser && dataUser.phone}
                      value={dataUser && dataUser.phone}
                      disabled
                    />
                    <Typography.Text style={styles.textError}>
                      Nếu muốn đổi số điện thoại, bạn vui lòng chỉnh sửa tại{" "}
                      <Link
                        to="/customer/account-info"
                        style={{ fontSize: 12 }}
                      >
                        "Thông tin tài khoản"
                      </Link>{" "}
                      sau khi hoàn tất quá trình đăng ký
                    </Typography.Text>
                  </div>
                </Form.Item>
              </StyledStepContent>

              <StyledStepFooter>
                <div></div>
                <Button type="primary" htmlType="submit" style={{ height: 40 }}>
                  Tiếp theo
                </Button>
              </StyledStepFooter>
            </Form>
          )}

          {currentStep === 1 && (
            <>
              <StyledStepContent>
                <div style={{ marginTop: -24 }}>
                  <div style={{ padding: "0 24px" }}>
                    <StyledPanelHeader>
                      <Typography.Title level={4} style={{ margin: 0 }}>
                        Phương thức vận chuyển
                      </Typography.Title>

                      <Typography.Text style={{ color: "#999" }}>
                        Kích hoạt phương thức vận chuyển phù hợp
                      </Typography.Text>
                    </StyledPanelHeader>

                    <StyledShippingContainer>
                      <div style={{ marginBottom: 30 }}>
                        <Typography.Title level={4} style={{ margin: 0 }}>
                          Express
                        </Typography.Title>
                        <StyledShippingItemBox>
                          <StyledShippingItem>
                            <Typography.Title level={5} style={{ margin: 0 }}>
                              Hoả Tốc
                            </Typography.Title>

                            <Switch disabled />
                          </StyledShippingItem>
                        </StyledShippingItemBox>
                      </div>
                      <div style={{ marginBottom: 30 }}>
                        <Typography.Title level={4} style={{ margin: 0 }}>
                          Standard
                        </Typography.Title>
                        <StyledShippingItemBox>
                          <StyledShippingItem>
                            <Typography.Title level={5} style={{ margin: 0 }}>
                              Nhanh
                            </Typography.Title>

                            <Switch disabled />
                          </StyledShippingItem>
                        </StyledShippingItemBox>
                      </div>
                      <div style={{ marginBottom: 30 }}>
                        <Typography.Title level={4} style={{ margin: 0 }}>
                          Economy
                        </Typography.Title>
                        <StyledShippingItemBox>
                          <StyledShippingItem>
                            <Typography.Title level={5} style={{ margin: 0 }}>
                              Tiết Kiệm
                            </Typography.Title>

                            <Switch disabled />
                          </StyledShippingItem>
                        </StyledShippingItemBox>
                      </div>
                    </StyledShippingContainer>
                  </div>
                </div>
              </StyledStepContent>

              <StyledStepFooter>
                <Button
                  type="default"
                  htmlType="submit"
                  style={{ height: 40 }}
                  onClick={handlePrev}
                >
                  Quay lại
                </Button>

                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ height: 40 }}
                  onClick={() => handleNext()}
                >
                  Tiếp theo
                </Button>
              </StyledStepFooter>
            </>
          )}

          {currentStep === 2 && (
            <StyledContentResultStep>
              <StyledResult>
                <StyledResultContent>
                  <BsFillCheckCircleFill style={styles.iconResult} />
                  <Typography.Title level={3}>
                    Nhấn hoàn thành để hoàn tất đăng ký
                  </Typography.Title>

                  <Typography.Paragraph style={styles.textResultDesc}>
                    Hãy đăng bán sản phẩm đầu tiên để khởi động hành trình bán
                    hàng cùng Tiki nhé!
                  </Typography.Paragraph>

                  <Button
                    type="primary"
                    style={styles.btnResult}
                    onClick={handleCreateStore}
                  >
                    Hoàn thành
                  </Button>
                </StyledResultContent>
              </StyledResult>
            </StyledContentResultStep>
          )}
        </StyledLayoutContent>
      </StyledLayoutMain>

      {/* Modal Add */}
      <StyledModal className={`${openModalAdd ? "block" : "hidden"}`}>
        <StyledModalContainer>
          <StyledModalBox>
            <StyledModalContent>
              <Typography.Title level={4} style={{ margin: 0, padding: 24 }}>
                Thêm địa chỉ mới
              </Typography.Title>

              <Form layout="vertical">
                <StyledModalBody>
                  <Form.Item
                    label="Địa chỉ"
                    name="address-modal-add"
                    rules={[
                      {
                        validator: validateAddress,
                      },
                    ]}
                  >
                    <Select
                      placeholder="Chọn Tỉnh/Thành phố"
                      value={selectedProvince}
                      onChange={handleProvinceChange}
                      dropdownStyle={styles.selectInput}
                    >
                      {provinces.map((province: any) => (
                        <Option key={province.code} value={province.code}>
                          {province.name}
                        </Option>
                      ))}
                    </Select>

                    <Select
                      placeholder="Chọn Quận/Huyện"
                      value={selectedDistrict}
                      onChange={handleDistrictChange}
                      style={{ margin: "16px 0" }}
                      dropdownStyle={styles.selectInput}
                    >
                      {districts.map((district: any) => (
                        <Option key={district.code} value={district.code}>
                          {district.name}
                        </Option>
                      ))}
                    </Select>

                    <Select
                      placeholder="Chọn Phường/Xã"
                      value={selectedWard}
                      onChange={handleWardChange}
                      dropdownStyle={styles.selectInput}
                    >
                      {wards.map((ward: any) => (
                        <Option key={ward.code} value={ward.code}>
                          {ward.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Địa chỉ chi tiết"
                    name="detail-address-modal-add"
                    rules={[
                      {
                        required: true,
                        message: "Không được để trống ô này",
                      },
                      {
                        min: 5,
                        message: "Địa chỉ không được dưới 5 ký tự",
                      },
                    ]}
                  >
                    <Input.TextArea
                      placeholder="Số nhà, tên đường,v.v.."
                      rows={4}
                      value={detailAddressTemp}
                      onChange={(e) =>
                        handleDetailAddressChange(e.target.value)
                      }
                    />
                  </Form.Item>
                </StyledModalBody>

                <StyledModalFooter>
                  <div style={{ textAlign: "right" }}>
                    <Button
                      type="default"
                      style={styles.btnModal}
                      onClick={() => setOpenModalAdd(false)}
                    >
                      Huỷ
                    </Button>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ ...styles.btnModal, marginLeft: 16 }}
                      onClick={() => {
                        if (
                          wardNameTemp.length === 0 ||
                          detailAddressTemp.length < 5 ||
                          districtNameTemp.length === 0 ||
                          provinceNameTemp.length === 0
                        ) {
                          return;
                        } else {
                          setOpenModalAdd(false);
                          setOpenButtonEdit(true);
                          setOpenAddress(true);
                          setOpenButtonAdd(false);
                          setButtonClicked(true);
                        }
                      }}
                    >
                      Lưu
                    </Button>
                  </div>
                </StyledModalFooter>
              </Form>
            </StyledModalContent>

            <VscClose
              style={styles.closeModal}
              onClick={() => setOpenModalAdd(false)}
            />
          </StyledModalBox>
        </StyledModalContainer>
      </StyledModal>

      {/* Modal Edit */}
      <StyledModal className={`${openModalEdit ? "block" : "hidden"}`}>
        <StyledModalContainer>
          <StyledModalBox>
            <StyledModalContent>
              <Typography.Title level={4} style={{ margin: 0, padding: 24 }}>
                Chỉnh sửa địa chỉ
              </Typography.Title>

              <Form layout="vertical">
                <StyledModalBody>
                  <Form.Item
                    label="Địa chỉ"
                    name="address-modal-edit"
                    rules={[
                      {
                        validator: validateAddress,
                      },
                    ]}
                  >
                    <Select
                      placeholder="Chọn Tỉnh/Thành phố"
                      value={selectedProvince}
                      onChange={handleProvinceChange}
                      dropdownStyle={styles.selectInput}
                    >
                      {provinces.map((province: any) => (
                        <Option key={province.code} value={province.code}>
                          {province.name}
                        </Option>
                      ))}
                    </Select>

                    <Select
                      placeholder="Chọn Quận/Huyện"
                      value={selectedDistrict}
                      onChange={handleDistrictChange}
                      style={{ margin: "16px 0" }}
                      dropdownStyle={styles.selectInput}
                    >
                      {districts.map((district: any) => (
                        <Option key={district.code} value={district.code}>
                          {district.name}
                        </Option>
                      ))}
                    </Select>

                    <Select
                      placeholder="Chọn Phường/Xã"
                      value={selectedWard}
                      onChange={handleWardChange}
                      dropdownStyle={styles.selectInput}
                    >
                      {wards.map((ward: any) => (
                        <Option key={ward.code} value={ward.code}>
                          {ward.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Địa chỉ chi tiết"
                    name="detail-address-modal-edit"
                    rules={[
                      {
                        required: true,
                        message: "Không được để trống ô này",
                      },
                      {
                        min: 5,
                        message: "Địa chỉ không được dưới 5 ký tự",
                      },
                    ]}
                  >
                    <Input.TextArea
                      placeholder="Số nhà, tên đường,v.v.."
                      rows={4}
                      value={detailAddressTemp}
                      onChange={(e) =>
                        handleDetailAddressChange(e.target.value)
                      }
                    />
                  </Form.Item>
                </StyledModalBody>

                <StyledModalFooter>
                  <div style={{ textAlign: "right" }}>
                    <Button
                      type="default"
                      style={styles.btnModal}
                      onClick={() => setOpenModalEdit(false)}
                    >
                      Huỷ
                    </Button>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ ...styles.btnModal, marginLeft: 16 }}
                      onClick={() => {
                        if (
                          wardNameTemp.length === 0 ||
                          detailAddressTemp.length < 5 ||
                          districtNameTemp.length === 0 ||
                          provinceNameTemp.length === 0
                        ) {
                          return;
                        } else {
                          setOpenModalEdit(false);
                          setButtonClicked(true);
                        }
                      }}
                    >
                      Lưu
                    </Button>
                  </div>
                </StyledModalFooter>
              </Form>
            </StyledModalContent>

            <VscClose
              style={styles.closeModal}
              onClick={() => setOpenModalEdit(false)}
            />
          </StyledModalBox>
        </StyledModalContainer>
      </StyledModal>
    </>
  );
}

export default RegistrationForm;
