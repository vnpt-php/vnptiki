import { Menu, Typography } from "antd";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { SubMenu } from "rc-menu";
import { BsBox2, BsTag } from "react-icons/bs";
import { IoMdPaper } from "react-icons/io";
import { CgShoppingBag } from "react-icons/cg";
import { TbWallet } from "react-icons/tb";
import { AiOutlineLineChart } from "react-icons/ai";
import { TfiMedallAlt } from "react-icons/tfi";
import { RiBillLine } from "react-icons/ri";
import { MdStorefront } from "react-icons/md";
import { IoSettingsOutline } from "react-icons/io5";
import { BiHelpCircle } from "react-icons/bi";

const StyledSidebarContainer = styled.div`
  position: fixed;
  z-index: 10;
  width: 220px;
  height: 100%;
  overflow-y: auto;
  background: #fff;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const styles = {
  iconSubMenu: {
    marginRight: "8px",
    color: "#999",
    fontSize: "16px",
  },
};

function SidebarStore(props: any) {
  const subMenuTransport = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <BsBox2 style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Vận chuyển
      </Typography.Text>
    </div>
  );

  const subMenuOrders = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <IoMdPaper style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Quản lý đơn hàng
      </Typography.Text>
    </div>
  );

  const subMenuProducts = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <CgShoppingBag style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Quản lý sản phẩm
      </Typography.Text>
    </div>
  );

  const subMenuMarketing = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <BsTag style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Kênh marketing
      </Typography.Text>
    </div>
  );

  const subMenuFinance = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <TbWallet style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Tài chính
      </Typography.Text>
    </div>
  );

  const subMenuData = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <AiOutlineLineChart style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Dữ liệu
      </Typography.Text>
    </div>
  );

  const subMenuGrow = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <TfiMedallAlt style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Phát triển
      </Typography.Text>
    </div>
  );

  const subMenuCustomerCare = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <RiBillLine style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Chăm sóc KH
      </Typography.Text>
    </div>
  );

  const subMenuStore = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <MdStorefront style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Quản lý cửa hàng
      </Typography.Text>
    </div>
  );

  const subMenuSettingStore = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <IoSettingsOutline style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Thiết lập cửa hàng
      </Typography.Text>
    </div>
  );

  const subMenuSupportPortal = (
    <div style={{ display: "flex", alignItems: "center" }}>
      <BiHelpCircle style={styles.iconSubMenu} />
      <Typography.Text style={{ fontWeight: 600, color: "#999" }}>
        Trợ giúp
      </Typography.Text>
    </div>
  );

  return (
    <StyledSidebarContainer>
      <Menu mode="inline" defaultSelectedKeys={[props.defaultSelectedKey]}>
        <SubMenu key="sub1" title={subMenuTransport}>
          <Menu.Item key="1">
            <Link to="#">Quản lý vận chuyển</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="#">Giao hàng loạt</Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="#">Cài đặt vận chuyển</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub2" title={subMenuOrders}>
          <Menu.Item key="4">
            <Link to="/store/sale/order">Tất cả</Link>
          </Menu.Item>
          <Menu.Item key="5">
            <Link to="#">Đơn huỷ</Link>
          </Menu.Item>
          <Menu.Item key="6">
            <Link to="#">Trả hàng/Hoàn tiền</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub3" title={subMenuProducts}>
          <Menu.Item key="7">
            <Link to="/store/product/list/all">Tất cả sản phẩm</Link>
          </Menu.Item>
          <Menu.Item key="8">
            <Link to="/store/product/new">Thêm sản phẩm</Link>
          </Menu.Item>
          <Menu.Item key="9">
            <Link to="#">Sản phẩm vi phạm</Link>
          </Menu.Item>
          <Menu.Item key="10">
            <Link to="/store/product/setting">Cài đặt sản phẩm</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub4" title={subMenuMarketing}>
          <Menu.Item key="11">
            <Link to="#">Kênh marketing</Link>
          </Menu.Item>
          <Menu.Item key="12">
            <Link to="#">Quảng cáo Tiki</Link>
          </Menu.Item>
          <Menu.Item key="13">
            <Link to="#">Mã giảm giá của tôi</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub5" title={subMenuFinance}>
          <Menu.Item key="14">
            <Link to="#">Doanh thu</Link>
          </Menu.Item>
          <Menu.Item key="15">
            <Link to="#">Số dư tài khoản Tiki</Link>
          </Menu.Item>
          <Menu.Item key="16">
            <Link to="#">Tài khoản ngân hàng</Link>
          </Menu.Item>
          <Menu.Item key="17">
            <Link to="#">Cài đặt thanh toán</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub6" title={subMenuData}>
          <Menu.Item key="18">
            <Link to="#">Phân tích bán hàng</Link>
          </Menu.Item>
          <Menu.Item key="19">
            <Link to="#">Hiệu quả hoạt động</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub7" title={subMenuGrow}>
          <Menu.Item key="20">
            <Link to="#">Nhiệm vụ Người bán</Link>
          </Menu.Item>
          <Menu.Item key="21">
            <Link to="#">Cửa hàng yêu thích</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub8" title={subMenuCustomerCare}>
          <Menu.Item key="22">
            <Link to="#">Trợ lý Chat</Link>
          </Menu.Item>
          <Menu.Item key="23">
            <Link to="#">Hỏi - Đáp</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub9" title={subMenuStore}>
          <Menu.Item key="24">
            <Link to="#">Đánh giá</Link>
          </Menu.Item>
          <Menu.Item key="25">
            <Link to="/store/setting/profile">Hồ sơ</Link>
          </Menu.Item>
          <Menu.Item key="26">
            <Link to="#">Trang trí</Link>
          </Menu.Item>
          <Menu.Item key="27">
            <Link to="#">Danh mục</Link>
          </Menu.Item>
          <Menu.Item key="28">
            <Link to="#">Kho hình ảnh/video</Link>
          </Menu.Item>
          <Menu.Item key="29">
            <Link to="#">Báo cáo của tôi</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub10" title={subMenuSettingStore}>
          <Menu.Item key="30">
            <Link to="/store/address">Địa chỉ</Link>
          </Menu.Item>
          <Menu.Item key="31">
            <Link to="#">Thiết lập</Link>
          </Menu.Item>
          <Menu.Item key="32">
            <Link to="#">Tài khoản</Link>
          </Menu.Item>
          <Menu.Item key="33">
            <Link to="#">Nền tảng đối tác (API)</Link>
          </Menu.Item>
        </SubMenu>

        <SubMenu key="sub11" title={subMenuSupportPortal}>
          <Menu.Item key="34">
            <Link to="#">Cổng thông tin hỗ trợ</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </StyledSidebarContainer>
  );
}

export default SidebarStore;
