import { Button, Popover, Typography } from "antd";
import { IoIosLogOut } from "react-icons/io";
import { IoAppsOutline } from "react-icons/io5";
import { RiImageEditLine } from "react-icons/ri";
import { AiOutlineBell, AiOutlineHome } from "react-icons/ai";
import styled from "styled-components";
import { CSSProperties, useCallback, useContext, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Store } from "../../models/types/Store";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { ApiResponse } from "../../models/ApiResponse";
import { useApi } from "../../hooks/useApi";
import StoreContext from "../../context/StoreContext";
import ProductContext from "../../context/ProductContext";
import { Product } from "../../models/types/Product";
import CategoryContext from "../../context/Category";
import { Category } from "../../models/types/Category";
import BrandContext from "../../context/BrandContext";
import { Brand } from "../../models/types/Brand";

const StyledHeaderBar = styled.div`
  top: 0;
  left: 0;
  position: fixed;
  width: 100%;
  min-height: 56px;
  max-height: 56px;
  z-index: 6666;
  background-color: #fff;
  box-shadow: 0 1px 4px 0 rgba(74, 74, 78, 0.12);
`;

const StyledHeaderContent = styled.div`
  display: flex;
  width: 100vw;
  margin: auto;
  min-height: 56px;
  max-height: 56px;
`;

const StyledTitleHeader = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  margin: 2px 0 0 5px;
  -webkit-box-flex: 1;
  flex: 1;
`;

const StyledAccountInfoBox = styled.div`
  position: relative;
  padding: 8px 16px 8px 0;

  ::after {
    content: "";
    right: 0;
    top: 12px;
    position: absolute;
    width: 1px;
    height: 32px;
    background-color: #e8e8e8;
  }
`;

const StyledAccountInfo = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  position: relative;
  height: 40px;
  padding: 4px 16px 4px 4px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  color: #333;
  cursor: pointer;

  :hover {
    background-color: #f3f1f1;
    border-radius: 20px;
  }
`;

const StyledHover = styled.div`
  :hover {
    background-color: #eee;
  }
`;

const StyledHeaderAction = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  padding: 0 4px 0 12px;
  margin-right: 16px;
`;

const StyledDropBtn = styled.div`
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  width: 40px;
  height: 40px;
  font-size: 0;
  cursor: pointer;

  :hover {
    background-color: #f3f1f1;
    border-radius: 50%;
  }
`;

const StyledNotification = styled.div`
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  position: relative;
  width: 40px;
  height: 40px;
  cursor: pointer;

  :hover {
    background-color: #f3f1f1;
    border-radius: 50%;
  }
`;

const StyledNotificationIcon = styled.div`
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  width: 40px;
  height: 40px;
`;

const StyledBadgeX = styled.div`
  position: relative;
  display: inline-block;
  font-size: 0;
  vertical-align: middle;
`;

const StyledBadgeXSup = styled.sup`
  width: 20px;
  height: 20px;
  padding: 0;
  line-height: 18px;
  position: absolute;
  top: 0;
  right: 10px;
  transform: translateY(-50%) translateX(100%);
  display: inline-block;
  font-size: 12px;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  background-color: #ee4d2d;
  border: 1px solid #fff;
  border-radius: 10px;
  box-sizing: border-box;
  user-select: none;
`;

const styles = {
  headerLogo: {
    position: "relative",
    marginTop: "10px",
    marginLeft: "16px",
  } as CSSProperties,

  accountImg: {
    width: "32px",
    height: "32px",
    marginRight: "8px",
    borderRadius: "50%",
  },

  btn: {
    height: "100%",
    display: "flex",
    alignItems: "center",
  },

  iconBoxAccount: {
    width: "18px",
    height: "18px",
    marginRight: "8px",
  },

  boxAccount: {
    boxShadow: "0 1px 4px 0 rgba(74, 74, 78, 0.12)",
    zIndex: "999",
    position: "fixed",
  } as CSSProperties,
};

const HeaderStore: React.FC = () => {
  const navigate = useNavigate();
  const { callApi } = useApi();
  const token = sessionStorage.getItem("ACCESS_TOKEN");
  const { dataStore, setDataStore } = useContext(StoreContext);
  const { setDataProduct } = useContext(ProductContext);
  const { setDataCategory } = useContext(CategoryContext);
  const { setDataBrand } = useContext(BrandContext);

  const handleLogout = useCallback(() => {
    sessionStorage.removeItem("ACCESS_TOKEN");
    navigate("/");
  }, [navigate]);

  const getDataStore = useCallback(() => {
    callApi<ApiResponse<Store>>(REQUEST_TYPE.GET, "api/store")
      .then((response) => {
        setDataStore(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataStoreProducts = useCallback(() => {
    callApi<ApiResponse<Product[]>>(REQUEST_TYPE.GET, "api/my-products")
      .then((response) => {
        setDataProduct(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataCategory = useCallback(() => {
    callApi<ApiResponse<Category[]>>(REQUEST_TYPE.GET, "api/categories")
      .then((response) => {
        setDataCategory(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataBrand = useCallback(() => {
    callApi<ApiResponse<Brand[]>>(REQUEST_TYPE.GET, "api/brands")
      .then((response) => {
        setDataBrand(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  useEffect(() => {
    if (token) {
      getDataStore();
      getDataStoreProducts();
      getDataCategory();
      getDataBrand();
    } else {
      navigate("/");
    }
  }, [token, navigate]);

  const contentAccount = (
    <>
      <StyledHover>
        <Link to="/store/setting/profile">
          <Button type="ghost" style={styles.btn}>
            <RiImageEditLine style={styles.iconBoxAccount} />
            <Typography.Text>Hồ sơ cửa hàng</Typography.Text>
          </Button>
        </Link>
      </StyledHover>
      <StyledHover>
        <Link to="/">
          <Button type="ghost" style={styles.btn}>
            <AiOutlineHome style={styles.iconBoxAccount} />
            <Typography.Text>Quay về trang chủ Tiki</Typography.Text>
          </Button>
        </Link>
      </StyledHover>
      <StyledHover>
        <Button type="ghost" style={styles.btn} onClick={handleLogout}>
          <IoIosLogOut style={styles.iconBoxAccount} />
          <Typography.Text>Đăng xuất</Typography.Text>
        </Button>
      </StyledHover>
    </>
  );

  return (
    <StyledHeaderBar>
      <StyledHeaderContent>
        <Link to="/store" style={styles.headerLogo}>
          <img
            src="https://salt.tikicdn.com/ts/upload/e4/49/6c/270be9859abd5f5ec5071da65fab0a94.png"
            alt=""
            width={42.75}
            height={30}
          />
        </Link>

        <StyledTitleHeader>
          <Typography.Text style={{ color: "1d1d1d", fontSize: 18 }}>
            Kênh Người bán
          </Typography.Text>
        </StyledTitleHeader>

        <Popover
          content={contentAccount}
          placement="bottom"
          trigger="hover"
          overlayStyle={styles.boxAccount}
        >
          <StyledAccountInfoBox>
            <StyledAccountInfo>
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAMAAAAKE/YAAAABJlBMVEXP2NxgfYtjf412j5uMoavO19uis7vG0dVxi5fBzNJyjJhjgI56kp5hfoxxi5h5kZ2ouL9ohJGInaipucCHnah3kJyxv8bL1NnM1dlphJGXqrN4kZzM1tqUp7Gnt76InqlkgY6wvsWGnKdrhpOnuL+xwMZ8lJ+3xMvAzNGCmaTFz9Sdr7fN1trN19tlgY5qhZJkgI6/y9COoqzL1dlphJJ0jZp7k566x8y7x81ng5CywMegsrp4kZ14kJyhs7uJn6nH0dZlgY+Fm6bG0daWqrO2xMqzwcfF0NXBzdK3xcu0wsh7k5+qusG0wsmzwchuiJVviZaYq7S7yM2Kn6m8yM6/y9GInqiWqbKNoqxzjJmjtLyqucGfsbmdr7iwv8WKoKpuiZVif42PQ9RwAAABzUlEQVR4Xu3YVZLjShBA0UyBmZmamXuYmZmZ3tv/Jua3Y8JWTdsKKR1zzwruR1aVlDIdAAAAAAAAAAAAAAAAAADyN1t+3fPqfiOTl7kQ1qp6QqkZinmdnP6hcFVs6wY6RtATwx75OlY7K2ZlczpBzmx194ZONOyKTbc0QiAmrWikFTHoIKeRKgdiz7o6LIo919XhophzRp3yYs01dcqINavqtCTWXFAnX6y5ok7nxZqyOhWJNjAeHERzVx6Py7Y6PRZzqnP4wSQ1dajN4U9AIRSDOhqpIyYFGuGs2HRuqBMNR/O3rKlYXjG1dazhbTGsF4yd557YtlFwrHpNCpslPeHZ81DmQj7T8OvFYt9vfM3LPwkAgOzDzMLdUqV/qHqnXymtLmRe3hPD9h/s7RzpGEc7e1v7Yk+4dX/zWCMsbw6eiClPgzV107XPL8SI3cEr/Wuv3+xK+t4ueXoq3rv3kq4PrbKeWvnjtqRn9MnTqXiDkaTkUkGn9uWypKK5rDM4/CYp+K4z+iGJW9SZrSc+z57OzEt6rtsag6ok6qfGYkOS1NJY/JIk/aexyEmS/tdYHEuSNCbzGU000UQTTTQAAAAAAAAAAAAAAAAAAL8BwZgl987F+p8AAAAASUVORK5CYII="
                alt=""
                style={styles.accountImg}
              />

              <Typography.Text style={{ minWidth: "64px" }}>
                {dataStore && dataStore.name}
              </Typography.Text>
            </StyledAccountInfo>
          </StyledAccountInfoBox>
        </Popover>

        <StyledHeaderAction>
          <div style={{ padding: "8px 0" }}>
            <StyledDropBtn>
              <IoAppsOutline style={{ width: 22, height: 22 }} />
            </StyledDropBtn>
          </div>

          <div style={{ margin: "0 8px", padding: "8px 0" }}>
            <StyledNotification>
              <StyledNotificationIcon>
                <StyledBadgeX>
                  <AiOutlineBell style={{ width: 22, height: 22 }} />
                  <StyledBadgeXSup>2</StyledBadgeXSup>
                </StyledBadgeX>
              </StyledNotificationIcon>
            </StyledNotification>
          </div>
        </StyledHeaderAction>
      </StyledHeaderContent>
    </StyledHeaderBar>
  );
};

export default HeaderStore;
