import { Form, Input, Typography, Select, Button, Alert } from "antd";
import styled from "styled-components";
import { CSSProperties, useContext, useEffect, useState } from "react";
import { RiImageAddFill } from "react-icons/ri";
import { ImEnlarge2 } from "react-icons/im";
import { BsTrash } from "react-icons/bs";
import {
  StyledAlertWrapper,
  StyledPageContainer,
  StyledPageContentWrapper,
  StyledProductBasicInfo,
  StyledProductRow,
  StyledProductTitle,
} from "../styled/styled-components";
import { Link } from "react-router-dom";
import CategoryContext from "../../context/Category";
import axios from "axios";
import { baseUrl } from "../../constants/url";

const { Option } = Select;

const StyledOffset = styled.div`
  flex-basis: 808px;
  width: 808px;
  max-width: 808px;
  margin-bottom: -16px;
`;

const StyledStoreImageManager = styled.div`
  display: inline-block;
  width: auto;
  max-width: 808px;
  position: relative;
  color: #1791f2;
`;

const StyledImgItemBox = styled.div`
  width: 80px;
  max-width: 80px;
  height: 80px;
  max-height: 80px;
  -webkit-box-flex: 0;
  flex: 0 0 80px;
  min-height: 80px;
  margin: 0 16px 16px 0;
  border-radius: 4px;
`;

const StyledImgContent = styled.div`
  cursor: move;
  position: relative;
  padding-top: 100%;
  border-radius: 2px;
  user-select: none;

  &:hover > .tool-actions {
    display: flex !important;
  }

  &:hover > .tool-title {
    display: none !important;
  }
`;

const StyledImgUpload = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 2;
  border: 1px dashed #d8d8d8;
  border-radius: 4px;
  transition: color 0.25s ease, border-color 0.25s ease;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  user-select: none;

  :hover {
    background-color: #fde3e0;
    border-color: red;
  }
`;

const StyledImgUploadContent = styled.div`
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  -webkit-box-pack: center;
  justify-content: center;
  padding: 0 2px;
  align-items: center;
  cursor: pointer;
`;

const StyledImgManagerTool = styled.div`
  display: flex;
  justify-content: space-evenly;
  -webkit-box-align: center;
  align-items: center;
  position: absolute;
  width: 100%;
  bottom: 0;
  height: 24px;
  background-color: rgba(0, 0, 0, 0.3);
  border-radius: 0 0 4px 4px;
  backdrop-filter: saturate(180%) blur(20px);
  font-weight: 700;
`;

const styles = {
  title: {
    fontSize: "20px",
    fontWeight: "600",
    lineHeight: "22px",
    color: "#333",
    margin: "0",
  },

  img: {
    objectFit: "contain",
    position: "absolute",
    top: "0",
    left: "0",
    width: "100%",
    height: "100%",
    borderRadius: "4px",
    background: "transparent 50% no-repeat",
    backgroundSize: "contain",
    userSelect: "none",
  } as CSSProperties,
};

function CreateProduct() {
  const { dataCategory } = useContext(CategoryContext);

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [selectedImages, setSelectedImages] = useState<File[]>([]);
  const [selectedIdCate, setSelectedIdCate] = useState("");
  const [selectedIdBrand, setSelectedIdBrand] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [nameProduct, setNameProduct] = useState("");

  const handleCreateProduct = () => {
    const productInfo = {
      name: nameProduct,
      price: price,
      description: description,
      category_id: selectedIdCate,
      brand_id: selectedIdBrand,
    };

    const formData = new FormData();
    formData.append("data", JSON.stringify(productInfo));
    for (let i = 0; i < selectedImages.length; i++) {
      formData.append("images[]", selectedImages[i]);
    }

    const url = `${baseUrl}/api/product/create`;

    axios
      .post(url, formData, {
        headers: {
          Authorization: "Bearer " + sessionStorage.getItem("ACCESS_TOKEN"),
          "content-type": "multipart/form-data",
        },
      })
      .then((res) => {
        setSuccess("Thêm sản phẩm thành công");
        window.location.reload();
      })
      .catch((err) => {
        setError("Thêm sản phẩm thất bại");
        console.error(err);
      });
  };

  const handleNameProductChange = (value: any) => {
    setNameProduct(value);
  };

  const handlePriceChange = (value: any) => {
    setPrice(value);
  };

  const handleDescriptionChange = (value: any) => {
    setDescription(value);
  };

  const handleFileSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    if (files) {
      setSelectedImages(Array.from(files));
    }
  };

  const handleImageClick = (index: number) => {
    const newSelectedImages = [...selectedImages];
    newSelectedImages.splice(index, 1);
    setSelectedImages(newSelectedImages.filter((img) => img !== null));
  };

  const onChangeCate = (value: any) => {
    setSelectedIdCate(value);
    setSelectedIdBrand("");
  };

  const onChangeBrand = (value: any) => {
    setSelectedIdBrand(value);
  };

  useEffect(() => {}, [selectedIdBrand, selectedIdCate]);

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledPageContainer>
        <StyledPageContentWrapper>
          <div style={{ margin: "16px auto 0" }}>
            <StyledProductBasicInfo>
              <div style={{ marginBottom: 24 }}>
                <Typography.Title level={5} style={styles.title}>
                  Thông tin cơ bản
                </Typography.Title>
              </div>

              <Form>
                <StyledProductRow>
                  <StyledProductTitle>
                    <Typography.Text
                      style={{
                        color: "#ee4d2d",
                        marginRight: 3,
                        fontFamily: "SimSun,sans-serif",
                      }}
                    >
                      *
                    </Typography.Text>
                    <Typography.Text
                      style={{
                        fontFamily:
                          "apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,'Noto Sans',sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji'",
                      }}
                    >
                      Hình ảnh sản phẩm:
                    </Typography.Text>
                  </StyledProductTitle>

                  <StyledOffset>
                    <StyledStoreImageManager>
                      <div style={{ display: "flex", flexWrap: "wrap" }}>
                        {selectedImages.map((file, index) => (
                          <StyledImgItemBox key={index}>
                            <div style={{ height: "100%", width: "100%" }}>
                              <StyledImgContent>
                                <img
                                  src={URL.createObjectURL(file)}
                                  alt={file.name}
                                  style={styles.img}
                                />

                                <StyledImgManagerTool
                                  className="tool-actions"
                                  style={{ display: "none" }}
                                >
                                  <ImEnlarge2
                                    height={16}
                                    width={16}
                                    style={{
                                      color: "#fff",
                                      cursor: "pointer",
                                    }}
                                  />

                                  <Typography.Text
                                    style={{
                                      width: 1,
                                      height: 12,
                                      background: "hsla(0,0%,100%,.5)",
                                      borderRadius: 0.5,
                                    }}
                                  ></Typography.Text>

                                  <BsTrash
                                    height={16}
                                    width={16}
                                    style={{
                                      color: "#fff",
                                      cursor: "pointer",
                                    }}
                                    onClick={() => handleImageClick(index)}
                                  />
                                </StyledImgManagerTool>

                                <StyledImgManagerTool
                                  className="tool-title"
                                  style={{
                                    justifyContent: "center",
                                    WebkitBoxAlign: "center",
                                  }}
                                >
                                  <Typography.Text
                                    style={{
                                      color: "#ee4d2d",
                                      marginRight: 3,
                                      verticalAlign: "middle",
                                    }}
                                  >
                                    *
                                  </Typography.Text>
                                  <Typography.Text
                                    style={{ color: "#fff", fontSize: 12 }}
                                  >
                                    Ảnh bìa
                                  </Typography.Text>
                                </StyledImgManagerTool>
                              </StyledImgContent>
                            </div>
                          </StyledImgItemBox>
                        ))}

                        <StyledImgItemBox>
                          <StyledImgContent>
                            <StyledImgUpload>
                              <input
                                type="file"
                                accept="image/*"
                                id="fileInput"
                                multiple
                                onChange={handleFileSelect}
                                style={{ display: "none" }}
                              />

                              <label htmlFor="fileInput">
                                <StyledImgUploadContent>
                                  <div style={{ height: 23 }}>
                                    <RiImageAddFill
                                      height={23}
                                      width={23}
                                      style={{ color: "#ee4d2d" }}
                                    />
                                  </div>

                                  <Typography.Text
                                    style={{
                                      color: "#ee4d2d",
                                      fontSize: 12,
                                      textAlign: "center",
                                    }}
                                  >
                                    Thêm hình ảnh
                                  </Typography.Text>
                                </StyledImgUploadContent>
                              </label>
                            </StyledImgUpload>
                          </StyledImgContent>
                        </StyledImgItemBox>
                      </div>
                    </StyledStoreImageManager>
                  </StyledOffset>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Tên sản phẩm"
                    name="name-product"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "100%", marginLeft: 32 }}
                  >
                    <Input
                      placeholder="Nhập vào"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      value={nameProduct}
                      onChange={(e) => handleNameProductChange(e.target.value)}
                    />
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Chọn loại sản phẩm"
                    name="category-product"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "100%", marginLeft: -4 }}
                  >
                    <Select
                      placeholder="Chọn"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      onChange={onChangeCate}
                      value={selectedIdCate}
                    >
                      {dataCategory &&
                        dataCategory.map((item) => (
                          <Option key={item.id} value={item.id}>
                            {item.name}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Chọn thương hiệu"
                    name="brand-product"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "100%", marginLeft: 6 }}
                  >
                    <Select
                      placeholder="Chọn"
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      onChange={onChangeBrand}
                      value={selectedIdBrand}
                    >
                      {dataCategory.map((category) => {
                        const isSelected = category.id === selectedIdCate;
                        if (isSelected) {
                          return category.brands.map((brand) => (
                            <Option key={brand.id} value={brand.id}>
                              {brand.name}
                            </Option>
                          ));
                        } else {
                          return null;
                        }
                      })}
                    </Select>
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Giá sản phẩm"
                    name="price-product"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                    ]}
                    style={{ width: "100%", marginLeft: 35 }}
                  >
                    <Input
                      placeholder="Nhập vào"
                      maxLength={13}
                      onKeyDown={(event) => {
                        const isNumber = /[0-9]/.test(event.key);
                        const allowedKeys = [
                          "Backspace",
                          "Delete",
                          "ArrowLeft",
                          "ArrowRight",
                          "Tab",
                        ];
                        if (!isNumber && !allowedKeys.includes(event.key)) {
                          event.preventDefault();
                        }
                      }}
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      value={price}
                      onChange={(e) => handlePriceChange(e.target.value)}
                    />
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <Form.Item
                    label="Mô tả sản phẩm"
                    name="describe-product"
                    rules={[
                      {
                        required: true,
                        message: "",
                      },
                      {
                        max: 3000,
                        message: "Tối đa 3000 ký tự",
                      },
                      {
                        min: 50,
                        message: "Ít nhất 50 ký tự",
                      },
                    ]}
                    style={{
                      width: "100%",
                      marginLeft: 20,
                    }}
                  >
                    <Input.TextArea
                      maxLength={3000}
                      showCount
                      placeholder="Nhập vào"
                      rows={10}
                      style={{ width: "calc(100% - 19px)", marginLeft: 19 }}
                      value={description}
                      onChange={(e) => handleDescriptionChange(e.target.value)}
                    />
                  </Form.Item>
                </StyledProductRow>

                <StyledProductRow>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row-reverse",
                      width: "100%",
                    }}
                  >
                    <Button
                      type="primary"
                      style={{ minWidth: 140, height: 32 }}
                      onClick={handleCreateProduct}
                    >
                      Lưu & Hiển thị
                    </Button>
                    <Button
                      type="default"
                      style={{ minWidth: 140, height: 32, marginRight: 16 }}
                    >
                      <Link to="/store/product/list/all">Huỷ</Link>
                    </Button>
                  </div>
                </StyledProductRow>
              </Form>
            </StyledProductBasicInfo>
          </div>
        </StyledPageContentWrapper>
      </StyledPageContainer>
    </>
  );
}

export default CreateProduct;
