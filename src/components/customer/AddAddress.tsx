import { Alert, Button, Checkbox, Form, Radio, Select, Typography } from "antd";
import styled from "styled-components";
import { useState, useEffect, useCallback } from "react";
import subVN from "sub-vn";
import { ApiResponse } from "../../models/ApiResponse";
import { useNavigate } from "react-router-dom";
import { StyledAlertWrapper } from "../styled/styled-components";
import { Address } from "../../models/types/Address";
import { useApi } from "../../hooks/useApi";
import { REQUEST_TYPE } from "../../enums/RequestType";

const { Option } = Select;

const StyledHeading = styled.div`
  font-size: 20px;
  line-height: 32px;
  font-weight: 300;
  margin: 4px 0 12px;
`;

const StyledInner = styled.div`
  background-color: rgb(255, 255, 255);
  border-radius: 4px;
  min-height: 400px;
  padding: 30px 20px;
`;

const StyledFormControl = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  margin: 0 0 15px;
`;

const StyledField = styled.div`
  display: flex;
  flex: 1 1 0%;
  position: relative;
  z-index: 1;
`;

const StyledInputField = styled.input`
  height: 34px;
  border-radius: 4px;
  padding: 0 12px;
  outline: none;
  line-height: 20px;
  border: 1px solid rgb(204, 204, 204);
  ::placeholder {
    font-weight: 500;
    color: rgb(196, 196, 207);
  }
`;

const StyledTextAreaField = styled.textarea`
  border: 1px solid rgb(204, 204, 204);
  border-radius: 4px;
  width: 100%;
  padding: 6px 12px;
  font-family: inherit;
  ::placeholder {
    /* font-weight: 600; */
    color: rgb(196, 196, 207);
  }
`;

const styles = {
  inputLabel: {
    width: "110px",
    minWidth: "110px",
    fontSize: "13px",
    color: "rgb(51, 51, 51)",
  },

  input: {
    flex: "1 1 0%",
  },

  btnUpdate: {
    width: "176px",
    height: "40px",
    borderRadius: "4px",
    fontSize: "14px",
  },

  inputSelect: {
    width: "100%",
    height: "34px",
    lineHeight: "34px",
    outline: "none",
    background: "0 center",
  },
};

function AddAddress() {
  const navigate = useNavigate();
  const { callApi } = useApi();

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [nameCustomerAddress, setNameCustomerAddress] = useState("");
  const [phoneCustomerAddress, setPhoneCustomerAddress] = useState("");
  const [detailAddress, setDetailAddress] = useState("");
  const [provinceName, setProvinceName] = useState("");
  const [districtName, setDistrictName] = useState("");
  const [wardName, setWardName] = useState("");

  const [selectedProvince, setSelectedProvince] = useState<string | null>(null);
  const [selectedDistrict, setSelectedDistrict] = useState<string | null>(null);
  const [selectedWard, setSelectedWard] = useState<string | null>(null);

  const handleProvinceChange = (value: any) => {
    const province = provinces.find((p: any) => p.code === value);
    setSelectedProvince(value);
    setProvinceName(province ? province.name : "");
    setSelectedDistrict(null);
    setSelectedWard(null);
  };

  const handleDistrictChange = (value: any) => {
    const district = districts.find((p: any) => p.code === value);
    setSelectedDistrict(value);
    setDistrictName(district ? district.name : "");
    setSelectedWard(null);
  };

  const handleWardChange = (value: any) => {
    const ward = wards.find((p: any) => p.code === value);
    setSelectedWard(value);
    setWardName(ward ? ward.name : "");
  };

  const provinces = subVN.getProvinces();

  const districts =
    selectedProvince !== null
      ? subVN.getDistrictsByProvinceCode(selectedProvince)
      : [];

  const wards =
    selectedDistrict !== null
      ? subVN.getWardsByDistrictCode(selectedDistrict)
      : [];

  // function onChangeTypeAddress(e: any) {
  //   ///
  // }

  const handleNameCustomerAddressChange = (value: any) => {
    setNameCustomerAddress(value);
  };

  const handlePhoneCustomerAddressChange = (value: any) => {
    setPhoneCustomerAddress(value);
  };

  const handleDetailAddressChange = (value: any) => {
    setDetailAddress(value);
  };

  const handleAddAddress = useCallback(() => {
    const data = {
      name: nameCustomerAddress,
      city: provinceName,
      phone: phoneCustomerAddress,
      ward: wardName,
      district: districtName,
      default: false,
      detail: detailAddress,
    };

    callApi<ApiResponse<Address>>(REQUEST_TYPE.POST, "api/address/create", data)
      .then((response) => {
        setSuccess("Tạo sổ địa chỉ thành công");
        navigate("/customer/address");
      })
      .catch((error) => {
        setError("Tạo sổ địa chỉ thất bại");
        console.log(error);
      });
  }, [
    callApi,
    detailAddress,
    districtName,
    nameCustomerAddress,
    phoneCustomerAddress,
    wardName,
    provinceName,
    navigate,
  ]);

  useEffect(() => {}, [selectedProvince, selectedDistrict, selectedWard]);

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledHeading>Tạo sổ địa chỉ</StyledHeading>

      <StyledInner>
        <Form style={{ width: 600 }}>
          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Họ và tên:
            </Typography.Text>

            <StyledField>
              <StyledInputField
                required
                style={styles.input}
                placeholder="Nhập họ và tên"
                value={nameCustomerAddress}
                onChange={(e) =>
                  handleNameCustomerAddressChange(e.target.value)
                }
              />
            </StyledField>
          </StyledFormControl>

          {/* <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Công ty:
            </Typography.Text>

            <StyledField>
              <StyledInputField
                style={styles.input}
                placeholder="Nhập công ty"
              />
            </StyledField>
          </StyledFormControl> */}

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Số điện thoại:
            </Typography.Text>

            <StyledField>
              <StyledInputField
                required
                style={styles.input}
                placeholder="Nhập số điện thoại"
                value={phoneCustomerAddress}
                onChange={(e) =>
                  handlePhoneCustomerAddressChange(e.target.value)
                }
              />
            </StyledField>
          </StyledFormControl>

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Tỉnh/Thành phố:
            </Typography.Text>

            <Select
              placeholder="Chọn Tỉnh/Thành phố"
              value={selectedProvince}
              onChange={handleProvinceChange}
              style={styles.inputSelect}
            >
              {provinces.map((province: any) => (
                <Option key={province.code} value={province.code}>
                  {province.name}
                </Option>
              ))}
            </Select>
          </StyledFormControl>

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Quận huyện:
            </Typography.Text>

            <Select
              placeholder="Chọn Quận/Huyện"
              value={selectedDistrict}
              onChange={handleDistrictChange}
              style={styles.inputSelect}
            >
              {districts.map((district: any) => (
                <Option key={district.code} value={district.code}>
                  {district.name}
                </Option>
              ))}
            </Select>
          </StyledFormControl>

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Phường xã:
            </Typography.Text>

            <Select
              placeholder="Chọn Phường/Xã"
              value={selectedWard}
              onChange={handleWardChange}
              style={styles.inputSelect}
            >
              {wards.map((ward: any) => (
                <Option key={ward.code} value={ward.code}>
                  {ward.name}
                </Option>
              ))}
            </Select>
          </StyledFormControl>

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Địa chỉ:
            </Typography.Text>

            <StyledField>
              <StyledTextAreaField
                required
                rows={5}
                style={styles.input}
                placeholder="Nhập địa chỉ"
                value={detailAddress}
                onChange={(e) => handleDetailAddressChange(e.target.value)}
              />
            </StyledField>
          </StyledFormControl>

          {/* <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>
              Loại địa chỉ:
            </Typography.Text>

            <Radio.Group onChange={onChangeTypeAddress}>
              <Radio value="house">Nhà riêng / Chung cư</Radio>
              <Radio value="company">Cơ quan / Công ty</Radio>
            </Radio.Group>
          </StyledFormControl> */}

          <StyledFormControl>
            <Typography.Text style={styles.inputLabel}>&nbsp;</Typography.Text>

            <StyledField>
              <Button
                type="primary"
                style={styles.btnUpdate}
                htmlType="submit"
                onClick={handleAddAddress}
              >
                Cập nhật
              </Button>
            </StyledField>
          </StyledFormControl>
        </Form>
      </StyledInner>
    </>
  );
}

export default AddAddress;
