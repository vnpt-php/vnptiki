import { Helmet } from "react-helmet";

function TitleAddress() {
  return (
    <Helmet>
      <title>Sổ địa chỉ (Addres Book) | VNPTiki</title>
    </Helmet>
  );
}

export default TitleAddress;
