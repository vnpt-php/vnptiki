import {
  Alert,
  Button,
  DatePicker,
  Form,
  Input,
  Radio,
  Typography,
} from "antd";
import styled from "styled-components";
import { CountryDropdown } from "react-country-region-selector";
import { useCallback, useEffect, useState } from "react";
import { useApi } from "../../hooks/useApi";
import { ApiResponse } from "../../models/ApiResponse";
import { User } from "../../models/types/User";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { StyledAlertWrapper } from "../styled/styled-components";

const dateFormat = "DD";
const monthFormat = "MM";
const yearFormat = "YYYY";

const StyledHeading = styled.div`
  font-size: 20px;
  line-height: 32px;
  font-weight: 300;
  margin: 4px 0 12px;
`;

const StyledInfo = styled.div`
  display: flex;
  flex-wrap: nowrap;
  -webkit-box-pack: justify;
  justify-content: space-between;
  background-color: rgb(255, 255, 255);
  border-radius: 8px;
`;

const StyledInfoLeft = styled.div`
  width: 553px;
  padding: 16px 24px 16px 16px;
`;

const StyledInfoVertical = styled.div`
  border-left: 1px solid rgb(235, 235, 240);
  margin: 16px 0;
`;

const StyledInfoRight = styled.div`
  width: calc(100% - 553px);
  padding: 16px 16px 16px 24px;
  display: flex;
  flex-direction: column;
`;

const StyledAccountInfo = styled.div`
  background-color: rgb(255, 255, 255);
  border-radius: 4px;
  margin-top: 16px;
`;

const StyledFormInfo = styled.div`
  display: flex;
  flex-direction: row;
`;

const StyledAvatar = styled.div`
  background-color: rgb(240, 248, 255);
  border-radius: 50%;
  width: 112px;
  height: 112px;
  border: 4px solid rgb(194, 255, 255);
  position: relative;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
`;
const StyledAvatarView = styled.div`
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
  cursor: pointer;
  width: 100%;
  height: 100%;
`;

const StyledEdit = styled.div`
  position: absolute;
  width: 16px;
  height: 16px;
  background-color: rgb(100, 100, 109);
  border-radius: 12px;
  right: 3px;
  bottom: 5px;
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  align-items: center;
`;

const StyledFormName = styled.div`
  width: 100%;
  display: flex;
  -webkit-box-pack: justify;
  justify-content: space-between;
  flex-direction: column;
`;

const StyledFormControl = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  margin: 0 0 34px;
`;

const StyledField = styled.div`
  display: flex;
  flex: 1 1 0%;
  position: relative;
  z-index: 1;
`;

const StyledInputField = styled.input`
  width: 100%;
  height: 36px;
  border-radius: 4px;
  padding: 0 12px;
  outline: none;
  line-height: 20px;
  border: 1px solid rgb(196, 196, 207);
  ::placeholder {
    font-weight: 600;
    color: rgb(196, 196, 207);
  }
`;

const StyledListItem = styled.div`
  background-color: rgb(255, 255, 255);
  border-radius: 4px;
  margin-bottom: 16px;
`;

const StyledItem = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  width: 100%;
  -webkit-box-pack: justify;
  justify-content: space-between;
  padding: 19px 0;
  border-top: 1px solid rgb(248, 248, 248);
`;

const StyledInfoInfo = styled.div`
  display: flex;
  flex-wrap: nowrap;
  -webkit-box-pack: justify;
  justify-content: space-between;
  background-color: rgb(255, 255, 255);
  border-radius: 8px;
`;

const StyledInfoDetail = styled.div`
  display: flex;
  flex-direction: column;
`;

const StyledStatus = styled.div`
  display: flex;
  -webkit-box-pack: justify;
  justify-content: space-between;
  background-color: rgb(255, 255, 255);
  flex: 1 1 0%;
`;

const StyledAvatarModalOverlay = styled.div`
  position: fixed;
  inset: 0;
  background: rgba(0, 0, 0, 0.53);
  overflow-y: scroll;
  z-index: 999;
`;

const StyledAvatarModal = styled.div`
  position: relative;
  inset: 0;
  border: none;
  background: rgb(255, 255, 255);
  overflow-y: unset;
  border-radius: 20px;
  padding: 24px;
  width: 650px;
  margin: 80px auto;
`;

const StyledUploadImg = styled.div`
  position: relative;
  width: 100%;
`;

const StyledHeaderModal = styled.div`
  border-bottom: 1px solid rgb(235, 235, 240);
  display: flex;
  flex-direction: row;
  -webkit-box-pack: justify;
  justify-content: space-between;
  -webkit-box-align: center;
  align-items: center;
  padding: 16px;
  margin-bottom: 24px;
`;

const StyledUploadFile = styled.div`
  height: 100%;
  width: 100%;
  background: rgb(245, 245, 250);
`;

const StyledImgSelect = styled.div`
  display: flex;
  -webkit-box-pack: center;
  justify-content: center;
  text-align: center;
  -webkit-box-align: center;
  align-items: center;
  border-radius: 8px;
  border: 2px dashed rgb(196, 196, 207);
  height: 90%;
  margin: 12px;
`;

const StyledCountryDropdown = styled(CountryDropdown)`
  width: 100%;
  height: 36px;
  border-radius: 4px;
  padding: 10px 12px;
  line-height: 20px;
  outline: none;
  border: 1px solid rgb(196, 196, 207);
`;

const styles = {
  infoTitle: {
    fontSize: "16px",
    lineHeight: "24px",
    fontWeight: 400,
    color: "rgb(100, 100, 109)",
  },

  inputLabel: {
    width: "110px",
    minWidth: "110px",
    fontSize: "14px",
    color: "rgb(51, 51, 51)",
    marginRight: "16px",
  },

  btnUpdate: {
    height: "28px",
    padding: "0 12px",
  },

  btnLinked: {
    border: 0,
    color: "rgb(11, 116, 229)",
    backgroundColor: "#cccccc",
  },
};

function AccountInfo() {
  const [openAvtModal, setOpenAvtModal] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState("");
  const [avatar, setAvatar] = useState<File | null>(null);

  const { callApi } = useApi();
  const [dataUser, setDataUser] = useState<User | undefined>();
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const getDataUser = useCallback(() => {
    callApi<ApiResponse<User>>(REQUEST_TYPE.GET, "api/user/current")
      .then((response) => {
        setDataUser(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const handleEditInfo = useCallback(() => {
    if (!avatar) {
      return;
    }
    const formData = new FormData();
    formData.append("avatar", avatar);

    callApi<ApiResponse<User>>(
      REQUEST_TYPE.POST,
      "api/user/upload-avatar",
      formData
    )
      .then((response) => {
        setSuccess("Thay đổi thông tin thành công");
        window.location.reload();
      })
      .catch((error) => {
        setError("Thay đổi thông tin thất bại");
        console.log(error);
      });
  }, [callApi, avatar]);

  useEffect(() => {
    getDataUser();
  }, []);

  function onChangeName(event: any) {
    if (dataUser) {
      setDataUser({ ...dataUser, name: event.target.value });
    }
  }

  function onChangeDate() {}

  function onChangeMonth() {}

  function onChangeYear() {}

  function onChangeGender(e: any) {
    console.log(`Gender changed to ${e.target.value}`);
  }

  function handleCountryChange(value: any) {
    setSelectedCountry(value);
  }

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const selectedImage = e.target.files?.[0];
    if (selectedImage) {
      setAvatar(selectedImage);
    }
  };

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledHeading>Thông tin tài khoản</StyledHeading>

      <StyledInfo>
        <StyledInfoLeft>
          <Typography.Text style={styles.infoTitle}>
            Thông tin cá nhân
          </Typography.Text>

          <StyledAccountInfo>
            <Form>
              {/* Avatar */}
              <StyledFormInfo>
                <div style={{ marginRight: 16 }}>
                  <StyledAvatar>
                    <StyledAvatarView>
                      {dataUser?.avatar ? (
                        <img
                          src={dataUser.avatar}
                          alt="avatar"
                          style={{
                            borderRadius: "50%",
                            width: "100%",
                            height: "100%",
                          }}
                          onClick={() => setOpenAvtModal(true)}
                        />
                      ) : (
                        <img
                          src="  https://frontend.tikicdn.com/_desktop-next/static/img/account/avatar.png"
                          alt="avatar"
                          height={50}
                          width={50}
                          onClick={() => setOpenAvtModal(true)}
                        />
                      )}
                      <StyledEdit>
                        <img
                          src="https://frontend.tikicdn.com/_desktop-next/static/img/account/edit.png"
                          alt=""
                          width={10}
                          height={10}
                          onClick={() => setOpenAvtModal(true)}
                        />
                      </StyledEdit>
                    </StyledAvatarView>
                  </StyledAvatar>
                </div>

                {/* Họ tên & Nickname */}
                <StyledFormName>
                  <StyledFormControl>
                    <Typography.Text style={styles.inputLabel}>
                      Họ & Tên
                    </Typography.Text>
                    <StyledField>
                      <StyledInputField
                        value={dataUser?.name ?? ""}
                        onChange={onChangeName}
                        placeholder="Thêm họ tên"
                      />
                    </StyledField>
                  </StyledFormControl>

                  <StyledFormControl>
                    <Typography.Text style={styles.inputLabel}>
                      Nickname
                    </Typography.Text>
                    <StyledField>
                      <StyledInputField disabled placeholder="Thêm nickname" />
                    </StyledField>
                  </StyledFormControl>
                </StyledFormName>
              </StyledFormInfo>

              {/* Ngày sinh */}
              <StyledFormControl>
                <Typography.Text style={styles.inputLabel}>
                  Ngày sinh
                </Typography.Text>

                <DatePicker
                  picker="date"
                  format={dateFormat}
                  onChange={onChangeDate}
                  placeholder="Ngày"
                  disabled
                />
                <DatePicker
                  picker="month"
                  format={monthFormat}
                  onChange={onChangeMonth}
                  placeholder="Tháng"
                  style={{ margin: "0 16px" }}
                  disabled
                />
                <DatePicker
                  picker="year"
                  format={yearFormat}
                  onChange={onChangeYear}
                  placeholder="Năm"
                  disabled
                />
              </StyledFormControl>

              {/* Giới tính */}
              <StyledFormControl>
                <Typography.Text style={styles.inputLabel}>
                  Giới tính
                </Typography.Text>

                <Radio.Group onChange={onChangeGender} disabled>
                  <Radio value="male">Nam</Radio>
                  <Radio value="female">Nữ</Radio>
                  <Radio value="other">Khác</Radio>
                </Radio.Group>
              </StyledFormControl>

              {/* Quốc tịch */}
              <StyledFormControl>
                <Typography.Text style={styles.inputLabel}>
                  Quốc tịch
                </Typography.Text>

                <StyledCountryDropdown
                  disabled
                  value={selectedCountry}
                  onChange={handleCountryChange}
                />
              </StyledFormControl>

              {/* Button Lưu */}
              <StyledFormControl>
                <Typography.Text style={styles.inputLabel}>
                  &nbsp;
                </Typography.Text>

                <Button
                  type="primary"
                  htmlType="submit"
                  style={{ height: "40px", width: "176px" }}
                  onClick={handleEditInfo}
                >
                  Lưu thay đổi
                </Button>
              </StyledFormControl>
            </Form>
          </StyledAccountInfo>
        </StyledInfoLeft>

        <StyledInfoVertical />

        <StyledInfoRight>
          <Typography.Text style={styles.infoTitle}>
            Số điện thoại và Email
          </Typography.Text>
          <StyledListItem>
            {/* Số điện thoại */}
            <StyledItem style={{ border: "none" }}>
              <StyledInfoInfo>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/phone.png"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />

                <StyledInfoDetail>
                  <Typography.Text>Số điện thoại</Typography.Text>
                  <Typography.Text>{dataUser?.phone}</Typography.Text>
                </StyledInfoDetail>
              </StyledInfoInfo>

              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Cập nhật
                </Button>
              </StyledStatus>
            </StyledItem>

            {/* Email */}
            <StyledItem>
              <StyledInfoInfo>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/email.png"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />

                <StyledInfoDetail>
                  <Typography.Text>Địa chỉ email</Typography.Text>
                  <Typography.Text>{dataUser?.email}</Typography.Text>
                </StyledInfoDetail>
              </StyledInfoInfo>

              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Cập nhật
                </Button>
              </StyledStatus>
            </StyledItem>
          </StyledListItem>

          <Typography.Text style={styles.infoTitle}>Bảo mật</Typography.Text>
          <StyledListItem style={{ border: "none" }}>
            {/* Mật khẩu */}
            <StyledItem>
              <div style={{ display: "flex", alignItems: "center" }}>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/lock.png"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />
                <Typography.Text>Thiết lập mật khẩu</Typography.Text>
              </div>

              {/* Mã PIN */}
              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Cập nhật
                </Button>
              </StyledStatus>
            </StyledItem>

            <StyledItem>
              <div style={{ display: "flex", alignItems: "center" }}>
                <img
                  src="https://salt.tikicdn.com/ts/upload/99/50/d7/cc0504daa05199e1fb99cd9a89e60fa5.jpg"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />
                <Typography.Text>Thiết lập mã PIN</Typography.Text>
              </div>

              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Thiết lập
                </Button>
              </StyledStatus>
            </StyledItem>
          </StyledListItem>

          <Typography.Text style={styles.infoTitle}>
            Liên kết mạng xã hội
          </Typography.Text>
          <StyledListItem style={{ marginBottom: 0 }}>
            {/* Facebook */}
            <StyledItem style={{ border: "none" }}>
              <div style={{ display: "flex", alignItems: "center" }}>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/facebook.png"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />
                <Typography.Text>Facebook</Typography.Text>
              </div>

              {/* Google */}
              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Liên kết
                </Button>
              </StyledStatus>
            </StyledItem>

            <StyledItem>
              <div style={{ display: "flex", alignItems: "center" }}>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/google.png"
                  alt=""
                  width={24}
                  height={24}
                  style={{ marginRight: 4 }}
                />
                <Typography.Text>Google</Typography.Text>
              </div>

              <StyledStatus>
                <Typography.Text></Typography.Text>
                <Button type="default" style={styles.btnUpdate}>
                  Liên kết
                </Button>
                {/* <Button
                  type="default"
                  style={{ ...styles.btnUpdate, ...styles.btnLinked }}
                >
                  Đã liên kết
                </Button> */}
              </StyledStatus>
            </StyledItem>
          </StyledListItem>
        </StyledInfoRight>
      </StyledInfo>

      {/* Modal */}
      <StyledAvatarModalOverlay
        className={`${openAvtModal ? "block" : "hidden"}`}
      >
        <StyledAvatarModal>
          <StyledUploadImg>
            <StyledHeaderModal>
              <Typography.Title level={5} style={{ margin: 0 }}>
                Cập nhật ảnh đại diện
              </Typography.Title>
              <img
                src="https://salt.tikicdn.com/ts/upload/fe/20/d7/6d7764292a847adcffa7251141eb4730.png"
                alt=""
                height={36}
                width={36}
                style={{ cursor: "pointer" }}
                onClick={() => setOpenAvtModal(false)}
              />
            </StyledHeaderModal>

            <div style={{ width: "100%", height: 350, position: "relative" }}>
              <Input
                id="fileInput"
                type="file"
                style={{ display: "none" }}
                onChange={handleFileChange}
              />

              <label htmlFor="fileInput">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    height: 300,
                    position: "absolute",
                    width: "100%",
                  }}
                >
                  {avatar && (
                    <img
                      src={URL.createObjectURL(avatar)}
                      alt="Selected"
                      height="300px"
                      width="300px"
                    />
                  )}

                  <StyledUploadFile>
                    <StyledImgSelect>
                      <Typography.Text
                        style={{ color: "rgb(11, 116, 229)", fontSize: 16 }}
                      >
                        Nhấn để chọn hoặc kéo thả hình ảnh vào khung này.
                      </Typography.Text>
                    </StyledImgSelect>
                  </StyledUploadFile>
                </div>
              </label>
            </div>
          </StyledUploadImg>
        </StyledAvatarModal>
      </StyledAvatarModalOverlay>
    </>
  );
}

export default AccountInfo;
