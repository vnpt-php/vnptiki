import { Menu } from "antd";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { FaUser, FaBell, FaShoppingCart, FaReceipt } from "react-icons/fa";
import { IoLocationSharp } from "react-icons/io5";
import { BsEyeFill, BsFillHeartFill } from "react-icons/bs";
import { MdOutlinePayment } from "react-icons/md";
import { ImBoxRemove } from "react-icons/im";
import { FcRating } from "react-icons/fc";
import { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { baseUrl } from "../../constants/url";
import { User } from "../../models/types/User";
import { ApiResponse } from "../../models/ApiResponse";

const StyledAvatar = styled.div`
  display: flex;
  -webkit-box-align: center;
  align-items: center;
  padding-left: 7px;
  margin: 0px 0px 12px;
`;

const StyledInfo = styled.div`
  flex: 1 1 0%;
  font-size: 13px;
  line-height: 15px;
  color: rgb(51, 51, 51);
  font-weight: 400;
`;

const styles = {
  sidebar: {
    width: "250px",
    marginRight: "17px",
  },

  avatarImg: {
    margin: "0 12px 0 0",
    borderRadius: "50%",
  },

  textStrong: {
    fontSize: "16px",
    lineHeight: "19px",
    fontWeight: "400",
    display: "block",
    margin: "4px 0 0",
  },

  icon: {
    marginRight: "22px",
    fontSize: "20px",
    verticalAlign: "middle",
  },
};

const menuItems = [
  {
    key: "1",
    icon: <FaUser style={styles.icon} />,
    label: (
      <Link to="/customer/account-info" style={{ verticalAlign: "middle" }}>
        Thông tin tài khoản
      </Link>
    ),
  },
  {
    key: "2",
    icon: <FaBell style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Thông báo của tôi
      </Link>
    ),
  },
  {
    key: "3",
    icon: <FaReceipt style={styles.icon} />,
    label: (
      <Link to="/sales/order/history" style={{ verticalAlign: "middle" }}>
        Quản lý đơn hàng
      </Link>
    ),
  },
  {
    key: "4",
    icon: <ImBoxRemove style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Quản lý đổi trả
      </Link>
    ),
  },
  {
    key: "5",
    icon: <IoLocationSharp style={styles.icon} />,
    label: (
      <Link to="/customer/address" style={{ verticalAlign: "middle" }}>
        Sổ địa chỉ
      </Link>
    ),
  },
  {
    key: "6",
    icon: <MdOutlinePayment style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Thông tin thanh toán
      </Link>
    ),
  },
  {
    key: "7",
    icon: <FcRating style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Đánh giá sản phẩm
      </Link>
    ),
  },
  {
    key: "8",
    icon: <BsEyeFill style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Sản phẩm bạn đã xem
      </Link>
    ),
  },
  {
    key: "9",
    icon: <BsFillHeartFill style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Sản phẩm yêu thích
      </Link>
    ),
  },
  {
    key: "10",
    icon: <FaShoppingCart style={styles.icon} />,
    label: (
      <Link to="" style={{ verticalAlign: "middle" }}>
        Sản phẩm mua sau
      </Link>
    ),
  },
];

function AccountSidebar(props: any) {
  const navigate = useNavigate();
  const [data, setData] = useState<User | undefined>();
  const token = sessionStorage.getItem("ACCESS_TOKEN");

  const getUser = useCallback(async () => {
    try {
      const { data: userData } = await axios.get<ApiResponse<User>>(
        `${baseUrl}/api/user/current`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      if (userData && userData.status === 200) {
        setData(userData.data);
      } else {
        console.log(userData.error);
      }
    } catch (error) {
      console.log(error);
    }
  }, [token]);

  useEffect(() => {
    if (!token) {
      navigate("/");
    }

    getUser();
  }, []);

  return (
    <aside style={styles.sidebar}>
      <StyledAvatar>
        {data?.avatar ? (
          <img
            src={data.avatar}
            alt=""
            width={45}
            height={45}
            style={styles.avatarImg}
          />
        ) : (
          <img
            src="https://salt.tikicdn.com/desktop/img/avatar.png"
            alt=""
            width={45}
            style={styles.avatarImg}
          />
        )}

        <StyledInfo>
          Tài khoản của <br />
          <strong style={styles.textStrong}>{data?.name}</strong>
        </StyledInfo>
      </StyledAvatar>

      <Menu
        mode="inline"
        defaultSelectedKeys={[props.defaultSelectedKey]}
        style={{ background: "none", border: "none" }}
        items={menuItems}
      />
    </aside>
  );
}

export default AccountSidebar;
