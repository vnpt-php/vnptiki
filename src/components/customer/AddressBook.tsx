import { Alert, Button, Typography } from "antd";
import styled from "styled-components";
import { GoPlus } from "react-icons/go";
import { IoIosCheckmarkCircleOutline } from "react-icons/io";
import { Link } from "react-router-dom";
import { CSSProperties, useCallback, useEffect, useState } from "react";
import { useApi } from "../../hooks/useApi";
import { Address } from "../../models/types/Address";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { ApiResponse } from "../../models/ApiResponse";
import { StyledAlertWrapper } from "../styled/styled-components";

const StyledHeading = styled.div`
  font-size: 20px;
  line-height: 32px;
  font-weight: 300;
  margin: 4px 0 12px;
`;

const StyledItem = styled.div`
  background-color: rgb(255, 255, 255);
  padding: 17px;
  border-radius: 4px;
  margin: 0 0 10px;
  display: flex;
  -webkit-box-pack: justify;
  justify-content: space-between;
  font-size: 13px;
  line-height: 19px;
  :last-child {
    margin: 0;
  }
`;

const StyledName = styled.div`
  text-transform: uppercase;
  margin: 0 0 10px;
  display: flex;
  align-items: center;
`;

const styles = {
  addAddress: {
    backgroundColor: "rgba(255, 255, 255)",
    height: "60px",
    border: "1px dashed rgb(216, 216, 216)",
    margin: "0 0 10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    textDecoration: "none",
  },

  iconAdd: {
    color: "rgb(120, 120, 120)",
    fontSize: "28px",
    margin: "0 20px",
  },

  addressDefault: {
    fontSize: "12px",
    margin: "0 0 0 15px",
    display: "inline-flex",
    alignItems: "center",
    textTransform: "none",
    color: "rgb(38, 188, 78)",
  } as CSSProperties,

  btnEdit: {
    fontSize: "14px",
    display: "inline-block",
    padding: "6px 12px",
    textDecoration: "none",
    color: "rgb(11, 116, 229)",
  },
};

function AddressBook() {
  const { callApi } = useApi();
  const [dataAddresses, setDataAddresses] = useState<Address[] | undefined>([]);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const getDataAddresses = useCallback(() => {
    callApi<ApiResponse<Address[]>>(REQUEST_TYPE.GET, "api/address")
      .then((response) => {
        setDataAddresses(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const changeDefaultAddress = useCallback(
    (idAddress: string) => {
      callApi<ApiResponse<Address>>(
        REQUEST_TYPE.PUT,
        `api/address/set-default/${idAddress}`
      )
        .then((response) => {
          setSuccess("Thay đổi địa chỉ mặc định thành công");
          window.location.reload();
        })
        .catch((error) => {
          setError("Thay đổi địa chỉ mặc định thất bại");
          console.log(error);
        });
    },
    [callApi]
  );

  useEffect(() => {
    getDataAddresses();
  }, []);

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledHeading>Sổ địa chỉ</StyledHeading>

      <div>
        <Link to="/customer/address/create" style={styles.addAddress}>
          <GoPlus style={styles.iconAdd} />
          <Typography.Text style={{ color: "rgb(11, 116, 229)", fontSize: 15 }}>
            Thêm địa chỉ mới
          </Typography.Text>
        </Link>

        {dataAddresses &&
          dataAddresses.map((value, key) => (
            <StyledItem key={key}>
              <div>
                <StyledName>
                  {value.name}
                  {value.default ? (
                    <Typography.Text style={styles.addressDefault}>
                      <IoIosCheckmarkCircleOutline style={{ marginRight: 5 }} />
                      Địa chỉ mặc định
                    </Typography.Text>
                  ) : (
                    ""
                  )}
                </StyledName>

                <div style={{ margin: "0 0 5px" }}>
                  <Typography.Text style={{ color: "rgb(120, 120, 120)" }}>
                    Địa chỉ:{" "}
                  </Typography.Text>
                  {value.detail}, {value.ward}, {value.district}, {value.city}
                </div>

                <div>
                  <Typography.Text style={{ color: "rgb(120, 120, 120)" }}>
                    Điện thoại:{" "}
                  </Typography.Text>
                  {value.phone}
                </div>
              </div>

              <div>
                {!value.default ? (
                  <Button
                    type="ghost"
                    style={{ color: "rgb(11, 116, 229)" }}
                    onClick={() => changeDefaultAddress(value.id)}
                  >
                    Mặc định
                  </Button>
                ) : (
                  ""
                )}

                <Link
                  to={`/customer/address/edit/${value.id}`}
                  style={styles.btnEdit}
                >
                  Chỉnh sửa
                </Link>
              </div>
            </StyledItem>
          ))}
      </div>
    </>
  );
}

export default AddressBook;
