import { Button, Input, Tabs, Typography } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import styled from "styled-components";
import { BiSearch } from "react-icons/bi";
import { TbBuildingStore } from "react-icons/tb";
import { FcCancel } from "react-icons/fc";
import { CSSProperties } from "react";
import { useApi } from "../../hooks/useApi";
import { useCallback, useEffect, useState } from "react";
import { ApiResponse } from "../../models/ApiResponse";
import { Order } from "../../models/types/Order";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { CgToolbox } from "react-icons/cg";
import { CiDeliveryTruck } from "react-icons/ci";
import { BsHouseCheck } from "react-icons/bs";

const { Search } = Input;

const StyledHeading = styled.div`
  font-size: 20px;
  line-height: 32px;
  font-weight: 300;
  margin: 4px 0 12px;
`;

const StyledSearch = styled.div`
  width: 100%;
  position: relative;
  margin: 12px 0;
`;

const StyledComponent = styled.div`
  height: auto;
  overflow: auto;
  display: flex;
  flex-direction: column;
`;

const StyledEmpty = styled.div`
  display: flex;
  flex-direction: column;
  -webkit-box-align: center;
  align-items: center;
  background-color: rgb(255, 255, 255);
  padding: 35px;
`;

const StyledOrder = styled.div`
  background: rgb(255, 255, 255);
  border-radius: 4px;
  font-size: 13px;
  margin-bottom: 20px;
  padding: 16px;
`;

const StyledOrderFooter = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin-top: 12px;
  width: 100%;
`;

const StyledOrderHeader = styled.div`
  border-bottom: 1px solid rgb(235, 235, 240);
  padding-bottom: 12px;
  color: rgb(128, 128, 137);
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
`;

const StyledProduct = styled.div`
  display: flex;
  flex-direction: row;
  padding: 16px 0px;
  border-bottom: 1px solid rgb(235, 235, 240);
  -webkit-box-pack: justify;
  justify-content: space-between;
  cursor: pointer;
`;

const StyledProductImg = styled.div`
  flex-shrink: 0;
  width: 80px;
  height: 80px;
  border-radius: 4px;
  border: 0.5px solid rgb(238, 238, 238);
  background-repeat: no-repeat;
  background-size: 90%;
  background-position: center center;
  position: relative;
`;

const StyledProductInfo = styled.div`
  margin: 0px 12px;
  display: flex;
  flex-direction: column;
`;

const StyledStore = styled.div`
  margin-top: 4px;
  display: flex;
  align-items: center;
  color: rgb(128, 128, 137);
`;

const StyledPrice = styled.div`
  min-width: 120px;
  -webkit-box-pack: end;
  justify-content: flex-end;
  display: flex;
`;

const StyledTotalMoney = styled.div`
  display: flex;
  margin-bottom: 12px;
`;

const styles = {
  quantityProduct: {
    fontSize: "12px",
    lineHeight: "16px",
    fontWeight: "400",
    color: "rgb(128, 128, 137)",
    textAlign: "center",
    position: "absolute",
    width: "28px",
    height: "28px",
    backgroundColor: "rgb(235, 235, 240)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    right: "0",
    bottom: "0",
    borderTopLeftRadius: "10px",
  } as CSSProperties,

  canceled: {
    color: "rgb(128, 128, 137)",
    display: "flex",
    alignItems: "center",
  },

  ordered: {
    color: "rgb(121, 172, 213)",
    display: "flex",
    alignItems: "center",
  },

  delivering: {
    color: "rgb(183, 45, 190)",
    display: "flex",
    alignItems: "center",
  },

  delivered: {
    color: "rgb(54, 199, 102)",
    display: "flex",
    alignItems: "center",
  },

  totalMoney: {
    fontWeight: "300",
    fontSize: "17px",
    color: "rgb(128, 128, 137)",
    marginRight: "8px",
  },

  btn: {
    height: "100%",
    padding: "8px 8px",
  },
};

function OrderManagement() {
  const { callApi } = useApi();
  const [dataOrders, setDataOrders] = useState<Order[]>([]);
  const [dataOrdered, setDataOrdered] = useState<Order[]>([]);
  const [dataDelivering, setDataDelivering] = useState<Order[]>([]);
  const [dataDelivered, setDataDelivered] = useState<Order[]>([]);
  const [dataCanceled, setDataCanceled] = useState<Order[]>([]);

  const getDataOrders = useCallback(() => {
    callApi<ApiResponse<Order[]>>(REQUEST_TYPE.GET, "api/order/my-order")
      .then((response) => {
        setDataOrders(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataOrdered = useCallback(() => {
    callApi<ApiResponse<Order[]>>(REQUEST_TYPE.GET, "api/orders/status=ordered")
      .then((response) => {
        setDataOrdered(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataDelivering = useCallback(() => {
    callApi<ApiResponse<Order[]>>(
      REQUEST_TYPE.GET,
      "api/orders/status=delivering"
    )
      .then((response) => {
        setDataDelivering(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataDelivered = useCallback(() => {
    callApi<ApiResponse<Order[]>>(
      REQUEST_TYPE.GET,
      "api/orders/status=delivered"
    )
      .then((response) => {
        setDataDelivered(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  const getDataCanceled = useCallback(() => {
    callApi<ApiResponse<Order[]>>(
      REQUEST_TYPE.GET,
      "api/orders/status=canceled"
    )
      .then((response) => {
        setDataCanceled(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  useEffect(() => {
    getDataOrders();
    getDataOrdered();
    getDataDelivering();
    getDataDelivered();
    getDataCanceled();
  }, []);

  return (
    <>
      <StyledHeading>Đơn hàng của tôi</StyledHeading>

      <StyledSearch>
        <Search
          prefix={<BiSearch />}
          placeholder="Tìm đơn hàng theo Tên sản phẩm"
          allowClear
          enterButton="Tìm đơn hàng"
          size="large"
        />
      </StyledSearch>

      <Tabs
        defaultActiveKey="1"
        centered
        style={{
          backgroundColor: "#fff",
          color: "rgb(128, 128, 137)",
        }}
      >
        <TabPane tab="Tất cả đơn" key="1">
          {dataOrders ? (
            dataOrders.map((value) => (
              <>
                {value.order_items.map((item) => (
                  <StyledOrder key={item.id}>
                    <StyledProduct>
                      <div style={{ display: "flex" }}>
                        <StyledProductImg
                          style={{
                            backgroundImage: `url(${item.product.product_images[0]?.url})`,
                          }}
                        >
                          <Typography.Text style={styles.quantityProduct}>
                            x{item.quantity}
                          </Typography.Text>
                        </StyledProductImg>

                        <StyledProductInfo>
                          <Typography.Paragraph style={{ marginBottom: 6 }}>
                            {item.product.name}
                          </Typography.Paragraph>

                          <StyledStore>
                            <TbBuildingStore
                              style={{ fontSize: 16, marginRight: 6 }}
                            />
                            {value.store.name}
                          </StyledStore>
                        </StyledProductInfo>
                      </div>

                      <StyledPrice>
                        <Typography.Text>
                          {item.product.price} ₫
                        </Typography.Text>
                      </StyledPrice>
                    </StyledProduct>

                    <StyledOrderFooter>
                      <StyledTotalMoney>
                        <Typography.Text style={styles.totalMoney}>
                          Tổng tiền: {item.product.price * item.quantity}
                        </Typography.Text>

                        <Typography.Text style={{ fontSize: 17 }}>
                          ₫
                        </Typography.Text>
                      </StyledTotalMoney>

                      <Button type="default" style={styles.btn}>
                        Xem chi tiết
                      </Button>
                    </StyledOrderFooter>
                  </StyledOrder>
                ))}
              </>
            ))
          ) : (
            <StyledComponent>
              <StyledEmpty>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png"
                  alt=""
                  width={200}
                  height={200}
                />

                <Typography.Paragraph style={{ fontSize: 16, marginTop: 15 }}>
                  Chưa có đơn hàng
                </Typography.Paragraph>
              </StyledEmpty>
            </StyledComponent>
          )}
        </TabPane>

        <TabPane tab="Đã đặt" key="2">
          {dataOrdered ? (
            dataOrdered.map((data) => (
              <>
                {data.order_items.map((item) => (
                  <StyledOrder key={item.id}>
                    <StyledOrderHeader>
                      <Typography.Text style={styles.ordered}>
                        <CgToolbox
                          style={{
                            fontSize: 20,
                            marginRight: 6,
                          }}
                        />
                        Đã đặt
                      </Typography.Text>
                    </StyledOrderHeader>

                    <StyledProduct>
                      <div style={{ display: "flex" }}>
                        <StyledProductImg
                          style={{
                            backgroundImage: `url(${item.product.product_images[0]?.url})`,
                          }}
                        >
                          <Typography.Text style={styles.quantityProduct}>
                            x{item.quantity}
                          </Typography.Text>
                        </StyledProductImg>

                        <StyledProductInfo>
                          <Typography.Paragraph style={{ marginBottom: 6 }}>
                            {item.product.name}
                          </Typography.Paragraph>

                          <StyledStore>
                            <TbBuildingStore
                              style={{ fontSize: 16, marginRight: 6 }}
                            />
                            {data.store.name}
                          </StyledStore>
                        </StyledProductInfo>
                      </div>

                      <StyledPrice>
                        <Typography.Text>
                          {item.product.price} ₫
                        </Typography.Text>
                      </StyledPrice>
                    </StyledProduct>

                    <StyledOrderFooter>
                      <StyledTotalMoney>
                        <Typography.Text style={styles.totalMoney}>
                          Tổng tiền: {item.product.price * item.quantity}
                        </Typography.Text>

                        <Typography.Text style={{ fontSize: 17 }}>
                          ₫
                        </Typography.Text>
                      </StyledTotalMoney>

                      <Button type="default" style={styles.btn}>
                        Xem chi tiết
                      </Button>
                    </StyledOrderFooter>
                  </StyledOrder>
                ))}
              </>
            ))
          ) : (
            <StyledComponent>
              <StyledEmpty>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png"
                  alt=""
                  width={200}
                  height={200}
                />

                <Typography.Paragraph style={{ fontSize: 16, marginTop: 15 }}>
                  Chưa có đơn hàng
                </Typography.Paragraph>
              </StyledEmpty>
            </StyledComponent>
          )}
        </TabPane>

        <TabPane tab="Đang vận chuyển" key="3">
          {dataDelivering ? (
            dataDelivering.map((data) => (
              <>
                {data.order_items.map((item) => (
                  <StyledOrder key={item.id}>
                    <StyledOrderHeader>
                      <Typography.Text style={styles.delivering}>
                        <CiDeliveryTruck
                          style={{
                            fontSize: 20,
                            marginRight: 6,
                          }}
                        />
                        Đang vận chuyển
                      </Typography.Text>
                    </StyledOrderHeader>

                    <StyledProduct>
                      <div style={{ display: "flex" }}>
                        <StyledProductImg
                          style={{
                            backgroundImage: `url(${item.product.product_images[0]?.url})`,
                          }}
                        >
                          <Typography.Text style={styles.quantityProduct}>
                            x{item.quantity}
                          </Typography.Text>
                        </StyledProductImg>

                        <StyledProductInfo>
                          <Typography.Paragraph style={{ marginBottom: 6 }}>
                            {item.product.name}
                          </Typography.Paragraph>

                          <StyledStore>
                            <TbBuildingStore
                              style={{ fontSize: 16, marginRight: 6 }}
                            />
                            {data.store.name}
                          </StyledStore>
                        </StyledProductInfo>
                      </div>

                      <StyledPrice>
                        <Typography.Text>
                          {item.product.price} ₫
                        </Typography.Text>
                      </StyledPrice>
                    </StyledProduct>

                    <StyledOrderFooter>
                      <StyledTotalMoney>
                        <Typography.Text style={styles.totalMoney}>
                          Tổng tiền: {item.product.price * item.quantity}
                        </Typography.Text>

                        <Typography.Text style={{ fontSize: 17 }}>
                          ₫
                        </Typography.Text>
                      </StyledTotalMoney>

                      <Button type="default" style={styles.btn}>
                        Xem chi tiết
                      </Button>
                    </StyledOrderFooter>
                  </StyledOrder>
                ))}
              </>
            ))
          ) : (
            <StyledComponent>
              <StyledEmpty>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png"
                  alt=""
                  width={200}
                  height={200}
                />

                <Typography.Paragraph style={{ fontSize: 16, marginTop: 15 }}>
                  Chưa có đơn hàng
                </Typography.Paragraph>
              </StyledEmpty>
            </StyledComponent>
          )}
        </TabPane>

        <TabPane tab="Đã giao" key="4">
          {dataDelivered ? (
            dataDelivered.map((data) => (
              <>
                {data.order_items.map((item) => (
                  <StyledOrder key={item.id}>
                    <StyledOrderHeader>
                      <Typography.Text style={styles.delivered}>
                        <BsHouseCheck
                          style={{
                            fontSize: 20,
                            marginRight: 6,
                          }}
                        />
                        Đã giao
                      </Typography.Text>
                    </StyledOrderHeader>

                    <StyledProduct>
                      <div style={{ display: "flex" }}>
                        <StyledProductImg
                          style={{
                            backgroundImage: `url(${item.product.product_images[0]?.url})`,
                          }}
                        >
                          <Typography.Text style={styles.quantityProduct}>
                            x{item.quantity}
                          </Typography.Text>
                        </StyledProductImg>

                        <StyledProductInfo>
                          <Typography.Paragraph style={{ marginBottom: 6 }}>
                            {item.product.name}
                          </Typography.Paragraph>

                          <StyledStore>
                            <TbBuildingStore
                              style={{ fontSize: 16, marginRight: 6 }}
                            />
                            {data.store.name}
                          </StyledStore>
                        </StyledProductInfo>
                      </div>

                      <StyledPrice>
                        <Typography.Text>
                          {item.product.price} ₫
                        </Typography.Text>
                      </StyledPrice>
                    </StyledProduct>

                    <StyledOrderFooter>
                      <StyledTotalMoney>
                        <Typography.Text style={styles.totalMoney}>
                          Tổng tiền: {item.product.price * item.quantity}
                        </Typography.Text>

                        <Typography.Text style={{ fontSize: 17 }}>
                          ₫
                        </Typography.Text>
                      </StyledTotalMoney>

                      <Button type="default" style={styles.btn}>
                        Xem chi tiết
                      </Button>
                    </StyledOrderFooter>
                  </StyledOrder>
                ))}
              </>
            ))
          ) : (
            <StyledComponent>
              <StyledEmpty>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png"
                  alt=""
                  width={200}
                  height={200}
                />

                <Typography.Paragraph style={{ fontSize: 16, marginTop: 15 }}>
                  Chưa có đơn hàng
                </Typography.Paragraph>
              </StyledEmpty>
            </StyledComponent>
          )}
        </TabPane>

        <TabPane tab="Đã huỷ" key="5">
          {dataCanceled ? (
            dataCanceled.map((data) => (
              <>
                {data.order_items.map((item) => (
                  <StyledOrder key={item.id}>
                    <StyledOrderHeader>
                      <Typography.Text style={styles.canceled}>
                        <FcCancel
                          style={{
                            fontSize: 20,
                            marginRight: 6,
                          }}
                        />
                        Đã huỷ
                      </Typography.Text>
                    </StyledOrderHeader>

                    <StyledProduct>
                      <div style={{ display: "flex" }}>
                        <StyledProductImg
                          style={{
                            backgroundImage: `url(${item.product.product_images[0]?.url})`,
                          }}
                        >
                          <Typography.Text style={styles.quantityProduct}>
                            x{item.quantity}
                          </Typography.Text>
                        </StyledProductImg>

                        <StyledProductInfo>
                          <Typography.Paragraph style={{ marginBottom: 6 }}>
                            {item.product.name}
                          </Typography.Paragraph>

                          <StyledStore>
                            <TbBuildingStore
                              style={{ fontSize: 16, marginRight: 6 }}
                            />
                            {data.store.name}
                          </StyledStore>
                        </StyledProductInfo>
                      </div>

                      <StyledPrice>
                        <Typography.Text>
                          {item.product.price} ₫
                        </Typography.Text>
                      </StyledPrice>
                    </StyledProduct>

                    <StyledOrderFooter>
                      <StyledTotalMoney>
                        <Typography.Text style={styles.totalMoney}>
                          Tổng tiền: {item.product.price * item.quantity}
                        </Typography.Text>

                        <Typography.Text style={{ fontSize: 17 }}>
                          ₫
                        </Typography.Text>
                      </StyledTotalMoney>

                      <Button type="default" style={styles.btn}>
                        Xem chi tiết
                      </Button>
                    </StyledOrderFooter>
                  </StyledOrder>
                ))}
              </>
            ))
          ) : (
            <StyledComponent>
              <StyledEmpty>
                <img
                  src="https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png"
                  alt=""
                  width={200}
                  height={200}
                />

                <Typography.Paragraph style={{ fontSize: 16, marginTop: 15 }}>
                  Chưa có đơn hàng
                </Typography.Paragraph>
              </StyledEmpty>
            </StyledComponent>
          )}
        </TabPane>
      </Tabs>
    </>
  );
}

export default OrderManagement;
