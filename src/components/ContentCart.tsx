import {
  Table,
  Typography,
  InputNumber,
  Button,
  Space,
  Switch,
  Alert,
} from "antd";
import React, { useState, useContext, useCallback, useEffect } from "react";
import { CaretUpOutlined, CaretDownOutlined } from "@ant-design/icons";
import { Link, useNavigate } from "react-router-dom";
import type { ColumnsType } from "antd/es/table";
import type { TableRowSelection } from "antd/es/table/interface";
import CartContext from "../context/CartContext";
import { Cart } from "../models/types/Cart";
import { CartItem } from "../models/types/CartItem";
import { Address } from "../models/types/Address";
import { useApi } from "../hooks/useApi";
import { ApiResponse } from "../models/ApiResponse";
import { REQUEST_TYPE } from "../enums/RequestType";
import { StyledAlertWrapper } from "./styled/styled-components";

interface IData {
  key: string;
  name: string;
  children: IDataChild[];
}

interface IDataChild {
  key: string;
  name: string;
  quantity: number;
  price: number;
  amount: number;
}

const ContentCart: React.FC = () => {
  const { callApi } = useApi();
  // const { dataCart } = useContext(CartContext);
  const navigate = useNavigate();
  const token = sessionStorage.getItem("ACCESS_TOKEN");
  const [dataCart, setDataCart] = useState<Cart[]>([]);

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [checkStrictly, setCheckStrictly] = useState(false);
  const [dataAddresses, setDataAddresses] = useState<Address[] | undefined>([]);
  const [defaultIdAddress, setDefaultIdAddress] = useState("");
  const [productOrders, setProductOrders] = useState<any[]>([]);
  const [total, setTotal] = useState({
    total: 0,
    count: 0,
  });

  const columns: ColumnsType<IData> = [
    {
      title: `Tất cả (${dataCart
        .map((cart, index) =>
          index === 0
            ? dataCart.reduce((acc, cart) => acc + cart.cart_items.length, 0)
            : ""
        )
        .join(" ")} sản phẩm)`,
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
      render: (value, record) => {
        if (!record.children || Array.isArray(value)) {
          return (
            <InputNumber
              min={1}
              max={99}
              value={value}
              onKeyDown={(event) => {
                const isNumber = /[0-9]/.test(event.key);
                const allowedKeys = [
                  "Backspace",
                  "Delete",
                  "ArrowLeft",
                  "ArrowRight",
                  "Tab",
                ];
                if (!isNumber && !allowedKeys.includes(event.key)) {
                  event.preventDefault();
                }
              }}
              style={{
                width: "60px",
              }}
              upHandler={
                <CaretUpOutlined
                  onClick={() => IncreaseOrDecrease("UP", record.key)}
                />
              }
              downHandler={
                <CaretDownOutlined
                  onClick={() => IncreaseOrDecrease("DOWN", record.key)}
                />
              }
            />
          );
        }
        return value;
      },
    },
    {
      title: "Thành tiền",
      dataIndex: "amount",
      key: "amount",
    },
  ];

  const data: IData[] = dataCart.map((cart: Cart) => ({
    key: cart.store_id,
    name: cart.store.name,
    children: cart.cart_items.map((cartItem: CartItem) => ({
      key: cartItem.id,
      store_id: cart.store_id,
      product_id: cartItem.product_id,
      cart_item_id: cartItem.id,
      name: cartItem.product.name,
      quantity: cartItem.quantity,
      price: cartItem.product.price,
      amount: cartItem.product.price * cartItem.quantity,
    })),
  }));

  const rowSelection: TableRowSelection<IData> = {
    onSelect: (record, selected, selectedRows) => {
      cartOrder(selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
      cartOrder(selectedRows);
    },
  };

  const getDataCart = useCallback(() => {
    callApi<ApiResponse<Cart[]>>(REQUEST_TYPE.GET, "api/cart")
      .then((response) => {
        setDataCart(response.data.data);
        const totalLength = dataCart.reduce(
          (acc, curr) => acc + curr.cart_items.length,
          0
        );
        localStorage.setItem("CART_QUANTITY", totalLength.toString());
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi, dataCart]);

  const getDataAddresses = useCallback(() => {
    callApi<ApiResponse<Address[]>>(REQUEST_TYPE.GET, "api/address")
      .then((response) => {
        setDataAddresses(response.data.data);
        response.data.data.forEach((value) => {
          if (value.default) {
            setDefaultIdAddress(value.id);
          }
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  useEffect(() => {
    if (!token) {
      navigate("/");
    }
    getDataAddresses();
    getDataCart();
  }, [token, navigate]);

  const cartOrder = (listOrder: any) => {
    const orderList: any[] = [];
    const orders: any[] = [];
    let total = 0;
    let count = 0;
    listOrder.forEach((value: any) => {
      if ("store_id" && "product_id" in value) {
        orderList.push(value);
        if ("amount" in value) {
          total = total + value.amount;
          count++;
        }
      }
    });
    setTotal({ total, count });
    const groupedProducts = orderList.reduce((result, product) => {
      (result[product.store_id] = result[product.store_id] || []).push(product);
      return result;
    }, {});
    for (const storeId in groupedProducts) {
      const productsInStore = groupedProducts[storeId];
      const order = {
        store_id: storeId,
        address_id: defaultIdAddress,
        products: productsInStore.map((product: any) => {
          return {
            id: product.product_id,
            quantity: product.quantity,
            cart_item_id: product.cart_item_id,
          };
        }),
      };
      orders.push(order);
    }
    setProductOrders(orders);
  };

  const IncreaseOrDecrease = (key: string, id: any) => {
    switch (key) {
      case "UP":
        callApi(REQUEST_TYPE.PUT, `api/cart/increase/${id}`).finally(() =>
          window.location.reload()
        );
        break;
      case "DOWN":
        callApi(REQUEST_TYPE.PUT, `api/cart/decrease/${id}`).finally(() =>
          window.location.reload()
        );
        break;
      default:
        alert();
        break;
    }
  };

  const handleOrder = () => {
    if (total.total !== 0) {
      const confirmed = window.confirm("Bạn có chắc chắn muốn mua hàng?");
      if (confirmed) {
        callApi(REQUEST_TYPE.POST, "api/order/create", {
          orders: productOrders,
        })
          .then((res) => {
            setSuccess("Mua hàng thành công");
            localStorage.setItem("AMOUNT", total.total.toString());
            localStorage.setItem("CHECK_PAYMENT", "true");
            navigate("/cart/payment");
          })
          .catch((err) => {
            setError("Mua hàng thất bại");
            console.error(err);
          });
      } else {
        window.location.reload();
      }
    } else {
      setError("Chưa có sản phẩm nào được chọn");
      return;
    }
  };

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <div
        style={{
          width: "1240px",
          marginRight: "auto",
          marginLeft: "auto",
          padding: "0px 0px 16px",
        }}
      >
        <Typography.Title
          level={4}
          style={{
            margin: "20px 0",
          }}
        >
          GIỎ HÀNG
        </Typography.Title>
        <div
          style={{
            borderRadius: "5px",
            display: "flex",
          }}
        >
          <div
            style={{
              flex: 3,
            }}
          >
            <Table
              pagination={false}
              style={{
                backgroundColor: "white",
                minHeight: "46vh",
              }}
              columns={columns}
              rowSelection={{ ...rowSelection, checkStrictly }}
              dataSource={data}
            />
          </div>

          <div
            style={{
              marginLeft: "20px",
              flex: 1,
            }}
          >
            {dataAddresses!.length > 0 && dataAddresses ? (
              dataAddresses.map((value) =>
                value.default ? (
                  <div
                    style={{
                      background: "rgb(255, 255, 255)",
                      borderRadius: "4px",
                      paddingBottom: "8px",
                      margin: "0 0 15px 0",
                    }}
                    key={value.id}
                  >
                    <div
                      style={{
                        padding: "17px 20px",
                        flexWrap: "nowrap",
                        WebkitBoxPack: "justify",
                        margin: "0px",
                      }}
                    >
                      <Typography.Title
                        level={5}
                        style={{
                          color: "rgb(128, 128, 137)",
                          margin: "0 0 12px 0",
                        }}
                      >
                        Giao tới
                      </Typography.Title>
                      <div
                        style={{
                          display: "flex",
                        }}
                      >
                        <Typography.Text
                          style={{
                            fontWeight: "bold",
                          }}
                        >
                          {value.name}
                        </Typography.Text>
                        <div
                          style={{
                            display: "block",
                            width: "1px",
                            height: "20px",
                            backgroundColor: "rgb(235, 235, 240)",
                            margin: "0px 8px",
                          }}
                        ></div>
                        <Typography.Text
                          style={{
                            fontWeight: "bold",
                          }}
                        >
                          {value.phone}
                        </Typography.Text>
                      </div>
                      <Typography.Text
                        style={{
                          color: "rgb(128, 128, 137)",
                        }}
                      >
                        {value.detail}, {value.ward}, {value.district},{" "}
                        {value.city}
                      </Typography.Text>
                    </div>
                  </div>
                ) : (
                  <div
                    style={{
                      background: "rgb(255, 255, 255)",
                      borderRadius: "4px",
                      margin: "0 0 15px 0",
                    }}
                  >
                    <div
                      style={{
                        padding: "17px 20px",
                        flexWrap: "nowrap",
                        WebkitBoxPack: "justify",
                        margin: "0px",
                      }}
                    >
                      <Typography.Text style={{ color: "red" }}>
                        Vui lòng chỉnh lại địa chỉ mặc định{" "}
                        <Link to="/customer/address">Tại đây</Link>
                      </Typography.Text>
                    </div>
                  </div>
                )
              )
            ) : (
              <div
                style={{
                  background: "rgb(255, 255, 255)",
                  borderRadius: "4px",
                  margin: "0 0 15px 0",
                }}
              >
                <div
                  style={{
                    padding: "17px 20px",
                    flexWrap: "nowrap",
                    WebkitBoxPack: "justify",
                    margin: "0px",
                  }}
                >
                  <Typography.Text style={{ color: "red" }}>
                    Chưa có địa chỉ, vui lòng cập nhật{" "}
                    <Link to="/customer/address">Tại đây</Link>
                  </Typography.Text>
                </div>
              </div>
            )}

            <div
              style={{
                background: "rgb(255, 255, 255)",
                borderRadius: "4px",
                paddingBottom: "8px",
              }}
            >
              <div
                style={{
                  padding: "17px 20px",
                  display: "flex",
                  flexWrap: "nowrap",
                  WebkitBoxPack: "justify",
                  justifyContent: "space-between",
                  margin: "0px",
                }}
              >
                <Typography.Text>Tổng tiền</Typography.Text>
                <Typography.Title
                  level={4}
                  style={{
                    color: "red",
                    margin: 0,
                  }}
                >
                  {total.total}
                </Typography.Title>
              </div>
            </div>

            <Button
              style={{
                background: "rgb(255, 66, 78)",
                color: "rgb(255, 255, 255)",
                padding: "13px 10px",
                textAlign: "center",
                borderRadius: "4px",
                border: "none",
                width: "100%",
                display: "flex",
                cursor: "pointer",
                margin: "15px 0px 0px",
                justifyContent: "center",
                alignItems: "center",
              }}
              onClick={handleOrder}
            >
              <Typography.Text
                style={{
                  color: "white",
                }}
              >
                Mua Hàng ({total.count})
              </Typography.Text>
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContentCart;
