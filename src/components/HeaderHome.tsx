import { ShoppingCartOutlined } from "@ant-design/icons";
import {
  HomeOutlined,
  SearchOutlined,
  SmileOutlined,
} from "@ant-design/icons/lib/icons";
import { Image, Typography, Badge, Button, Dropdown, Input } from "antd";
import {
  useState,
  CSSProperties,
  useCallback,
  useEffect,
  useContext,
} from "react";
import { BsShop } from "react-icons/bs";
import { Link, useLocation, useNavigate } from "react-router-dom";
import RegisterLoginModal from "../components/RegisterLoginModal";
import { useApi } from "../hooks/useApi";
import { Cart } from "../models/types/Cart";
import { ApiResponse } from "../models/ApiResponse";
import { REQUEST_TYPE } from "../enums/RequestType";
import CartContext from "../context/CartContext";
import { User } from "../models/types/User";

const { Search } = Input;

const headerStyle = {
  textAlign: "center",
  color: "#fff",
  height: 72,
  paddingInline: 50,
  padding: "8px 0",
  display: "flex",
  backgroundColor: "white",
} as CSSProperties;

const headerContainer = {
  width: "1440px",
  paddingLeft: "24px",
  paddingRight: "24px",
  marginRight: "40px",
  marginLeft: "40px",
  display: "flex",
  boxSizing: "border-box",
} as CSSProperties;

const HeaderHome: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const token = sessionStorage.getItem("ACCESS_TOKEN");
  const cartQuantity = localStorage.getItem("CART_QUANTITY");

  const { callApi } = useApi();

  // const { dataCart, setDataCart } = useContext(CartContext);
  // const [cartQuantity, setCartQuantity] = useState(0);
  const [dataUser, setDataUser] = useState<User | undefined>();

  const onCloseModal = () => setIsVisible(false);
  const onClick = () => setIsVisible(true);
  const Results = () => <RegisterLoginModal onCloseModal={onCloseModal} />;
  const [isVisible, setIsVisible] = useState(false);
  const items = [
    {
      key: "1",
      label: (
        <Typography.Link
          href="/customer/account-info"
          style={{
            cursor: "pointer",
            display: "flex",
            padding: "4px 0",
          }}
          rel="noopener noreferrer"
        >
          Thông tin tài khoản
        </Typography.Link>
      ),
    },
    {
      key: "2",
      label: (
        <Link
          style={{
            cursor: "pointer",
            display: "flex",
            padding: "4px 0",
          }}
          rel="noopener noreferrer"
          to="/sales/order/history"
        >
          Đơn hàng của tôi
        </Link>
      ),
    },
    {
      key: "3",
      label: (
        <Typography.Link
          style={{
            cursor: "pointer",
            display: "flex",
            padding: "4px 0",
          }}
          rel="noopener noreferrer"
          onClick={() => handleLogout()}
        >
          Đăng xuất
        </Typography.Link>
      ),
    },
  ];
  const [searchItem, setSearchItem] = useState("");

  const handleChange = (event: any) => {
    setSearchItem(event.target.value);
  };

  const handleLogout = useCallback(() => {
    sessionStorage.removeItem("ACCESS_TOKEN");

    if (location.pathname !== "/" && location.pathname !== "/home") {
      navigate("/");
    } else {
      window.location.reload();
    }
  }, [location.pathname, navigate]);

  // const getDataCart = useCallback(() => {
  //   callApi<ApiResponse<Cart[]>>(REQUEST_TYPE.GET, "api/cart")
  //     .then((response) => {
  //       setDataCart(response.data.data);
  //       const totalLength = dataCart.reduce(
  //         (acc, curr) => acc + curr.cart_items.length,
  //         0
  //       );
  //       setCartQuantity(totalLength);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }, [callApi, dataCart]);

  const getDataUser = useCallback(() => {
    callApi<ApiResponse<User>>(REQUEST_TYPE.GET, "api/user/current")
      .then((response) => {
        setDataUser(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  useEffect(() => {
    if (token) {
      // getDataCart();
      getDataUser();
    }
  }, [token]);

  return (
    <>
      {isVisible ? <Results /> : null}
      <div style={headerStyle}>
        <div style={headerContainer}>
          <div
            style={{
              display: "flex",
              flex: "1 1 0%",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginRight: "48px",
              }}
            >
              <Link
                rel="noopener noreferrer"
                to="/"
                style={{
                  border: "none",
                  height: "40px",
                  padding: "0",
                  marginRight: "48px",
                }}
              >
                <Image
                  src={
                    "https://salt.tikicdn.com/ts/upload/e4/49/6c/270be9859abd5f5ec5071da65fab0a94.png"
                  }
                  preview={false}
                  style={{
                    width: "57px",
                    height: "40px",
                    cursor: "pointer",
                  }}
                />
              </Link>
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%",
              }}
            >
              <Search
                prefix={<SearchOutlined />}
                placeholder="Tìm kiếm sản phẩm"
                allowClear
                enterButton="Tìm kiếm"
                onClick={handleChange}
                size="large"
              />
              {/* {searchItem !== '' ? <ContentHome input={searchItem}></ContentHome> : ''} */}
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              marginLeft: "48px",
            }}
          >
            <Link
              rel="noopener noreferrer"
              to="/"
              type="text"
              style={{
                height: "40px",
                padding: "8px 16px",
                display: "flex",
                alignItems: "center",
                textDecoration: "none",
              }}
            >
              <Typography.Text
                style={{
                  margin: "0",
                  cursor: "pointer",
                  color: "rgb(128, 128, 137)",
                  display: "flex",
                  alignItems: "center",
                  lineHeight: "unset",
                }}
              >
                <HomeOutlined
                  style={{
                    marginRight: "4px",
                    color: "rgb(128, 128, 137)",
                    fontSize: "24px",
                  }}
                />
                Trang chủ
              </Typography.Text>
            </Link>

            {token ? (
              <Link
                rel="noopener noreferrer"
                to={
                  dataUser && dataUser.store
                    ? `/store`
                    : `/store/registration/form`
                }
                type="text"
                style={{
                  height: "40px",
                  padding: "8px 16px",
                  display: "flex",
                  alignItems: "center",
                  textDecoration: "none",
                }}
              >
                <Typography.Text
                  style={{
                    margin: "0",
                    cursor: "pointer",
                    color: "rgb(128, 128, 137)",
                    display: "flex",
                    alignItems: "center",
                    lineHeight: "unset",
                  }}
                >
                  <BsShop
                    style={{
                      marginRight: "4px",
                      color: "rgb(128, 128, 137)",
                      fontSize: "24px",
                    }}
                  />
                  Kênh Người bán
                </Typography.Text>
              </Link>
            ) : (
              ""
            )}

            {token ? (
              <Dropdown
                menu={{ items }}
                placement="bottomRight"
                arrow
                overlayStyle={{
                  width: "240px",
                }}
                trigger={["click"]}
              >
                <Button
                  type="text"
                  style={{
                    height: "40px",
                    padding: "8px 16px",
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Typography.Text
                    style={{
                      margin: "0",
                      cursor: "pointer",
                      color: "rgb(128, 128, 137)",
                      display: "flex",
                      alignItems: "center",
                      lineHeight: "unset",
                    }}
                  >
                    <SmileOutlined
                      style={{
                        marginRight: "4px",
                        color: "rgb(128, 128, 137)",
                        fontSize: "24px",
                      }}
                    />
                    Tài khoản
                  </Typography.Text>
                </Button>
              </Dropdown>
            ) : (
              <Button
                type="text"
                style={{
                  height: "40px",
                  padding: "8px 16px",
                  display: "flex",
                  alignItems: "center",
                }}
                onClick={() => onClick()}
              >
                <Typography.Text
                  style={{
                    margin: "0",
                    cursor: "pointer",
                    color: "rgb(128, 128, 137)",
                    display: "flex",
                    alignItems: "center",
                    lineHeight: "unset",
                  }}
                >
                  <SmileOutlined
                    style={{
                      marginRight: "4px",
                      color: "rgb(128, 128, 137)",
                      fontSize: "24px",
                    }}
                  />
                  Tài khoản
                </Typography.Text>
              </Button>
            )}

            <div
              style={{
                marginLeft: "24px",
              }}
            ></div>
            {token ? (
              <Link
                rel="noopener noreferrer"
                to="/cart"
                type="text"
                style={{
                  height: "40px",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <Badge
                  count={cartQuantity}
                  showZero
                  offset={[-5, 1]}
                  size="small"
                >
                  <div
                    style={{
                      content: "",
                      display: "block",
                      height: "20px",
                      position: "absolute",
                      left: "-20px",
                      border: "1px solid rgb(235, 235, 240)",
                    }}
                  ></div>
                  <ShoppingCartOutlined
                    style={{
                      color: "rgb(11, 116, 229)",
                      fontSize: "24px",
                      width: "40px",
                    }}
                  />
                </Badge>
              </Link>
            ) : (
              <div
                style={{
                  height: "40px",
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                  userSelect: "none",
                }}
                onClick={() => onClick()}
              >
                <Badge count={0} showZero offset={[-5, 1]} size="small">
                  <div
                    style={{
                      content: "",
                      display: "block",
                      height: "20px",
                      position: "absolute",
                      left: "-20px",
                      border: "1px solid rgb(235, 235, 240)",
                    }}
                  ></div>
                  <ShoppingCartOutlined
                    style={{
                      color: "rgb(11, 116, 229)",
                      fontSize: "24px",
                      width: "40px",
                    }}
                  />
                </Badge>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderHome;
