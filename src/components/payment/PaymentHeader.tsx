import { Image, Typography, Button } from "antd";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function PaymentHeader() {
  const token = sessionStorage.getItem("ACCESS_TOKEN");
  const navigate = useNavigate();
  const check = localStorage.getItem("CHECK_PAYMENT");

  useEffect(() => {
    if (!token || check == null) {
      navigate("/");
    }
  }, [token, navigate, check]);

  return (
    <div
      style={{
        background: "rgb(255, 255, 255)",
        position: "relative",
        zIndex: 1,
      }}
    >
      <div
        style={{
          display: "flex",
          WebkitBoxAlign: "center",
          alignItems: "center",
          height: "100px",
          width: "1270px",
          paddingLeft: "15px",
          paddingRight: "15px",
          marginRight: "auto",
          marginLeft: "auto",
        }}
      >
        <div
          style={{
            flex: "1 1 0%",
            display: "flex",
            alignItems: "flex-end",
          }}
        >
          <Button
            href="/"
            style={{
              border: "none",
              height: "40px",
              padding: "0",
            }}
          >
            <Image
              src={
                "https://salt.tikicdn.com/ts/upload/1c/11/e6/d8211526b5bdc67aaaef2af9e8cf7dc8.png"
              }
              preview={false}
              style={{
                width: "57px",
                height: "40px",
              }}
            />
          </Button>
          <div
            style={{
              width: "1px",
              height: "32px",
              margin: "0px 16px",
              display: "block",
              backgroundColor: "rgb(26, 148, 255)",
            }}
          ></div>
          <Typography.Title
            level={3}
            style={{
              color: "#1ba8ff",
              margin: "0",
            }}
          >
            Thanh Toán
          </Typography.Title>
        </div>
      </div>
    </div>
  );
}

export default PaymentHeader;
