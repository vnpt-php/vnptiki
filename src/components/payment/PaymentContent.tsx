import { Image, Typography } from "antd";
import { Link } from "react-router-dom";

function PaymentContent() {
  const amount = localStorage.getItem("AMOUNT");

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        paddingTop: "20px",
        paddingBottom: "68px",
        width: "1110px",
        minHeight: "calc(100vh - 260px)",
      }}
    >
      <div
        style={{
          width: "100%",
          marginRight: "20px",
        }}
      >
        <div
          style={{
            padding: "40px",
            borderRadius: "8px",
            display: "flex",
            position: "relative",
            overflow: "hidden",
            height: "370px",
          }}
        >
          <div style={{ zIndex: 1 }}>
            <Image
              style={{
                marginRight: "32px",
                zIndex: "1",
                marginTop: "35px",
              }}
              preview={false}
              src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/checkout/tiki-mascot-congrat.svg"
            ></Image>
          </div>
          <div className="backgroundPayment"></div>
          <div
            style={{
              flex: "1 1 0%",
              zIndex: 1,
              display: "flex",
              justifyContent: "space-between",
              flexDirection: "column",
            }}
          >
            <div>
              <Typography.Title
                level={3}
                style={{
                  color: "white",
                  margin: 0,
                }}
              >
                Yay, đặt hàng thành công!
              </Typography.Title>
              <Typography.Title
                level={5}
                style={{
                  color: "white",
                  margin: 0,
                }}
              >
                Chuẩn bị tiền mặt {amount} ₫
              </Typography.Title>
            </div>
            <div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Typography.Text
                  style={{
                    color: "rgb(128, 128, 137)",
                  }}
                >
                  Tổng cộng
                </Typography.Text>
                <Typography.Title
                  level={4}
                  style={{
                    margin: 0,
                  }}
                >
                  {amount} ₫
                </Typography.Title>
              </div>
              <div>
                <Link
                  rel="noopener noreferrer"
                  to="/"
                  style={{
                    height: "44px",
                    width: "80%",
                    border: "1px solid rgb(11, 116, 229)",
                    marginTop: "20px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Typography.Title
                    level={5}
                    style={{
                      margin: 0,
                      color: "rgb(11, 116, 229)",
                    }}
                  >
                    Quay về trang chủ
                  </Typography.Title>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PaymentContent;
