import { Avatar, Image, Progress, Rate, Space, Typography } from "antd";
import React, { useState } from "react";
import { UserOutlined, CheckCircleFilled } from "@ant-design/icons";
function ProductComment() {
  const images = [
    {
      id: 1,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/0c/dd/e7/4be0add110579c0ce57e53fba16e656e.png.webp",
    },
    {
      id: 2,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/89/0b/da/8b477a94538d824b14a42263ab77ec22.jpg.webp",
    },
    {
      id: 3,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/c2/dd/23/537700a59cfac1cc3659451761fdc928.jpg.webp",
    },
    {
      id: 4,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/00/5a/12/a5806ea42649b04805f8dfde90a278fa.jpg.webp",
    },
    {
      id: 5,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/39/4d/de/ee60e5a6b610648f0dc7b1cb9553f72a.jpg.webp",
    },
    {
      id: 6,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/24/df/c4/374e418535b8b01d99d2f1c471b51b90.jpg.webp",
    },
    {
      id: 7,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/e9/58/9e/e34bdec8d829982e0b3d67af3d2bcf62.jpg.webp",
    },
    {
      id: 8,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/b5/8e/fc/1e2fdc0ab315c51ca8ba43f747c6d3c0.jpg.webp",
    },
    {
      id: 9,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/aa/db/c7/d094508487a2bc403806d18f509fb09b.jpg.webp",
    },
    {
      id: 10,
      images:
        "https://salt.tikicdn.com/cache/750x750/ts/product/48/d7/80/2bb9cefa4882debf41219adb34ee93e3.jpg.webp",
    },
  ];
  const [visible, setVisible] = useState(false);
  return (
    <div
      style={{
        marginBottom: "16px",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "4px",
      }}
    >
      <Typography.Title
        level={4}
        style={{
          padding: "8px 16px",
          margin: 0,
        }}
      >
        Đánh Giá - Nhận Xét Từ Khách Hàng
      </Typography.Title>
      <div>
        <div
          style={{
            display: "flex",
            padding: "0px 48px",
          }}
        >
          <div
            style={{
              flexBasis: "335px",
              flexShrink: 0,
              margin: "0px 0px 32px",
            }}
          >
            <div>
              <div
                style={{
                  display: "flex",
                  WebkitBoxAlign: "center",
                  alignItems: "center",
                }}
              >
                <Typography.Title
                  level={2}
                  style={{
                    margin: "0px 16px 0px 0px",
                  }}
                >
                  4.9
                </Typography.Title>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={5}
                    style={{
                      marginTop: "-4px",
                    }}
                  />
                  <Typography.Text
                    style={{
                      color: "rgb(128, 128, 137)",
                      fontSize: "13px",
                    }}
                  >
                    231 nhận xét
                  </Typography.Text>
                </div>
              </div>
              <div
                style={{
                  margin: "12px 0px 0px",
                }}
              >
                <div
                  style={{
                    margin: "4px 0px",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={5}
                    style={{
                      fontSize: "10px",
                    }}
                  />
                  <Progress
                    style={{ width: "170px", margin: "0px 8px" }}
                    percent={30}
                    size="small"
                    status="active"
                    showInfo={false}
                  />
                  <Typography.Text
                    style={{
                      fontSize: "11px",
                      color: "rgb(128, 128, 137)",
                    }}
                  >
                    224
                  </Typography.Text>
                </div>
                <div
                  style={{
                    margin: "4px 0px",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={4}
                    style={{
                      fontSize: "10px",
                    }}
                  />
                  <Progress
                    style={{ width: "170px", margin: "0px 8px" }}
                    percent={30}
                    size="small"
                    status="active"
                    showInfo={false}
                  />
                  <Typography.Text
                    style={{
                      fontSize: "11px",
                      color: "rgb(128, 128, 137)",
                    }}
                  >
                    224
                  </Typography.Text>
                </div>
                <div
                  style={{
                    margin: "4px 0px",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={3}
                    style={{
                      fontSize: "10px",
                    }}
                  />
                  <Progress
                    style={{ width: "170px", margin: "0px 8px" }}
                    percent={30}
                    size="small"
                    status="active"
                    showInfo={false}
                  />
                  <Typography.Text
                    style={{
                      fontSize: "11px",
                      color: "rgb(128, 128, 137)",
                    }}
                  >
                    224
                  </Typography.Text>
                </div>
                <div
                  style={{
                    margin: "4px 0px",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={2}
                    style={{
                      fontSize: "10px",
                    }}
                  />
                  <Progress
                    style={{ width: "170px", margin: "0px 8px" }}
                    percent={30}
                    size="small"
                    status="active"
                    showInfo={false}
                  />
                  <Typography.Text
                    style={{
                      fontSize: "11px",
                      color: "rgb(128, 128, 137)",
                    }}
                  >
                    224
                  </Typography.Text>
                </div>
                <div
                  style={{
                    margin: "4px 0px",
                  }}
                >
                  <Rate
                    disabled
                    defaultValue={1}
                    style={{
                      fontSize: "10px",
                    }}
                  />
                  <Progress
                    style={{ width: "170px", margin: "0px 8px" }}
                    percent={30}
                    size="small"
                    status="active"
                    showInfo={false}
                  />
                  <Typography.Text
                    style={{
                      fontSize: "11px",
                      color: "rgb(128, 128, 137)",
                    }}
                  >
                    224
                  </Typography.Text>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div>
              <Typography.Title
                level={5}
                style={{
                  margin: "0px 0px 16px",
                }}
              >
                Tất cả hình ảnh (95)
              </Typography.Title>
              <div
                style={{
                  display: "flex",
                }}
              >
                {images.map((item, key, num) => (
                  <div
                    style={{
                      margin: "auto",
                      display: "flex",
                      overflow: "hidden",
                    }}
                  >
                    {item.id <= 5 ? (
                      <Image
                        style={{
                          width: "88px",
                          height: "88px",
                          margin: "0px 16px 0px 0px",
                        }}
                        preview={
                          item.id === 5
                            ? {
                                maskClassName: "maskImage",
                                visible: false,
                                mask: "+90",
                              }
                            : false
                        }
                        src={item.images}
                        onClick={() => item.id === 5 && setVisible(true)}
                      ></Image>
                    ) : (
                      ""
                    )}
                  </div>
                ))}

                <div
                  style={{
                    width: "88px",
                    height: "88px",
                    margin: "0px 16px 0px 0px",
                  }}
                >
                  <Image.PreviewGroup
                    preview={{
                      visible,
                      onVisibleChange: (vis) => setVisible(vis),
                    }}
                  >
                    {images.map((item, key, num) => (
                      <div style={{ display: "none" }}>
                        {item.id >= 1 ? <Image src={item.images} /> : ""}
                      </div>
                    ))}
                  </Image.PreviewGroup>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{
            padding: "32px 48px",
            display: "flex",
            borderTop: "1px solid rgb(242, 242, 242)",
          }}
        >
          <div
            style={{
              display: "flex",
              flexBasis: "335px",
              flexShrink: 0,
              height: "fit-content",
            }}
          >
            <Space wrap size={16} style={{ margin: "0px 12px 0px 0px" }}>
              <Avatar size={56} icon={<UserOutlined />} />
            </Space>
            <div
              style={{
                display: "flex",
                alignItems: "start",
                flexDirection: "column",
                justifyContent: "center",
              }}
            >
              <Typography.Title
                level={5}
                style={{
                  margin: 0,
                }}
              >
                Hằng Nguyễn
              </Typography.Title>
              <Typography.Text
                style={{
                  fontSize: "13px",
                  color: "rgb(128, 128, 137)",
                }}
              >
                Đã tham gia 2 năm
              </Typography.Text>
            </div>
          </div>
          <div>
            <div
              style={{
                display: "flex",
                WebkitBoxAlign: "center",
                alignItems: "center",
                margin: "0px 0px 4px",
              }}
            >
              <Rate
                disabled
                defaultValue={5}
                style={{
                  marginTop: "-4px",
                }}
              />
              <Typography.Title
                level={5}
                style={{
                  margin: "0px 0px 0px 12px",
                }}
              >
                Cực kì hài lòng
              </Typography.Title>
            </div>
            <div
              style={{
                margin: "0px 0px 16px",
              }}
            >
              <CheckCircleFilled
                style={{
                  color: "green",
                  margin: "0px 6px 0px 0px",
                }}
              />
              <Typography.Text>Đã mua hàng</Typography.Text>
            </div>
            <div
              style={{
                margin: "0px 0px 8px",
              }}
            >
              <Typography.Text
                style={{
                  fontSize: "13px",
                }}
              >
                Hôm qua rất may mắn mình đã đặt được màu yêu thích và add mã
                giảm mua được giá khá là rẻ so với thị trường. Hôm qua lúc mình
                mua là gần 1h sáng vì nghĩ chắc hết suất giao sớm rồi và hoàn
                thành đơn Tiki hẹn 21-10 trả máy... Nhưng điều bất ngờ xảy ra,
                hôm nay Tiki giao cho mình luôn, quá tuyệt vời, anh shipper rất
                nhiệt tình chờ vì mình không biết đơn giao. Đã 3 lần săn deal
                trên Tiki, ip11 promax , ip13 và giờ ip14 pro. Rất tin tưởng,
                TiKi mãi đỉnh...., sẽ luôn ủng hộ Tiki. Cảm ơn
              </Typography.Text>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductComment;
