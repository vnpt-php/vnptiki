import { Typography, Card, Carousel } from "antd";
import { useState } from "react";
import { Product } from "../../models/types/Product";
import { useNavigate } from "react-router-dom";
import slugify from "slugify";

interface ProductOrderProps {
  dataProduct: Product | undefined;
  dataProducts: Product[] | undefined;
}

function ProductSimilar({ dataProduct, dataProducts }: ProductOrderProps) {
  const navigate = useNavigate();

  const [selectedImage, setSelectedImage] = useState("");

  return (
    <div
      style={{
        marginBottom: "16px",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "4px",
      }}
    >
      <Typography.Title
        level={4}
        style={{
          padding: "8px 16px",
          margin: 0,
        }}
      >
        Sản Phẩm Tương Tự
      </Typography.Title>

      <Carousel
        slidesToShow={1}
        slidesToScroll={1}
        dots={false}
        autoplay
        autoplaySpeed={1000}
      >
        {dataProducts &&
          dataProducts.map((item, key) =>
            item.category_id === dataProduct?.category_id ? (
              <div
                key={key}
                onClick={() => setSelectedImage(item.product_images[0]?.id)}
              >
                <div
                  style={{
                    position: "relative",
                  }}
                >
                  <div
                    style={{
                      margin: "auto",
                      display: "flex",
                      overflow: "hidden",
                    }}
                  >
                    <Card
                      style={{ width: "183px" }}
                      hoverable
                      cover={
                        <img
                          alt="example"
                          style={{ width: "183px", height: "183px" }}
                          src={item.product_images[0]?.url}
                        />
                      }
                      onClick={() =>
                        navigate(
                          `/product/${slugify(item.name, {
                            lower: true,
                            locale: "vi",
                            remove: /[*+~.()'"!:@]/g,
                          }).replace(/\s+/g, "-")}/${item.id}`
                        )
                      }
                    >
                      <Typography
                        style={{
                          textAlign: "start",
                          fontSize: "12px",
                          height: "36px",
                          wordBreak: "break-word",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          WebkitLineClamp: "2",
                          display: "-webkit-box",
                          WebkitBoxOrient: "vertical",
                        }}
                      >
                        {item.name}
                      </Typography>
                      <div style={{ display: "flex" }}>
                        {/* <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginRight: "6px",
                          }}
                        >
                          <Typography
                            style={{
                              fontSize: "12px",
                              marginRight: "3px",
                              color: "rgb(128, 128, 137)",
                            }}
                          >
                            {item.rate}
                          </Typography>
                          <StarFilled
                            style={{
                              fontSize: "12px",
                              color: "rgb(253, 216, 54)",
                            }}
                          />
                        </div> */}

                        {/* <div
                          style={{
                            position: "relative",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              content: "",
                              display: "block",
                              height: "10px",
                              position: "absolute",
                              border: "1px solid rgb(235, 235, 240)",
                            }}
                          ></div>
                          <Typography
                            style={{
                              fontSize: "12px",
                              marginRight: "6px",
                              paddingLeft: "6px",
                              color: "rgb(128, 128, 137)",
                              lineHeight: 0,
                            }}
                          >
                            Da ban {item.sold}
                          </Typography>
                        </div> */}
                      </div>
                      <div>
                        <Typography
                          style={{
                            textAlign: "start",
                            color: "red",
                            marginTop: "6px",
                          }}
                        >
                          {item.price} ₫
                        </Typography>
                      </div>
                    </Card>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )
          )}
      </Carousel>
    </div>
  );
}

export default ProductSimilar;
