import {
  Image,
  Typography,
  Rate,
  Divider,
  InputNumber,
  Button,
  Breadcrumb,
  Alert,
} from "antd";
import { useCallback, useEffect, useState } from "react";
import { StarFilled, HomeOutlined, PlusOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Product } from "../../models/types/Product";
import RegisterLoginModal from "../RegisterLoginModal";
import { useApi } from "../../hooks/useApi";
import { ApiResponse } from "../../models/ApiResponse";
import { Cart } from "../../models/types/Cart";
import { REQUEST_TYPE } from "../../enums/RequestType";
import { StyledAlertWrapper } from "../styled/styled-components";

interface ProductOrderProps {
  dataProduct: Product | undefined;
}

function ProductOrder({ dataProduct }: ProductOrderProps) {
  const { callApi } = useApi();
  const token = sessionStorage.getItem("ACCESS_TOKEN");

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [selectedImage, setSelectedImage] = useState("");
  const [quantityProduct, setQuantityProduct] = useState(1);
  const [dataCart, setDataCart] = useState<Cart[] | undefined>([]);
  const [idCartItem, setIdCartItem] = useState("");
  const [visible, setVisible] = useState(false);
  const [isVisible, setIsVisible] = useState(false);

  const onCloseModal = () => setIsVisible(false);
  const onClick = () => setIsVisible(true);
  const Results = () => <RegisterLoginModal onCloseModal={onCloseModal} />;

  const handleQuantityProductChange = (value: any) => {
    setQuantityProduct(value);
  };

  const handleAddToCart = useCallback(() => {
    const productId = dataProduct?.id;
    if (productId === idCartItem) {
      callApi<ApiResponse<Cart>>(
        REQUEST_TYPE.PUT,
        `api/cart/increase/${dataProduct?.id}`
      )
        .then((response) => {
          setSuccess("Thêm thành công");
        })
        .catch((error) => {
          setError("Thêm thất bại");
          console.log(error);
        });
    } else {
      const data = {
        product_id: productId,
        quantity: quantityProduct,
      };

      callApi<ApiResponse<Cart>>(REQUEST_TYPE.POST, "api/cart/create", data)
        .then((response) => {
          setSuccess("Thêm vào giỏ hàng thành công");
        })
        .catch((error) => {
          setError("Thêm vào giỏ hàng thất bại");
          console.log(error);
        });
    }
  }, [callApi, dataProduct?.id, quantityProduct, idCartItem]);

  const getDataCart = useCallback(() => {
    callApi<ApiResponse<Cart[]>>(REQUEST_TYPE.GET, "api/cart")
      .then((response) => {
        setDataCart(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [callApi]);

  useEffect(() => {
    if (token) {
      getDataCart();

      if (dataCart) {
        const ids = dataCart.flatMap((cart) =>
          cart.cart_items.map((item) => item.product_id)
        );
        const cartItemsIdsString = ids.join(",");
        setIdCartItem(cartItemsIdsString);
      }
    }
  }, [token]);

  return (
    <>
      {isVisible ? <Results /> : null}

      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <Breadcrumb
        style={{
          height: "40px",
          display: "flex",
          alignItems: "center",
        }}
        items={[
          {
            title: <Link to="/">Trang chủ</Link>,
          },
          {
            title: (
              <Typography.Text>{dataProduct?.category.name}</Typography.Text>
            ),
          },
          {
            title: <Typography.Text>{dataProduct?.name}</Typography.Text>,
          },
        ]}
      />

      <div
        style={{
          display: "flex",
          marginBottom: "16px",
          backgroundColor: "rgb(255, 255, 255)",
          borderRadius: "4px",
        }}
      >
        <div
          style={{
            width: "460px",
            padding: "16px 0px 16px 16px",
            position: "relative",
          }}
        >
          <div
            style={{ width: "444px", height: "444px", margin: "0px 0px 16px" }}
          >
            {selectedImage !== "" ? (
              selectedImage && (
                <Image
                  width={444}
                  height={444}
                  preview={false}
                  src={selectedImage}
                  alt="Selected Image"
                />
              )
            ) : (
              <Image
                preview={false}
                src={dataProduct?.product_images[0]?.url}
                width="100%"
                height="100%"
                alt="Selected Image"
              />
            )}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            {dataProduct?.product_images.slice(0, 5).map((image, index) => (
              <div
                onClick={() => setSelectedImage(image.url)}
                className={image.url === selectedImage ? "selectedItems" : ""}
              >
                <div
                  style={{
                    position: "relative",
                  }}
                >
                  <div
                    style={{
                      margin: "auto",
                      display: "flex",
                      overflow: "hidden",
                    }}
                  >
                    {dataProduct.product_images.slice(0, 4) ? (
                      <Image
                        style={{
                          width: "75px",
                          height: "75px",
                          borderRadius: "4px",
                        }}
                        preview={
                          index === 4
                            ? {
                                maskClassName: "maskImageOrder",
                                visible: false,
                                mask: "+90",
                              }
                            : false
                        }
                        src={image.url}
                        onClick={() => index === 4 && setVisible(true)}
                      ></Image>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div
            style={{
              width: "75px",
              height: "75px",
            }}
          >
            <Image.PreviewGroup
              preview={{ visible, onVisibleChange: (vis) => setVisible(vis) }}
            >
              {dataProduct?.product_images.map((image) => (
                <div style={{ display: "none" }}>
                  {dataProduct.product_images.length >= 1 ? (
                    <Image src={image.url} />
                  ) : (
                    ""
                  )}
                </div>
              ))}
            </Image.PreviewGroup>
          </div>
        </div>
        <div
          style={{
            margin: "0px 12px",
            width: "1px",
            background: "rgb(242, 242, 242)",
            flexShrink: "0",
          }}
        ></div>
        <div
          style={{
            flex: "1 1 0%",
          }}
        >
          <div
            style={{
              padding: "16px 28px 16px 0px",
              position: "relative",
            }}
          >
            <Typography
              style={{
                margin: "0px",
                fontSize: "13px",
                fontWeight: "400",
                lineHeight: "20px",
                color: "rgb(36, 36, 36)",
                display: "flex",
                marginTop: "8px",
              }}
            >
              Thương hiệu:
              <Link
                to={`/products/brand/${dataProduct?.brand_id}`}
                style={{
                  fontSize: "13px",
                  lineHeight: "20px",
                  marginLeft: 4,
                }}
              >
                {dataProduct?.brand.name}
              </Link>
            </Typography>
            <Typography.Title
              level={3}
              style={{
                margin: "0px 0px 4px",
              }}
            >
              {dataProduct?.name}
            </Typography.Title>
            {/* <div
            style={{
              display: "flex",
              WebkitBoxAlign: "center",
              alignItems: "center",
              marginTop: "4px",
              WebkitBoxPack: "justify",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                display: "flex",
              }}
            >
              <div
                style={{
                  display: "flex",
                  WebkitBoxAlign: "center",
                  alignItems: "center",
                }}
              >
                <Rate
                  disabled
                  defaultValue={5}
                  style={{
                    fontSize: "16px",
                    marginTop: "-4px",
                  }}
                />
                <Typography
                  style={{
                    display: "block",
                    cursor: "pointer",
                    marginLeft: "8px",
                    fontSize: "15px",
                    lineHeight: "24px",
                    color: "rgb(120, 120, 120)",
                  }}
                >
                  (Xem 50 đánh giá)
                </Typography>
                <div
                  style={{
                    width: "1px",
                    height: "12px",
                    marginLeft: "8px",
                    marginRight: "8px",
                    backgroundColor: "rgb(199, 199, 199)",
                  }}
                ></div>
              </div>
              <div>
                <Typography
                  style={{
                    fontSize: "15px",
                    lineHeight: "24px",
                    color: "rgb(120, 120, 120)",
                  }}
                >
                  Đã bán 203
                </Typography>
              </div>
            </div>
          </div> */}
          </div>
          <div
            style={{
              display: "flex",
              padding: "0px 24px 0px 0px",
            }}
          >
            <div
              style={{
                flex: "1 1 0%",
                paddingRight: "12px",
              }}
            >
              <Typography.Title
                level={2}
                style={{
                  lineHeight: "40px",
                  margin: 0,
                  color: "red",
                  borderRadius: "4px",
                  backgroundColor: "rgb(250, 250, 250)",
                  padding: 12,
                }}
              >
                {dataProduct?.price} ₫
              </Typography.Title>
              <Divider></Divider>
              <Typography
                style={{
                  cursor: "pointer",
                  padding: "12px 0px",
                  fontSize: "14px",
                }}
              >
                Giao đến{" "}
                <Typography.Text
                  style={{
                    textDecoration: "underline",
                    fontSize: "15px",
                    fontWeight: "bold",
                  }}
                >
                  {dataProduct?.store.address}
                </Typography.Text>
              </Typography>
              <Divider></Divider>
              <div
                style={{
                  padding: "16px 0px",
                }}
              >
                <div>
                  <Typography
                    style={{
                      fontSize: "15px",
                      lineHeight: "1.6",
                      margin: "0px 0px 10px",
                    }}
                  >
                    Số Lượng
                  </Typography>
                  <InputNumber
                    min={1}
                    max={99}
                    defaultValue={1}
                    onKeyDown={(event) => {
                      const isNumber = /[0-9]/.test(event.key);
                      const allowedKeys = [
                        "Backspace",
                        "Delete",
                        "ArrowLeft",
                        "ArrowRight",
                        "Tab",
                      ];
                      if (!isNumber && !allowedKeys.includes(event.key)) {
                        event.preventDefault();
                      }
                    }}
                    onChange={(value) => handleQuantityProductChange(value)}
                    style={{
                      width: "60px",
                    }}
                  />
                </div>
                <Button
                  style={{
                    marginTop: "16px",
                    backgroundColor: "rgb(255, 57, 69)",
                    cursor: "pointer",
                    width: "100%",
                    maxWidth: "300px",
                    height: "48px",
                    textTransform: "capitalize",
                    border: "none",
                    borderRadius: "4px",
                    alignItems: "center",
                    display: "flex",
                    justifyContent: "center",
                  }}
                  onClick={() => {
                    if (!token) {
                      onClick();
                    } else {
                      handleAddToCart();
                    }
                  }}
                >
                  <Typography
                    style={{
                      color: "rgb(255, 255, 255)",
                    }}
                  >
                    Chọn mua
                  </Typography>
                </Button>
              </div>
            </div>
            <div
              style={{
                paddingBottom: "20px",
                width: "240px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  minHeight: "60px",
                  padding: "8px 12px",
                }}
              >
                <Typography.Link
                  style={{
                    display: "flex",
                  }}
                >
                  <Image
                    preview={false}
                    src="https://vcdn.tikicdn.com/cache/w100/ts/seller/ee/fa/a0/98f3f134f85cff2c6972c31777629aa0.png.webp"
                    style={{
                      width: "44px",
                      borderRadius: "50%",
                      overflow: "hidden",
                    }}
                  ></Image>
                  <div
                    style={{
                      margin: "0px 0px 0px 12px",
                    }}
                  >
                    <Typography
                      style={{
                        margin: "0px 0px 2px",
                        fontSize: "15px",
                        fontStretch: "normal",
                        fontStyle: "normal",
                        lineHeight: "1.6",
                        letterSpacing: "normal",
                        color: "rgb(36, 36, 36)",
                        fontWeight: "700",
                      }}
                    >
                      {dataProduct?.store.name}
                    </Typography>
                    <Image
                      preview={false}
                      src={
                        "https://salt.tikicdn.com/cache/w100/ts/upload/5d/4c/f7/0261315e75127c2ff73efd7a1f1ffdf2.png.webp"
                      }
                      style={{
                        width: "68px",
                        height: "14px",
                        display: "flex",
                        alignItems: "center",
                      }}
                    ></Image>
                  </div>
                </Typography.Link>
              </div>
              <div
                style={{
                  display: "flex",
                  minHeight: "39px",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    WebkitBoxAlign: "center",
                    alignItems: "center",
                    flex: "1 1 0%",
                  }}
                >
                  <div>
                    <div>
                      <Typography.Text
                        style={{
                          fontWeight: "700",
                        }}
                      >
                        4.7 / 5
                      </Typography.Text>
                      <StarFilled
                        style={{
                          color: "rgb(255, 193, 32)",
                          marginLeft: "3px",
                          marginTop: "1px",
                        }}
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      fontSize: "11px",
                      fontWeight: "normal",
                      fontStretch: "normal",
                      fontStyle: "normal",
                      letterSpacing: "normal",
                      lineHeight: "1.45",
                      color: "rgb(120, 120, 120)",
                    }}
                  >
                    5.3tr+
                  </div>
                </div>
                <div
                  style={{
                    alignSelf: "center",
                    width: "1px",
                    height: "16px",
                    backgroundColor: "rgb(242, 242, 242)",
                  }}
                ></div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    WebkitBoxAlign: "center",
                    alignItems: "center",
                    flex: "1 1 0%",
                  }}
                >
                  <div>
                    <div>
                      <Typography.Text
                        style={{
                          fontWeight: "700",
                        }}
                      >
                        4.7 / 5+
                      </Typography.Text>
                    </div>
                  </div>
                  <div
                    style={{
                      fontSize: "11px",
                      fontWeight: "normal",
                      fontStretch: "normal",
                      fontStyle: "normal",
                      letterSpacing: "normal",
                      lineHeight: "1.45",
                      color: "rgb(120, 120, 120)",
                    }}
                  >
                    Theo dõi
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  padding: "12px",
                  WebkitBoxPack: "justify",
                  justifyContent: "space-between",
                }}
              >
                <Link rel="noopener noreferrer" to="/shop">
                  <Button
                    style={{
                      display: "flex",
                      WebkitBoxAlign: "center",
                      alignItems: "center",
                      borderRadius: "4px",
                      cursor: "pointer",
                      border: "1px solid rgb(13, 92, 182)",
                      padding: "6px 9px",
                    }}
                  >
                    <HomeOutlined
                      style={{
                        color: "rgb(13, 92, 182)",
                        width: "20px",
                        height: "20px",
                        display: "flex",
                        alignItems: "center",
                      }}
                    />
                    <Typography
                      style={{
                        fontSize: "13px",
                        fontWeight: "500",
                        fontStretch: "normal",
                        fontStyle: "normal",
                        letterSpacing: "normal",
                        marginLeft: "4px",
                        lineHeight: "1.54",
                        color: "rgb(13, 92, 182)",
                      }}
                    >
                      Xem Shop
                    </Typography>
                  </Button>
                </Link>
                <Button
                  style={{
                    display: "flex",
                    WebkitBoxAlign: "center",
                    alignItems: "center",
                    borderRadius: "4px",
                    cursor: "pointer",
                    border: "1px solid rgb(13, 92, 182)",
                    padding: "6px 9px",
                  }}
                >
                  <PlusOutlined
                    style={{
                      color: "rgb(13, 92, 182)",
                      width: "20px",
                      height: "20px",
                      display: "flex",
                      alignItems: "center",
                    }}
                  />
                  <Typography
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      fontStretch: "normal",
                      fontStyle: "normal",
                      letterSpacing: "normal",
                      marginLeft: "4px",
                      lineHeight: "1.54",
                      color: "rgb(13, 92, 182)",
                    }}
                  >
                    Theo Dõi
                  </Typography>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProductOrder;
