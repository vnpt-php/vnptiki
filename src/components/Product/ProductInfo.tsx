import { Typography, Descriptions } from "antd";
import { Product } from "../../models/types/Product";

interface ProductOrderProps {
  dataProduct: Product | undefined;
}

function ProductInfo({ dataProduct }: ProductOrderProps) {
  return (
    <div
      style={{
        marginBottom: "16px",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "4px",
      }}
    >
      <Typography.Title
        level={4}
        style={{
          padding: "8px 16px",
          margin: 0,
        }}
      >
        Thông Tin Chi Tiết
      </Typography.Title>
      <Descriptions
        style={{
          padding: "0px 16px 16px",
        }}
        bordered
        column={{ xl: 1 }}
      >
        <Descriptions.Item label="Tên sản phẩm">
          {dataProduct?.name}
        </Descriptions.Item>
        <Descriptions.Item label="Loại sản phẩm">
          {dataProduct?.category.name}
        </Descriptions.Item>
        <Descriptions.Item label="Thương hiệu">
          {dataProduct?.brand.name}
        </Descriptions.Item>
        <Descriptions.Item label="Giá">
          {dataProduct?.price} ₫
        </Descriptions.Item>
        {/* <Descriptions.Item label="Đã bán">203</Descriptions.Item>
                <Descriptions.Item label="Số lượt đánh giá">1248</Descriptions.Item>
                <Descriptions.Item label="Điểm đánh giá">4.7/5</Descriptions.Item> */}
      </Descriptions>
    </div>
  );
}

export default ProductInfo;
