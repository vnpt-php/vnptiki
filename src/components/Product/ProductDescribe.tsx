import { Typography } from "antd";
import { Product } from "../../models/types/Product";

interface ProductOrderProps {
  dataProduct: Product | undefined;
}

function ProductDescribe({ dataProduct }: ProductOrderProps) {
  return (
    <div
      style={{
        marginBottom: "16px",
        backgroundColor: "rgb(255, 255, 255)",
        borderRadius: "4px",
      }}
    >
      <Typography.Title
        level={4}
        style={{
          padding: "8px 16px",
          margin: 0,
        }}
      >
        Mô Tả Sản Phẩm
      </Typography.Title>
      <div
        style={{
          padding: "0px 16px 16px",
        }}
      >
        <Typography.Text>{dataProduct?.description}</Typography.Text>
      </div>
    </div>
  );
}

export default ProductDescribe;
