import { Image, Typography } from "antd";

function FooterHome() {
  return (
    <div
      style={{
        textAlign: "center",
        color: "#fff",
        backgroundColor: "white",
        padding: "0 16px",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          gap: "20px",
          padding: "16px 0",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Hỗ trợ khách hàng
          </Typography.Title>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Hotline: 1900-6035 (1000 đ/phút, 8-21h kể cả T7, CN)
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Các câu hỏi thường gặp
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Gửi yêu cầu hỗ trợ
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Hướng dẫn đặt hàng
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Phương thức vận chuyển
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Chính sách đổi trả
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Hướng dẫn trả góp
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Chính sách hàng nhập khẩu
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Hỗ trợ khách hàng: hotro@tiki.vn
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Báo lỗi bảo mật: security@tiki.vn
          </Typography.Text>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Về Tiki
          </Typography.Title>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Giới thiệu Tiki
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Tiki Blog
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Tuyển dụng
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Chính sách bảo mật thanh toán
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Chính sách bảo mật thông tin cá nhân
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Chính sách giải quyết khiếu nại
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Điều khoản sử dụng
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Giới thiệu Tiki Xu
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Gửi Astra nhận Xu mua sắm thả ga
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Tiếp thị liên kết cùng Tiki
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Bán hàng doanh nghiệp
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Điều kiện vận chuyển
          </Typography.Text>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Hợp tác và liên kết
          </Typography.Title>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Quy chế hoạt động Sàn GDTMĐT
          </Typography.Text>
          <Typography.Text
            style={{
              margin: "0",
              textAlign: "left",
              color: "rgb(128, 128, 137)",
              fontSize: "12px",
              fontWeight: "500",
              marginBottom: "4px",
            }}
          >
            Bán hàng cùng Tiki
          </Typography.Text>
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Chứng nhận bởi
          </Typography.Title>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              gap: "8px",
            }}
          >
            <Image
              preview={false}
              width={32}
              src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/bo-cong-thuong-2.png"
            ></Image>
            <Image
              preview={false}
              width={83}
              src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/bo-cong-thuong.svg"
            ></Image>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Phương thức thanh toán
          </Typography.Title>
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "100px 100px 100px",
            }}
          >
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Amazon/Amazon-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Bitcoin/Bitcoin-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Ebay/Ebay-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/PayPal/Paypal-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Stripe/Stripe-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Eway/Eway-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Coinkite/Coinkite-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Dwolla/Dwolla-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/GiroPay/GiroPay-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Klarna/Klarna-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Laser/Laser-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Monero/Monero-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/Shopify/Shopify-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/WebMoney/WebMoney-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/WorldPay/WorldPay-card-light.png?raw=true"
            ></Image>
            <Image
              preview={false}
              width={70}
              src="https://github.com/gregoiresgt/payment-icons/blob/master/Assets/Payment/WesternUnion/WesternUnion-card-light.png?raw=true"
            ></Image>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
              width: "150px",
            }}
          >
            Kết nối với chúng tôi
          </Typography.Title>
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
            }}
          >
            <Image
              preview={false}
              width={32}
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Icon_of_Zalo.svg/2048px-Icon_of_Zalo.svg.png"
            ></Image>
            <Image
              preview={false}
              width={32}
              src="https://png.pngtree.com/png-vector/20221018/ourmid/pngtree-youtube-social-media-round-icon-png-image_6315993.png"
            ></Image>
            <Image
              preview={false}
              width={32}
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfvwcuzkFGK5Iz9dkme2TwNWYHxhyM8ERP0QoblXy76w&s"
            ></Image>
          </div>
          <Typography.Title
            level={5}
            style={{
              margin: "0",
              textAlign: "left",
              marginBottom: "12px",
            }}
          >
            Tải ứng dụng trên điện thoại
          </Typography.Title>
          <div
            style={{
              display: "flex",
              gap: "8px",
            }}
          >
            <Image
              width={80}
              preview={false}
              src="https://frontend.tikicdn.com/_desktop-next/static/img/footer/qrcode.png"
            ></Image>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                flexDirection: "column",
              }}
            >
              <Image
                width={122}
                preview={false}
                src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/appstore.png"
              ></Image>
              <Image
                width={122}
                preview={false}
                src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/playstore.png"
              ></Image>
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          padding: "16px 0px",
          flexDirection: "column",
          display: "flex",
        }}
      >
        <Typography.Title
          level={5}
          style={{
            margin: "0",
            textAlign: "left",
            marginBottom: "12px",
          }}
        >
          Tiki - Thật nhanh, thật chất lượng, thật rẻ
        </Typography.Title>
        <Typography.Text
          style={{
            margin: "0",
            textAlign: "left",
            color: "rgb(128, 128, 137)",
            fontSize: "12px",
            fontWeight: "500",
            marginBottom: "4px",
          }}
        >
          Tiki có tất cả
        </Typography.Text>
        <Typography.Text
          style={{
            margin: "0",
            textAlign: "left",
            color: "rgb(128, 128, 137)",
            fontSize: "12px",
            marginBottom: "12px",
          }}
        >
          Với hàng triệu sản phẩm từ các thương hiệu, cửa hàng uy tín, hàng
          nghìn loại mặt hàng từ Điện thoại smartphone tới Rau củ quả tươi, kèm
          theo dịch vụ giao hàng siêu tốc TikiNOW, Tiki mang đến cho bạn một
          trải nghiệm mua sắm online bắt đầu bằng chữ tín. Thêm vào đó, ở Tiki
          bạn có thể dễ dàng sử dụng vô vàn các tiện ích khác như mua thẻ cào,
          thanh toán hoá đơn điện nước, các dịch vụ bảo hiểm.
        </Typography.Text>
        <Typography.Text
          style={{
            margin: "0",
            textAlign: "left",
            color: "rgb(128, 128, 137)",
            fontSize: "12px",
            fontWeight: "500",
            marginBottom: "4px",
          }}
        >
          Khuyến mãi, ưu đãi tràn ngập
        </Typography.Text>
        <Typography.Text
          style={{
            margin: "0",
            textAlign: "left",
            color: "rgb(128, 128, 137)",
            fontSize: "12px",
          }}
        >
          Bạn muốn săn giá sốc, Tiki có giá sốc mỗi ngày cho bạn! Bạn là tín đồ
          của các thương hiệu, các cửa hàng Official chính hãng đang chờ đón
          bạn. Không cần săn mã freeship, vì Tiki đã có hàng triệu sản phẩm
          trong chương trình Freeship+, không giới hạn lượt đặt, tiết kiệm thời
          gian vàng bạc của bạn. Mua thêm gói TikiNOW tiết kiệm để nhận 100%
          free ship 2h & trong ngày, hoặc mua gói TikiNOW cao cấp để nhận được
          100% freeship, áp dụng cho 100% sản phẩm, 100% tỉnh thành Việt Nam.
          Bạn muốn tiết kiệm hơn nữa? Đã có TikiCARD, thẻ tín dụng Tiki hoàn
          tiền 15% trên mọi giao dịch (tối đa hoàn 600k/tháng)
        </Typography.Text>
      </div>
    </div>
  );
}

export default FooterHome;
