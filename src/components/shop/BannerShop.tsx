import { Carousel, Image } from "antd";
function BannerShop() {
  return (
    <div
      style={{
        marginBottom: "16px",
      }}
    >
      <div>
        <div style={{}}>
          <Carousel
            style={{
              display: "flex",
              justifyItems: "center",
            }}
            slidesToShow={2}
            slidesToScroll={1}
            dots={false}
            autoplay
          >
            <Image
              preview={false}
              src="https://domainente.com/wp-content/uploads/2020/03/2fe0fb8654edf0794bc4ad67089030f4.png"
              height={270}
              width={480}
            />
            <Image
              preview={false}
              src="https://cdn.discordapp.com/attachments/1029747761094070354/1090512606579662949/tiki_1.png"
              width={480}
              height={270}
            />
            <Image
              preview={false}
              src="https://s3-ap-southeast-1.amazonaws.com/storage.adpia.vn/affiliate_document/img/tiki-700x400-11-11-hang-hieu-cuc-chat.jpg"
              width={480}
              height={270}
            />
            <Image
              preview={false}
              src="https://salt.tikicdn.com/ts/brickv2og/3b/82/a1/9493e7fe91e657b023f357a0db2aab05.png"
              width={480}
              height={270}
            />
            <Image
              preview={false}
              src="https://upbase.vn/wp-content/uploads/2021/10/luu-y-mua-sieu-sale.png"
              width={480}
              height={270}
            />
            <Image
              preview={false}
              src="https://salt.tikicdn.com/ts/brickv2og/cc/f8/5c/683800af71e0e25da555145f7eff3cf9.png"
              width={480}
              height={270}
            />
          </Carousel>
        </div>
      </div>
    </div>
  );
}

export default BannerShop;
