import React from "react";
import { Card, Image, Typography } from "antd";
import { Link } from "react-router-dom";
import { StarFilled } from "@ant-design/icons";
function ContentShop(prop: any) {
    const listProducts = [
        {
            id: "1",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 1",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "2",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 12",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "3",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 13",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "4",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 14",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "5",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 15",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "6",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 16",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "7",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 17",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "8",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 18",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "9",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 19",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
        {
            id: "10",
            image:
                "https://salt.tikicdn.com/cache/280x280/ts/product/f5/52/80/675e31a670afc560e7b0e46c0b65fb4f.png.webp",
            name: "Apple iPhone 20",
            rate: "4.95",
            sold: "500+",
            price: "27.750.000",
        },
    ];
    // const fillData = listProducts.filter((el) => {
    //   //if no input the return the original
    //   if (prop.input === '') {
    //     return el;
    //   }
    //   //return the item which contains the user input
    //   else {
    //     return el.name.toLowerCase().includes(prop.input)
    //   }
    // })
    return (
        <div
            style={{
                textAlign: "center",
                minHeight: "50vh",
                lineHeight: "120px",
                color: "#fff",
                display: "flex",
                flexWrap: "wrap",
                alignSelf: "stretch",
                gap: "8px",
                borderTop: "1px solid rgb(242, 242, 242)",
                padding: "0px 0px 16px",
                width: '100%',
            }}
        >
            {/* {fillData.map((item, key) => (
        <> */}
            {listProducts.map((item, key) => (
                <>
                    <Link style={{
                        width: "16%"
                    }} to="/product">
                        <Card
                            hoverable
                            cover={
                                <Image
                                    preview={false}
                                    alt="example"
                                    style={{ width: "183px", height: "183px" }}
                                    src={item.image}
                                />
                            }
                        >
                            <Typography
                                style={{
                                    textAlign: "start",
                                    fontSize: "12px",
                                    height: "36px",
                                    wordBreak: "break-word",
                                    overflow: "hidden",
                                    textOverflow: "ellipsis",
                                    WebkitLineClamp: "2",
                                    display: "-webkit-box",
                                    WebkitBoxOrient: "vertical",
                                }}
                            >
                                {item.name}
                            </Typography>
                            <div style={{ display: "flex" }}>
                                <div
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        marginRight: "6px",
                                    }}
                                >
                                    <Typography
                                        style={{
                                            fontSize: "12px",
                                            marginRight: "3px",
                                            color: "rgb(128, 128, 137)",
                                        }}
                                    >
                                        {item.rate}
                                    </Typography>
                                    <StarFilled
                                        style={{ fontSize: "12px", color: "rgb(253, 216, 54)" }}
                                    />
                                </div>
                                <div
                                    style={{
                                        position: "relative",
                                        display: "flex",
                                        alignItems: "center",
                                    }}
                                >
                                    <div
                                        style={{
                                            content: "",
                                            display: "block",
                                            height: "10px",
                                            position: "absolute",
                                            border: "1px solid rgb(235, 235, 240)",
                                        }}
                                    ></div>
                                    <Typography
                                        style={{
                                            fontSize: "12px",
                                            marginRight: "6px",
                                            paddingLeft: "6px",
                                            color: "rgb(128, 128, 137)",
                                            lineHeight: 0,
                                        }}
                                    >
                                        Da ban {item.sold}
                                    </Typography>
                                </div>
                            </div>
                            <div>
                                <Typography
                                    style={{ textAlign: "start", color: "red", marginTop: "6px" }}
                                >
                                    {item.price} ₫
                                </Typography>
                            </div>
                        </Card>
                    </Link>
                </>
            ))}
        </div>
    );
}

export default ContentShop;
