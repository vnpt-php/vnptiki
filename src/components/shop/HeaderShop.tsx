import { Button, Drawer, Image, Space, Typography } from "antd";
import React from "react";
import {
  StarFilled,
  HeartFilled,
  UserOutlined,
  PlusOutlined,
} from "@ant-design/icons";

function HeaderShop() {
  return (
    <div
      style={{
        marginBottom: "16px",
      }}
    >
      <div>
        <div
          style={{
            width: "100%",
            padding: "24px 0 24px 24px",
            backgroundSize: "cover",
            display: "flex",
            alignItems: "center",
            backgroundImage:
              "url(https://salt.tikicdn.com/ts/sellercenterFE/2d/24/dc/8b6b6c4ef997c6ad31f5ab7bed44e850.png)",
            // backgroundImage:
            //     'url("https://salt.tikicdn.com/ts/sellercenterFE/2d/24/dc/8b6b6c4ef997c6ad31f5ab7bed44e850.png")'
          }}
        >
          <Image
            src="https://salt.tikicdn.com/cache/w220/ts/seller/ee/fa/a0/98f3f134f85cff2c6972c31777629aa0.png"
            style={{
              width: "64px",
              borderRadius: "999px",
            }}
          ></Image>
          <div style={{ paddingLeft: "16px" }}>
            <Typography.Title
              level={5}
              style={{
                color: "white",
                margin: 0,
              }}
            >
              Tiki Trading
            </Typography.Title>
            <Image
              preview={false}
              src={
                "https://salt.tikicdn.com/cache/w100/ts/upload/5d/4c/f7/0261315e75127c2ff73efd7a1f1ffdf2.png.webp"
              }
              style={{
                width: "68px",
                height: "14px",
                display: "flex",
                alignItems: "center",
              }}
            ></Image>
            <div style={{ display: "flex" }}>
              <StarFilled
                style={{
                  color: "rgb(255, 193, 32)",
                  marginLeft: "3px",
                  marginRight: "2px",
                  marginTop: "4px",
                  fontSize: "12px",
                }}
              />
              <Typography.Text
                style={{
                  color: "rgba(255, 255, 255, 0.7)",
                }}
              >
                4.7 / 5
              </Typography.Text>
              <div
                style={{
                  width: "1px",
                  height: "8px",
                  marginTop: "7px",
                  marginRight: "8px",
                  marginLeft: "8px",
                  backgroundColor: "rgba(255, 255, 255, 0.7)",
                }}
              ></div>
              <div
                style={{
                  display: "flex",
                }}
              >
                <div
                  style={{
                    position: "relative",
                    marginRight: "2px",
                    marginTop: "1px",
                  }}
                >
                  <HeartFilled
                    style={{
                      color: "white",
                      position: "absolute",
                      fontSize: "5px",
                      right: "100%",
                      zIndex: 2,
                      left: "10px",
                      top: "11.2px",
                    }}
                  />
                  <UserOutlined
                    style={{
                      color: "rgba(255, 255, 255, 0.7)",
                      zIndex: 1,
                    }}
                  />
                </div>
                <div>
                  <Typography
                    style={{
                      color: "rgba(255, 255, 255, 0.7)",
                    }}
                  >
                    Người theo dõi: 468.6k+
                  </Typography>
                </div>
              </div>
            </div>
          </div>
          <div
            style={{
              borderLeft: "1px solid rgba(255, 255, 255, 0.2)",
              height: "32px",
              paddingLeft: "24px",
              marginLeft: "24px",
              display: "flex",
              WebkitBoxAlign: "center",
              alignItems: "center",
            }}
          >
            <Button
              icon={<PlusOutlined />}
              style={{
                backgroundColor: "rgb(11, 116, 229)",
                color: "white",
                borderColor: "unset",
              }}
            >
              Theo Dõi
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderShop;
