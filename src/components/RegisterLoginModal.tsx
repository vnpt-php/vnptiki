import { Alert, Button, Form, Input, Typography } from "antd";
import { useState, CSSProperties } from "react";
import styled from "styled-components";
import { baseUrl } from "../constants/url";
import axios from "axios";
import { ApiResponse } from "../models/ApiResponse";
import { AccessToken } from "../models/AccessToken";
import { StyledAlertWrapper } from "./styled/styled-components";

const StyledModal = styled.div`
  position: fixed;
  inset: 0;
  background: rgba(0, 0, 0, 0.53);
  overflow-y: scroll;
  z-index: 999;
`;

const StyledModalContainer = styled.div`
  position: relative;
  inset: 0;
  background: rgb(255, 255, 255);
  overflow: unset;
  border-radius: 20px;
  outline: none;
  padding: 0;
  width: 800px;
  margin: 80px auto;
`;

const StyledModalContent = styled.div`
  background: rgb(248, 248, 248);
  display: flex;
  width: 100%;
  border-radius: 20px;
  position: relative;
`;

const StyledLoginContainer = styled.div`
  width: 500px;
  padding: 40px 45px 24px;
  background: rgb(255, 255, 255);
  border-radius: 20px 0 0 20px;
`;

const StyledPhoto = styled.div`
  background: linear-gradient(
    136deg,
    rgb(240, 248, 255) -1%,
    rgb(219, 238, 255) 85%
  );
  width: 300px;
  -webkit-box-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border-radius: 0 20px 20px 0;
`;

const StyledPhotoContent = styled.div`
  margin: 30px 0 0;
  text-align: center;
`;

const styles = {
  btnClose: {
    position: "absolute",
    right: "-8px",
    top: "-8px",
    width: "42px",
    height: "42px",
    background: "none",
    border: "none",
    cursor: "pointer",
  } as CSSProperties,

  photoText: {
    color: "rgb(11, 116, 229)",
    fontWeight: 500,
  },

  inputField: {
    width: "100%",
    padding: "10px 15px",
    fontSize: "14px",
    color: "rgb(36, 36, 36)",
  },

  inputFieldRegister: {
    width: "100%",
    padding: "10px 15px",
    fontSize: "24px",
    color: "rgb(36, 36, 36)",
  },

  btnSubmit: {
    outline: "none",
    borderRadius: "4px",
    padding: "13px 0",
    width: "100%",
    height: "100%",
    color: "rgb(255, 255, 255)",
    border: "none",
    frontSize: "20px",
    fontWeight: 600,
  },

  textFooter: {
    color: "rgb(13, 92, 182)",
    cursor: "pointer",
    display: "inline-block",
  },

  btnAction: {
    padding: 0,
    margin: "0 0 12px",
    background: "transparent",
    border: "none",
  },

  textNote: {
    fontSize: "11px",
    color: "rgb(120, 120, 120)",
    textAlign: "center",
  } as CSSProperties,
};

interface Props {
  onCloseModal: () => void;
}

function RegisterLoginModal({ onCloseModal }: Props) {
  const [isOpenRegister, setIsOpenRegister] = useState(true);
  const [isCloseEmailRegister, setIsCloseEmailRegister] = useState(true);
  const [isOpenPassRegister, setIsOpenPassRegister] = useState(true);
  const [isOpenLogin, setIsOpenLogin] = useState(true);

  const [emailLogin, setEmailLogin] = useState("");
  const [passwordLogin, setPasswordLogin] = useState("");
  const [nameRegister, setNameRegister] = useState("");
  const [phoneRegister, setPhoneRegister] = useState("");
  const [emailRegister, setEmailRegister] = useState("");
  const [passwordRegister, setPasswordRegister] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const handleEmailChange = (value: any) => {
    setEmailLogin(value);
  };

  const handlePasswordChange = (value: any) => {
    setPasswordLogin(value);
  };

  const handleNameRegisterChange = (value: any) => {
    setNameRegister(value);
  };

  const handlePhoneRegisterChange = (value: any) => {
    setPhoneRegister(value);
  };

  const handleEmailRegisterChange = (value: any) => {
    setEmailRegister(value);
  };

  const handlePasswordRegisterChange = (value: any) => {
    setPasswordRegister(value);
  };

  const hasNumber = /\d/;
  const hasUpperCase = /[A-Z]/;
  const hasLowerCase = /[a-z]/;
  const hasSpecialCharacter = /[!@#$%^&*(),.?":{}|<>]/;

  const PasswordValidator = (_: any, value: any) => {
    if (!value) {
      return Promise.reject("");
    }
    if (value.length < 8) {
      return Promise.reject("Mật khẩu phải có ít nhất 8 ký tự");
    }
    if (!hasNumber.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 số");
    }
    if (!hasUpperCase.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 chữ cái viết hoa");
    }
    if (!hasLowerCase.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 chữ cái viết thường");
    }
    if (!hasSpecialCharacter.test(value)) {
      return Promise.reject("Mật khẩu phải chứa ít nhất 1 ký tự đặc biệt");
    }
    return Promise.resolve();
  };

  const handleLogin = async () => {
    const isPasswordValid = await PasswordValidator(null, passwordLogin)
      .then(() => true)
      .catch(() => false);
    if (!isPasswordValid) {
      setError("Mật khẩu không hợp lệ");
      return;
    }

    const data = {
      email: emailLogin,
      password: passwordLogin,
    };

    const url = `${baseUrl}/api/user/login`;

    axios
      .post<ApiResponse<AccessToken>>(url, data)
      .then((result) => {
        sessionStorage.setItem("ACCESS_TOKEN", result.data.data.token);
        setSuccess("Đăng nhập thành công");

        window.location.reload();
      })
      .catch(() => {
        setError("Đăng nhập thất bại");
      });
  };

  const handleRegister = async () => {
    const data = {
      name: nameRegister,
      email: emailRegister,
      phone: phoneRegister,
      password: passwordRegister,
    };

    const url = `${baseUrl}/api/user/create`;

    axios
      .post(url, data)
      .then(() => {
        setSuccess("Đăng ký thành công");
        window.location.reload();
      })
      .catch(() => {
        setError("Đăng ký thất bại");
      });
  };

  return (
    <>
      <StyledAlertWrapper>
        {error && (
          <Alert
            message={error}
            type="error"
            showIcon
            onClose={() => setError("")}
            closable
          />
        )}
        {success && (
          <Alert
            message={success}
            type="success"
            showIcon
            onClose={() => setSuccess("")}
            closable
          />
        )}
      </StyledAlertWrapper>

      <StyledModal>
        <StyledModalContainer>
          <StyledModalContent>
            <Button style={styles.btnClose} onClick={onCloseModal}>
              <img
                src="https://salt.tikicdn.com/ts/upload/fe/20/d7/6d7764292a847adcffa7251141eb4730.png"
                alt=""
                style={styles.btnClose}
              />
            </Button>

            <StyledLoginContainer>
              {/* Đăng nhập */}
              <div className={`${isOpenLogin ? "block" : "hidden"}`}>
                <Typography.Title level={2}>Xin chào,</Typography.Title>
                <Typography.Title level={3}>
                  Đăng nhập tài khoản
                </Typography.Title>
                <Typography.Paragraph style={{ marginBottom: "24px" }}>
                  Nhập email và mật khẩu tài khoản Tiki
                </Typography.Paragraph>

                <Form scrollToFirstError>
                  <Form.Item
                    label=""
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Địa chỉ email không hợp lệ",
                      },
                      {
                        required: true,
                        message: "Vui lòng nhập email của bạn",
                      },
                    ]}
                  >
                    <Input
                      placeholder="abc@gmail.com"
                      style={styles.inputField}
                      value={emailLogin}
                      onChange={(e) => handleEmailChange(e.target.value)}
                    />
                  </Form.Item>

                  <Form.Item
                    label=""
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu của bạn",
                      },
                      {
                        validator: PasswordValidator,
                      },
                    ]}
                  >
                    <Input.Password
                      placeholder="Mật khẩu"
                      style={styles.inputField}
                      value={passwordLogin}
                      onChange={(e) => handlePasswordChange(e.target.value)}
                    />
                  </Form.Item>

                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={styles.btnSubmit}
                      onClick={handleLogin}
                    >
                      ĐĂNG NHẬP
                    </Button>
                  </Form.Item>
                </Form>

                <Typography.Paragraph style={styles.textFooter}>
                  Quên mật khẩu?
                </Typography.Paragraph>
                <Typography.Paragraph style={{ color: "rgb(120, 120, 120)" }}>
                  Chưa có tài khoản?{" "}
                  <Typography.Text
                    style={styles.textFooter}
                    onClick={() => {
                      setIsOpenLogin(false);
                      setIsOpenRegister(!isOpenRegister);
                      setIsCloseEmailRegister(true);
                    }}
                  >
                    Tạo tài khoản
                  </Typography.Text>
                </Typography.Paragraph>
              </div>

              {/* Đăng ký */}
              <div className={`${isOpenRegister ? "hidden" : "block"}`}>
                <Button
                  style={styles.btnAction}
                  onClick={() => {
                    setIsOpenLogin(true);
                    setIsOpenRegister(!isOpenRegister);
                    setIsOpenPassRegister(true);
                  }}
                >
                  <img
                    src="https://salt.tikicdn.com/ts/upload/0b/43/2f/7c7435e82bce322554bee648e748c82a.png"
                    alt=""
                    width={21}
                  />
                </Button>

                <div className={`${isCloseEmailRegister ? "block" : "hidden"}`}>
                  <Typography.Title level={3} style={{ marginTop: 0 }}>
                    Tạo tài khoản
                  </Typography.Title>
                  <Typography.Paragraph style={{ marginBottom: "24px" }}>
                    Vui lòng nhập email và mật khẩu trên Tiki
                  </Typography.Paragraph>

                  <Form
                    scrollToFirstError
                    onFinish={() => {
                      setIsCloseEmailRegister(!isCloseEmailRegister);
                      setIsOpenPassRegister(!isOpenPassRegister);
                    }}
                  >
                    <Form.Item
                      label=""
                      name="emailRegister"
                      rules={[
                        {
                          type: "email",
                          message: "Địa chỉ email không hợp lệ",
                        },
                        {
                          required: true,
                          message: "Vui lòng nhập email của bạn",
                        },
                      ]}
                    >
                      <Input
                        placeholder="abc@gmail.com"
                        style={styles.inputField}
                        value={emailRegister}
                        onChange={(e) =>
                          handleEmailRegisterChange(e.target.value)
                        }
                      />
                    </Form.Item>

                    <Form.Item
                      label=""
                      name="passwordRegister"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng nhập mật khẩu của bạn",
                        },
                        {
                          validator: PasswordValidator,
                        },
                      ]}
                    >
                      <Input.Password
                        placeholder="Mật khẩu"
                        style={styles.inputField}
                        value={passwordRegister}
                        onChange={(e) =>
                          handlePasswordRegisterChange(e.target.value)
                        }
                      />
                    </Form.Item>

                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        style={styles.btnSubmit}
                      >
                        TIẾP TỤC
                      </Button>
                    </Form.Item>

                    <Typography.Paragraph style={styles.textNote}>
                      Bằng việc tiếp tục, bạn đã chấp nhận{" "}
                      <Typography.Link
                        style={{
                          ...styles.textNote,
                          textDecoration: "underline",
                        }}
                      >
                        điều khoản sử dụng
                      </Typography.Link>
                    </Typography.Paragraph>
                  </Form>
                </div>

                <div className={`${isOpenPassRegister ? "hidden" : "block"}`}>
                  <Typography.Title level={3} style={{ marginTop: 0 }}>
                    Nhập thông tin cá nhân
                  </Typography.Title>
                  <Typography.Paragraph style={{ marginBottom: "24px" }}>
                    Vui lòng nhập đầy đủ thông tin
                  </Typography.Paragraph>

                  <Form scrollToFirstError>
                    <Form.Item
                      label=""
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng nhập họ tên của bạn",
                        },
                      ]}
                    >
                      <Input
                        placeholder="Họ tên"
                        style={styles.inputField}
                        value={nameRegister}
                        onChange={(e) =>
                          handleNameRegisterChange(e.target.value)
                        }
                      />
                    </Form.Item>

                    <Form.Item
                      label=""
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng nhập số điện thoại của bạn",
                        },
                      ]}
                    >
                      <Input
                        placeholder="Số điện thoại"
                        style={styles.inputField}
                        value={phoneRegister}
                        onChange={(e) =>
                          handlePhoneRegisterChange(e.target.value)
                        }
                      />
                    </Form.Item>

                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        style={styles.btnSubmit}
                        onClick={() => handleRegister()}
                      >
                        ĐĂNG KÝ
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </StyledLoginContainer>

            <StyledPhoto>
              <img
                src="https://salt.tikicdn.com/ts/upload/eb/f3/a3/25b2ccba8f33a5157f161b6a50f64a60.png"
                alt=""
                width={203}
              />
              <StyledPhotoContent>
                <Typography.Title level={4} style={styles.photoText}>
                  Mua sắm tại Tiki
                </Typography.Title>
                <Typography.Text style={styles.photoText}>
                  Siêu ưu đãi mỗi ngày
                </Typography.Text>
              </StyledPhotoContent>
            </StyledPhoto>
          </StyledModalContent>
        </StyledModalContainer>
      </StyledModal>
    </>
  );
}

export default RegisterLoginModal;
