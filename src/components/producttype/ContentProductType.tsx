import { Card, Image, Typography } from "antd";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Product } from "../../models/types/Product";
import { ApiResponse } from "../../models/ApiResponse";
import { baseUrl } from "../../constants/url";
import axios from "axios";
import slugify from "slugify";

function ContentProductType() {
  const { id } = useParams();
  const [dataProducts, setDataProducts] = useState<Product[] | undefined>([]);

  useEffect(() => {
    const getDataProducts = () => {
      const url = `${baseUrl}/api/products/category=${id}`;
      axios
        .get<ApiResponse<Product[]>>(url)
        .then((res) => {
          setDataProducts(res.data.data);
        })
        .catch((err) => {
          console.error(err);
        });
    };

    getDataProducts();
  }, [id]);

  return (
    <div
      style={{
        textAlign: "center",
        minHeight: "50vh",
        lineHeight: "120px",
        color: "#fff",
        display: "flex",
        flexWrap: "wrap",
        alignSelf: "stretch",
        gap: "8px",
        borderTop: "1px solid rgb(242, 242, 242)",
        padding: "0px 0px 16px",
        width: "100%",
      }}
    >
      {dataProducts &&
        dataProducts.map((item, key) => (
          <Link
            style={{
              width: "16%",
            }}
            to={`/product/${slugify(item.name, {
              lower: true,
              locale: "vi",
              remove: /[*+~.()'"!:@]/g,
            }).replace(/\s+/g, "-")}/${item.id}`}
            key={key}
          >
            <Card
              hoverable
              cover={
                <Image
                  preview={false}
                  alt="example"
                  style={{ width: "183px", height: "183px" }}
                  src={item.product_images[0]?.url}
                />
              }
            >
              <Typography
                style={{
                  textAlign: "start",
                  fontSize: "13px",
                  height: "40px",
                  fontWeight: "bold",
                  wordBreak: "break-word",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  WebkitLineClamp: "2",
                  display: "-webkit-box",
                  WebkitBoxOrient: "vertical",
                }}
              >
                {item.name}
              </Typography>
              {/* <div style={{ display: "flex" }}>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginRight: "6px",
                        }}
                      >
                        <Typography
                          style={{
                            fontSize: "12px",
                            marginRight: "3px",
                            color: "rgb(128, 128, 137)",
                          }}
                        >
                          {item.rate}
                        </Typography>
                        <StarFilled
                          style={{ fontSize: "12px", color: "rgb(253, 216, 54)" }}
                        />
                      </div>
                      <div
                        style={{
                          position: "relative",
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <div
                          style={{
                            content: "",
                            display: "block",
                            height: "10px",
                            position: "absolute",
                            border: "1px solid rgb(235, 235, 240)",
                          }}
                        ></div>
                        <Typography
                          style={{
                            fontSize: "12px",
                            marginRight: "6px",
                            paddingLeft: "6px",
                            color: "rgb(128, 128, 137)",
                            lineHeight: 0,
                          }}
                        >
                          Da ban {item.sold}
                        </Typography>
                      </div>
                    </div> */}
              <div>
                <Typography
                  style={{
                    textAlign: "start",
                    color: "red",
                    marginTop: "6px",
                    fontWeight: "bold",
                  }}
                >
                  {item.price} ₫
                </Typography>
              </div>

              <div>
                <Typography.Text
                  style={{
                    textAlign: "start",
                    color: "#1677ff",
                    marginTop: "6px",
                    width: "100%",
                    fontSize: "12px",
                    display: "inline-block",
                  }}
                >
                  {item.store.name}
                </Typography.Text>
              </div>
            </Card>
          </Link>
        ))}
    </div>
  );
}

export default ContentProductType;
