import { Input } from 'antd';
import React from 'react'
import {
    SearchOutlined,
} from "@ant-design/icons/lib/icons";
import { useState } from "react";
import ContentHome from './ContentHome';
function SearchComponent(input: any) {
    const { Search } = Input;
    const handleChange = (event: any) => {
        setSearchItem(event.target.value);
        console.log(searchItem);
    }
    const [searchItem, setSearchItem] = useState("")

    return (
        <>
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    width: "100%",
                }}
            >
                <Search
                    prefix={<SearchOutlined />}
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    onClick={handleChange}
                    size="large"
                />
                {/* {searchItem !== '' ? <ContentHome input={searchItem}></ContentHome> : ''} */}
            </div>
        </>

    )
}

export default SearchComponent