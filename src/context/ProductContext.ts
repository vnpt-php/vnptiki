import React, { createContext } from "react";
import { Product } from "../models/types/Product";

interface ProductContextType {
  dataProduct: Product[];
  setDataProduct: React.Dispatch<React.SetStateAction<Product[]>>;
}

const ProductContext = createContext<ProductContextType>({
  dataProduct: [],
  setDataProduct: () => {},
});

export default ProductContext;
