import React, { createContext } from "react";
import { Category } from "../models/types/Category";

interface CategoryContextType {
  dataCategory: Category[];
  setDataCategory: React.Dispatch<React.SetStateAction<Category[]>>;
}

const CategoryContext = createContext<CategoryContextType>({
  dataCategory: [],
  setDataCategory: () => {},
});

export default CategoryContext;
