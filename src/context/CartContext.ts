import React, { createContext } from "react";
import { Cart } from "../models/types/Cart";

interface CartContextType {
  dataCart: Cart[];
  setDataCart: React.Dispatch<React.SetStateAction<Cart[]>>;
}

const CartContext = createContext<CartContextType>({
  dataCart: [],
  setDataCart: () => {},
});

export default CartContext;
