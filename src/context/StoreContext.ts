import React, { createContext } from "react";
import { Store } from "../models/types/Store";

interface StoreContextType {
  dataStore: Store | undefined;
  setDataStore: React.Dispatch<React.SetStateAction<Store | undefined>>;
}

const StoreContext = createContext<StoreContextType>({
  dataStore: {} as Store,
  setDataStore: () => {},
});

export default StoreContext;
