import { Brand } from "./../models/types/Brand";
import React, { createContext } from "react";

interface BrandContextType {
  dataBrand: Brand[];
  setDataBrand: React.Dispatch<React.SetStateAction<Brand[]>>;
}

const BrandContext = createContext<BrandContextType>({
  dataBrand: [],
  setDataBrand: () => {},
});

export default BrandContext;
