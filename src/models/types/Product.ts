import { Brand } from "./Brand";
import { Category } from "./Category";
import { ProductImage } from "./ProductImage";
import { Store } from "./Store";

export interface Product {
  id: string;
  name: string;
  description: string;
  price: number;
  store_id: string;
  category_id: string;
  brand_id: string;
  product_images: ProductImage[];
  brand: Brand;
  category: Category;
  store: Store;
}
