export interface Address {
  id: string;
  user_id: string;
  name: string;
  city: string;
  phone: string;
  ward: string;
  district: string;
  default: boolean;
  detail: string;
}
