import { Store } from "./Store";

export interface User {
  id: string;
  name: string;
  phone: number;
  email: string;
  avatar: string;
  store: Store;
}
