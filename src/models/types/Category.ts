import { Brand } from "./Brand";

export interface Category {
  id: string;
  name: string;
  brands: Brand[];
}
