import { OrderItem } from "./OrderItem";
import { Store } from "./Store";
import { User } from "./User";

export interface Order {
  id: string;
  user_id: string;
  store_id: string;
  status: string;
  user: User;
  store: Store;
  order_items: OrderItem[];
  created_at: Date;
}
