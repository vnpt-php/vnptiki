import { Category } from "./Category";

export interface Brand {
  id: string;
  name: string;
  category: Category[];
}
