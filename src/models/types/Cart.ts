import { CartItem } from "./CartItem";
import { Store } from "./Store";

export interface Cart {
  id: string;
  quantity: number;
  store_id: string;
  store: Store;
  cart_items: CartItem[];
}
