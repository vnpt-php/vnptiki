import { Product } from "./Product";

export interface OrderItem {
  id: string;
  order_id: string;
  product_id: string;
  product: Product;
  quantity: number;
  created_at: Date;
}
