export interface Store {
  id: string;
  user_id: string;
  name: string;
  address: string;
  created_at: Date;
}
